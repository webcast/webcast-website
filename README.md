# Webcast Website

[![pipeline status](https://gitlab.cern.ch/webcast/webcast-website/badges/openshift_master/pipeline.svg)](https://gitlab.cern.ch/webcast/webcast-website/commits/openshift_master)

[![coverage report](https://gitlab.cern.ch/webcast/webcast-website/badges/openshift_master/coverage.svg)](https://gitlab.cern.ch/webcast/webcast-website/commits/openshift_master)

## Operations documentation

- [https://dev-documentation.web.cern.ch/opsdoc/Webcast/webcast-website.html](https://dev-documentation.web.cern.ch/opsdoc/Webcast/webcast-website.html)
- Repository: [https://gitlab.cern.ch/collaborativeapps/opsdoc](https://gitlab.cern.ch/collaborativeapps/opsdoc)


## Preparing the Docker environment

Docker is needed.

### Cleaning Docker if an error occurs

```commandline
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
```

### First run - Creating the database

```bash
local$ docker-compose run web bash
docker$ flask init-db
```

## Building and running the Docker images

### Build

```commandline
docker-compose build
```

### Run

```commandline
docker-compose up
```

### Bash into the machine

```commandline
docker-compose run web bash
```

## Access the project using localhost

```
127.0.0.1:8080
```

or

````
<hostname>:8080
````

## Database management

Create a database migration
```commandline
flask db migrate -d app/migrations
```

Apply a database migration
```commandline
flask db upgrade -d app/migrations
```

## Run tests

```commandline
pytest
```

If there is an error about `__pycache__` and `.pyc` files, delete them with the following command:

```
find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
```

### Test WOWZA streams

* Webcast docs: `https://dev-documentation.web.cern.ch/doconverter/Webcast/HandyFFmpegCommands.html
`

You need ffmpeg to do the streaming to the wowza server and also a video to stream.

1. ffmpeg: https://ffmpeg.org/
1. Sample video (Big Bucks Bunny): https://peach.blender.org/download/

On mac, use this command:
```commandline
/Applications/ffmpeg/ffmpeg -re -i ~/Movies/Big_Buck_Bunny_1080p_surround_FrostWire.com.avi -acodec aac -strict -2 -ab 96k -vcodec libx264 -s 1920x1080 -b:v 5000k -preset medium -vprofile main -r 25 -f flv rtmp://tev:tev2016@wowzaqaorigin.cern.ch/liveoutside/test_event
```
* `liveoutside`: Is the application name
* `test_event`: Is the event ID (smil id will be: `test_event_all`, smil camera will be: `test_event_all.smil`)



# Openshift configuration for Webcast Website

The config folder includes the deployment configs for the application and also the services.

# Known problems

## Images not displayed properly

Ensure that the cronjob HOSTNAME is set up and that the route is redirecting http traffic to https.


## Timestamp invalid on Indico request

Probably the container date and time is not synchronized with the host. Restart docker and start it again.





