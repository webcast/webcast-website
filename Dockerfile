FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest

EXPOSE 8080

ENV USER_ID 1001
ENV WORKING_DIR /opt/app-root/src

RUN yum -y install \
  CERN-CA-certs \
  gcc \
  libffi-devel \
  libxml2-devel \
  libxslt-devel \
  mysql-devel \
  openssl-devel \
  postgresql-devel \
  pcre \
  pcre-devel \
  python \
  python-devel \
  python-pip \
  python-wheel

ENV REQUESTS_CA_BUNDLE /etc/ssl/certs/ca-bundle.crt
ENV PYTHONPATH ${WORKING_DIR}
ENV FLASK_APP ${WORKING_DIR}/wsgi.py
ENV PYTHONUNBUFFERED 1

# The following environment variables are used by Poetry to install dependencies
ENV POETRY_VERSION 1.5.0
ENV POETRY_HOME /opt/poetry
ENV POETRY_VIRTUALENVS_IN_PROJECT true
ENV POETRY_CACHE_DIR ${WORKING_DIR}/.cache
ENV VIRTUAL_ENVIRONMENT_PATH ${WORKING_DIR}/.venv

# Adding the virtual environment to PATH in order to "activate" it.
# https://docs.python.org/3/library/venv.html#how-venvs-work
ENV PATH="$VIRTUAL_ENVIRONMENT_PATH/bin:$PATH"

RUN python --version
RUN python -m ensurepip --upgrade
RUN python -m pip --version

RUN mkdir -p ${WORKING_DIR}
# Set folder permissions
RUN chgrp -R 0 ${WORKING_DIR} && \
  chmod -R g=u ${WORKING_DIR}

WORKDIR ${WORKING_DIR}

# Install Poetry and dependencies
COPY pyproject.toml ./
COPY poetry.lock ./

# Using Poetry to install dependencies without requiring the project main files to be present
RUN pip install poetry==${POETRY_VERSION} && poetry install --only main --no-root --no-directory


# Copy the application files
COPY etc /opt/app-root/src/etc
COPY server /opt/app-root/src/server

COPY wsgi.py /opt/app-root/src/wsgi.py
COPY celery_app.py /opt/app-root/src/celery_app.py

COPY app /opt/app-root/src/app
COPY migrations /opt/app-root/src/migrations

CMD ["sh", "/opt/app-root/src/etc/entrypoint.sh"]
