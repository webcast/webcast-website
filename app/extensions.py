from celery import Celery
from flask_caching import Cache
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
migrate = Migrate()

#
# Cahing: By default, caching is disabled, but this can be changed using the configuration parameters in config
#
cache = Cache(config={"CACHE_TYPE": "null"})

celery = Celery()

ALLOWED_IMAGES_EXTENSIONS = ["png", "jpg", "jpeg"]
