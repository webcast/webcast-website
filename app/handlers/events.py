import datetime
import json
import logging
from typing import Union

import sqlalchemy
from flask import current_app
from sqlalchemy.orm.exc import NoResultFound, ObjectDeletedError

from app.common.datetime_helper import generate_date_from_string
from app.common.utils import get_event_random_image
from app.extensions import db
from app.models.audiences import Audience
from app.models.events import Event, EventStatus, FollowUp
from app.models.streams import LIVESTREAM_TYPES, LiveStream
from app.models.webcast_lite_settings import WebcastLiteSettings
from app.plugins.indico_api_client import IndicoAPIClient
from app.services.indico import IndicoService
from app.tasks.webcast_lite import create_webcast_lite_event, task_create_webcast_lite_upcoming_events


class EventArchived(object):
    pass


class EventUpcomingHandler(object):
    def __init__(self, logger=None):
        if not logger:
            self.logger = logging.getLogger("webapp.events")
        else:
            self.logger = logger

    def fetch_event_by_id(self, event_id):
        """
        Gets the details of an existing Event
        :param event_id: Event ID
        :return:
        """
        self.logger.debug("Fetching single event")
        existing_event = Event.query.get(event_id)
        if existing_event and existing_event.indico_id:
            self._fetch_event_details(existing_event)

    def fetch_event_by_indico_id(self, indico_id):
        event_details_json = IndicoAPIClient().get_event(event_id=indico_id)
        event_details = json.loads(event_details_json[1])  # Event is in position 1 of the array
        if len(event_details["results"]) > 0:
            event_details = event_details["results"][0]
        else:
            return None
        if event_details:
            speakers = []
            for speaker in event_details["chairs"]:
                speakers.append(speaker["first_name"] + " " + speaker["last_name"])
            speakers_string = ", ".join(map(str, speakers))

            event_room = event_details["room"]
            event_category = event_details["categoryId"]

            event_url = event_details["url"]
            event_title = event_details["title"]
            event_start_date = event_details["startDate"]["date"]
            event_start_time = event_details["startDate"]["time"]
            event_timezone = event_details["startDate"]["tz"]
            #
            event_end_date = event_details["endDate"]["date"]
            event_end_time = event_details["endDate"]["time"]

            start_date = generate_date_from_string(event_start_date, event_start_time)
            end_date = generate_date_from_string(event_end_date, event_end_time)

            # Event expires if the previous connection took long, so we need to get it again.
            try:
                audience = Audience.query.filter_by(name="No restriction").one()
            except NoResultFound:
                audience = Audience(name="No restriction", default_application="live")
                db.session.add(audience)
                db.session.commit()

            event = Event.query.filter_by(indico_id=indico_id).one_or_none()

            if event:
                self.logger.debug("Updating an existing event")
                event.room = event_room
                event.link = event_url
                event.title = event_title
                event.timezone = event_timezone
                event.audience = audience
                event.status = EventStatus.UPCOMING
            else:
                event = Event(
                    room=event_room,
                    link=event_url,
                    title=event_title,
                    timezone=event_timezone,
                    indico_id=indico_id,
                    audience=audience,
                    status=EventStatus.UPCOMING,
                )
                event_handler = EventHandler(logger=self.logger)
                event_handler.add_default_live_stream(event)

                if event.audience:
                    event.default_img = get_event_random_image(event.audience.id, None)

            event.indico_category_id = event_category
            event.speakers = speakers_string
            event.start_date = start_date
            event.end_date = end_date

            event.is_synchronized = False
            db.session.add(event)
            db.session.commit()

            create_webcast_lite_event(event.id)

            return True
        return None

    def fetch_events(self):
        """
        Gets and creates a list of Upcoming Events

        :return:
        """

        # First we fetch the events from Indico.
        # By default the events of the following two months.
        indico_service = IndicoService(logger=self.logger)
        self.logger.info("Fetching upcoming Indico events -> Start")

        events_json = indico_service.fetch_upcoming_events()
        # Save current events
        current_events = []
        for event in Event.query.filter_by(is_synchronized=True, status=EventStatus.UPCOMING).all():
            current_events.append(event.id)

        # Add the new events
        events_to_keep = []
        for event_json in events_json:
            events = self.create_event_from_json(event_json)
            for event in events:
                events_to_keep.append(event.id)

        # Delete all events synchronized that are not in the events_sto_keep array
        for event_id in current_events:
            event: Union[Event, None] = Event.query.get(event_id)
            if event_id not in events_to_keep and event:
                self.logger.debug(
                    f"Event with Indico ID: {event.indico_id} is not " "on events_to_keep and will be deleted"
                )
                db.session.delete(event)
                db.session.commit()
            else:
                if event:
                    self.logger.debug(f"Event with Indico ID: {event.indico_id} IS on events_to_keep.")
        if not current_app.config["TESTING"] and not current_app.config["IS_LOCAL_INSTALLATION"]:
            task_create_webcast_lite_upcoming_events.delay()

        self.logger.info("Fetching upcoming Indico events -> Finished")

    def create_event_from_json(self, event):
        """
        {
          'audience': 'ATLAS collaborators only',
          '_ical_id': 'indico-audiovisual-c2510390@cern.ch',
          'room': 'Salle Curie',
          'services': [
            'webcast',
            'recording'
          ],
          'event_id': 622398,
          'status': 'A',
          'url': 'https://indico.cern.ch/event/622398/contributions/2510390/',
          'endDate': {
            'date': '2017-03-14',
            'tz': 'UTC',
            'time': '14:19:00'
          },
          'location': 'CERN',
          'room_full_name': '40-S2-C01 - Salle Curie',
          'title': 'ATLAS Weekly - Measurement of the $t\\bar{t}\\gamma$ production cr',
          'startDate': {
            'date': '2017-03-14',
            'tz': 'UTC',
            'time': '14:15:00'
          }
        }
        """
        event_audience = event["audience"]
        event_room = event["room"]
        event_id = str(event["event_id"])
        event_url = event["url"]
        event_title = event["title"]
        event_start_date = event["startDate"]["date"]
        event_start_time = event["startDate"]["time"]
        event_timezone = event["startDate"]["tz"]
        event_end_date = event["endDate"]["date"]
        event_end_time = event["endDate"]["time"]
        start_date = generate_date_from_string(event_start_date, event_start_time)
        end_date = generate_date_from_string(event_end_date, event_end_time)

        # We get or create an Audience for the Event
        audience = self._extract_audience(event_audience)
        # We check if the event has been already added.
        # If already added:
        #    - If synchronized:
        #        - Update the event
        #    - Else:
        #        - Don't do anything
        # Else:
        #    - We have to create a new event
        existing_events = Event.query.filter_by(indico_id=event_id).all()

        if len(existing_events) > 0:
            for event in existing_events:
                self.logger.debug(f"Updating exiting event id: {event.id} indico_id: {event.indico_id}")
                self.update_existing_event(
                    audience,
                    end_date,
                    event_id,
                    event_room,
                    event_timezone,
                    event_title,
                    event_url,
                    event,
                    start_date,
                )
                return existing_events
        else:
            event = self.create_new_event(
                audience,
                end_date,
                event_id,
                event_room,
                event_timezone,
                event_title,
                event_url,
                start_date,
            )
            return [event]

    def create_new_event(
        self,
        audience,
        end_date,
        indico_id,
        event_room,
        event_timezone,
        event_title,
        event_url,
        start_date,
    ):
        self.logger.debug(f"Creating a new Event with indico_id: {indico_id}")
        new_event = Event(
            room=event_room,
            indico_id=indico_id,
            link=event_url,
            title=event_title,
            audience=audience,
            timezone=event_timezone,
            status=EventStatus.UPCOMING,
        )
        new_event.start_date = start_date
        new_event.end_date = end_date
        self._fetch_event_details(new_event)
        db.session.add(new_event)
        db.session.commit()

        event_handler = EventHandler(logger=self.logger)
        event_handler.add_default_live_stream(new_event)

        if new_event.audience:
            new_event.default_img = get_event_random_image(new_event.audience.id, None)

        db.session.commit()

        return new_event

    def update_existing_event(
        self,
        audience,
        end_date,
        event_id,
        event_room,
        event_timezone,
        event_title,
        event_url,
        existing_event: Event,
        start_date,
    ):
        self.logger.debug(f"Updating existing Event {existing_event.id} with indico_id: {existing_event.indico_id}")
        event_to_update = existing_event
        if event_to_update and event_to_update.id and event_to_update not in db.session:
            event_to_update = Event.query.get(event_to_update.id)

        if event_to_update and event_to_update.is_synchronized:
            event_to_update.room = event_room
            event_to_update.link = event_url
            event_to_update.indico_id = event_id
            event_to_update.title = event_title
            event_to_update.audience = audience
            event_to_update.start_date = start_date
            event_to_update.end_date = end_date
            event_to_update.timezone = event_timezone
            event_to_update.status = EventStatus.UPCOMING

            self._fetch_event_details(event_to_update)

            if not event_to_update.default_img:
                event_to_update.default_img = get_event_random_image(audience.id, None)

            db.session.commit()

    def _extract_audience(self, event_audience):
        """
        Gets or creates an Audience by name

        :param event_audience: Name of the Audience
        :return:
        """
        query = Audience.query.filter_by(name=event_audience)
        try:
            audience = query.one()
        except NoResultFound:
            audience = Audience(name=event_audience)
            db.session.add(audience)
            db.session.commit()

        return audience

    def _fetch_event_details(self, event: Event):
        """
        {
          "count": 1,
          "additionalInfo": {

          },
          "_type": "HTTPAPIResult",
          "url": "https://indico.cern.ch/export/event/586436.json?ak=43f087052428a6679ab72",
          "results": [
            {
              "folders": [
                {
                  "_type": "folder",
                  "attachments": [
                    {
                      "_type": "attachment",
                      "description": "",
                      "content_type": "application/pdf",
                      "id": 2176100,
                      "size": 114945,
                      "modified_dt": "2017-03-01T10:36:39.009477+00:00",
                      "title": "CERNEPSeminar28.03.17.pdf",
                      "download_url": "https://indico.cern.ch/event/586436/1420240/CERNEPSeminar28.03.17.pdf",
                      "filename": "CERNEPSeminar28.03.17.pdf",
                      "is_protected": false,
                      "type": "file"
                    }
                  ],
                  "title": null,
                  "is_protected": false,
                  "default_folder": true,
                  "id": 1420240,
                  "description": ""
                }
              ],
              "startDate": {
                "date": "2017-03-28",
                "tz": "Europe/Zurich",
                "time": "11:00:00"
              },
              "_type": "Conference",
              "hasAnyProtection": false,
              "endDate": {
                "date": "2017-03-28",
                "tz": "Europe/Zurich",
                "time": "12:00:00"
              },
              "description": "<p>Precision physics requires appropriate inclusion of higher ordertained.<\\/p>",
              "roomMapURL": "https:\\/\\/maps.cern.ch\\/mapsearch\\/mapsearch.htm?n=[\'500\\/1-001\']",
              "creator": {
                "affiliation": "CERN",
                "_type": "Avatar",
                "last_name": "Lourenco",
                "email": "carlos.lourenco@cern.ch",
                "emailHash": "a01fc1ada4584baf2ceee9d5c639839b",
                "_fossil": "conferenceChairMetadata",
                "fullName": "Lourenco, Carlos",
                "first_name": "Carlos",
                "id": "2527"
              },
              "material": [

              ],
              "visibility": {
                "id": "",
                "name": "Everywhere"
              },
              "roomFullname": "500-1-001 - Main Auditorium",
              "references": [

              ],
              "allowed": {
                "users": [

                ],
                "groups": [

                ]
              },
              "address": "",
              "modificationDate": {
                "date": "2017-02-27",
                "tz": "Europe/Zurich",
                "time": "11:12:06.201099"
              },
              "timezone": "Europe/Zurich",
              "creationDate": {
                "date": "2016-11-03",
                "tz": "Europe/Zurich",
                "time": "12:31:51.601744"
              },
              "id": "586436",
              "category": "EP Seminar",
              "room": "Main Auditorium",
              "title": "Measurement of the running of the fine structure constant below 1 GeV with the KLOE detector",
              "url": "https://indico.cern.ch/event/586436/",
              "note": {

              },
              "chairs": [
                {
                  "_type": "ConferenceChair",
                  "last_name": "Venanzoni",
                  "affiliation": "INFN",
                  "db_id": 519525,
                  "_fossil": "conferenceChairMetadata",
                  "fullName": "Venanzoni, Graziano",
                  "id": "519525",
                  "first_name": "Graziano",
                  "emailHash": "16eef1969e6ed99352b6f68ee3fc3c4f",
                  "person_id": 2979360,
                  "email": "graziano.venanzoni@lnf.infn.it"
                }
              ],
              "location": "CERN",
              "_fossil": "conferenceMetadata",
              "type": "simple_event",
              "categoryId": 3247
            }
          ],
          "ts": 1490280109
        }
        """
        self.logger.debug(f"Fetching event details for event id: {event.id} with indico_id: {event.indico_id}")
        # Fetch events from Indico
        event_details_json = IndicoAPIClient().get_event(event_id=event.indico_id)
        event_details = json.loads(event_details_json[1])  # Event is in position 1 of the array

        event_details = event_details["results"][0]

        if event_details:
            speakers = []
            for speaker in event_details["chairs"]:
                speakers.append(speaker["first_name"] + " " + speaker["last_name"])
            speakers_string = ", ".join(map(str, speakers))

            event_room = event_details["room"]
            event_category = event_details["categoryId"]

            event_url = event_details["url"]
            event_title = event_details["title"]
            event_start_date = event_details["startDate"]["date"]
            event_start_time = event_details["startDate"]["time"]
            event_timezone = event_details["startDate"]["tz"]
            #
            event_end_date = event_details["endDate"]["date"]
            event_end_time = event_details["endDate"]["time"]

            start_date = generate_date_from_string(event_start_date, event_start_time)
            end_date = generate_date_from_string(event_end_date, event_end_time)

            # Event expires if the previous connection took long, so we need to get it again.
            current_event = event
            if event and event.id and event not in db.session:
                current_event = Event.query.get(event.id)

            if current_event:
                current_event.indico_category_id = event_category
                current_event.speakers = speakers_string
                current_event.room = event_room
                current_event.link = event_url
                current_event.title = event_title
                current_event.timezone = event_timezone
                current_event.start_date = start_date
                current_event.end_date = end_date

                current_event.is_synchronized = True

                if not current_event.webcast_lite_settings:
                    webcast_lite_settings = WebcastLiteSettings()
                    webcast_lite_settings.event = current_event
                    current_event.webcast_lite_settings = webcast_lite_settings
                    db.session.add(webcast_lite_settings)

            db.session.commit()


class EventHandler(object):
    def __init__(self, logger=None):
        if not logger:
            self.logger = logging.getLogger("webapp.events")
        else:
            self.logger = logger

    def add_default_live_stream(self, event):
        """
        Generate the default LiveStream for the Event

        :param event: Event to add the default LiveStream to
        :return:
        """
        self.logger.debug(f"Adding default live stream to Event {event.id}")
        live_stream = LiveStream(type="camera_slides")
        live_stream.set_default_values(event)
        # Persist
        db.session.add(live_stream)
        event.live_stream = live_stream
        db.session.commit()

    def get_event_or_404(self, event_id):
        dvr_suffix = "?DVR"
        event_found = True
        event = None
        if event_id == "0":
            event = self.create_test_channel()
        else:
            self.logger.debug(event_id)
            try:
                if event_id.startswith("i"):
                    event_id = event_id[1:]
                    self.logger.debug(event_id)
                    event = Event.query.filter_by(indico_id=event_id).first_or_404()
                else:
                    event = Event.query.get_or_404(event_id)

            except sqlalchemy.exc.DataError:
                event_found = False

            #
            # If the event is LIVE, we remove the DVR suffix
            #
            if event and event.status == EventStatus.LIVE:
                if event.live_stream.camera_src.endswith(dvr_suffix):
                    event.live_stream.camera_src = event.live_stream.camera_src[:-4]
                if event.live_stream.slides_src.endswith(dvr_suffix):
                    event.live_stream.slides_src = event.live_stream.slides_src[:-4]

        return event, event_found

    def get_sources_for_test_channel(self):
        edge_server_url = current_app.config["WOWZA_EDGE_URL"]

        camera_src = (
            edge_server_url
            + "/vod/_definist_/Video/Public/WebLectures/2022/1135177c8/1135177c8_desktop_camera.smil/playlist.m3u8"
        )  # noqa
        slides_src = (
            edge_server_url
            + "/vod/_definist_/Video/Public/WebLectures/2022/1135177c8/1135177c8_desktop_slides.smil/playlist.m3u8"
        )  # noqa

        return camera_src, slides_src

    def create_test_channel(self):
        self.logger.debug("Creating test channel...")
        audience = Audience.query.filter_by(name="public").first()

        camera_src, slides_src = self.get_sources_for_test_channel()

        if not audience:
            raise ValueError("An audience must be set in order to create the test channel")

        event = Event(
            title="10 years of Higgs boson measurements at ATLAS and CMS",
            indico_id="0",
            link="https://indico.cern.ch/event/1135177",
            audience=audience,
            timezone="Europe/Zurich",
            status=EventStatus.LIVE,
            room="500/1-001",
        )
        # event.default_img = '/static-files/images/default/events/CMS_collaborators_only/cms3.jpg'
        event.speakers = "Kerstin Tackmann"
        event.abstract = (
            "<p>On 4 July 2012, a packed CERN Auditorium, joined by physicists "
            "in Melbourne on the eve of ICHEP2012, along with a global webcast audience, "
            "learned that the long-awaited Brout-Englert-Higgs mechanism had finally "
            "been revealed by the ATLAS and CMS experiments at CERN. The Spokespersons "
            "of both collaborations presented compelling evidence for the Higgs boson.</p>"
            "<p>The following ten years have seen impressive advances in our understanding "
            "of the Higgs boson's properties, and there is much more to be learned. "
            "This anniversary symposium celebrates 10 years of Higgs boson physics, "
            "takes a look at the latest results, and considers future prospects.</p>"
        )
        event.start_date = datetime.datetime.now()
        event.end_date = datetime.datetime.now()
        event.id = 0
        live_stream = LiveStream(
            slides_src=slides_src,  # noqa
            camera_src=camera_src,  # noqa
        )

        live_stream.type = LIVESTREAM_TYPES[1][1]
        event.live_stream = live_stream

        event.extra_html = """
        This event is also available <a href="#">in French</a>
        """

        return event

    def get_live_events(self):
        live_events = Event.query.filter_by(status=EventStatus.LIVE, is_visible=True).order_by(Event.start_date).all()
        return live_events

    def get_first_upcoming_event(self):
        first_upcoming_event = (
            Event.query.filter_by(status=EventStatus.UPCOMING, is_visible=True)
            .order_by(Event.start_date)
            .limit(1)
            .all()
        )
        return first_upcoming_event

    def get_upcoming_events(self, limit=6):
        upcoming_events = (
            Event.query.filter_by(status=EventStatus.UPCOMING, is_visible=True)
            .order_by(Event.start_date)
            .limit(limit)
            .all()
        )
        return upcoming_events


class EventFollowUpHandler(object):
    def __init__(self, logger=None):
        if not logger:
            self.logger = logging.getLogger("webapp.events")
        else:
            self.logger = logger

    def stop_following_event(self, event, stream_name):
        """
        There are not next contributions, so we can archive the event and delete the smils and follow ups
        """
        if event.follow_ups:
            for follow_up in event.follow_ups:
                self.logger.debug(f"Deleting FollowUp without next contribution: {stream_name}")
                try:
                    if follow_up.stream_name == stream_name:
                        event_followup_handler = EventFollowUpHandler(logger=self.logger)
                        event_followup_handler.remove_follow_up_and_smil(event, follow_up, stream_name)
                except ObjectDeletedError as error:
                    self.logger.error(f"Error removing followup {stream_name}: {error}")

    def set_new_event_contribution(self, event, next_contribution_date, stream_name):
        event.end_date = next_contribution_date.date()
        event.is_synchronized = False
        follow_ups = event.follow_ups
        # Remove and recreate the matching FollowUp
        if event.follow_ups:
            for follow_up in follow_ups:
                if follow_up.stream_name == stream_name:
                    self.delete_follow_up(event, follow_up, stream_name)

    def remove_follow_up_and_smil(self, event, follow_up, stream_name):
        """
        Removes the parameter FollowUp and the SMIL file that corresponds with It

        :param event:
        :param follow_up: FollowUp that will be deleted
        :param stream_name: Name of the SMIL file that will be deleted
        :return:
        """
        if follow_up not in db.session:
            follow_up = db.session.query(FollowUp).get(event.id)

        try:
            db.session.delete(follow_up)
            db.session.commit()
        except ObjectDeletedError as error:
            self.logger.error(f"Remove followup error: {str(error)} (Probably the followup was already deleted)")

    def delete_follow_up(self, event, follow_up, stream_name):
        """
        Deletes and recreates the FollowUp

        :param event:
        :param follow_up: FollowUp to delete
        :param stream_name: stream_name of the new FollowUp that will be created
        :return:
        """
        # TODO Maybe It is only needed to set the on_air status to False?
        self.logger.debug(f"Deleting FollowUp with next contribution: {follow_up}")
        # We remove the current follow_up and create a new one
        if follow_up not in db.session:
            follow_up = FollowUp.query.get(event.id)

        follow_up_type = follow_up.stream_type
        app_name = follow_up.app_name
        origin_server = follow_up.origin_server

        db.session.delete(follow_up)
        db.session.commit()

        return follow_up_type, app_name, origin_server

    def unpublish(self, event, stream_name=None):
        self.logger.debug("Unpublishing event")

        # Check if there will be a next contribution for this event
        today = datetime.datetime.now()
        indico_service = IndicoService(logger=self.logger)
        next_contribution_date = indico_service.get_next_contribution(event.indico_id, from_date=today)

        current_event = event

        # After making a call to the API there is a chance that the session has expired.
        if current_event not in db.session:
            current_event = db.session.query(Event).get(current_event.id)

        # Check if the event has followups
        if len(current_event.follow_ups) > 0:
            self.logger.debug(f"Event {current_event.id} has {len(current_event.follow_ups)} follow ups")

        streams_to_remove = [stream_name]
        if not stream_name:
            streams_to_remove = [follow_up.stream_name for follow_up in current_event.follow_ups]

        for stream_name_temp in streams_to_remove:
            # Does the event has a new contribution?
            event_followup_handler = EventFollowUpHandler(logger=self.logger)
            if next_contribution_date:
                event_followup_handler.set_new_event_contribution(
                    current_event, next_contribution_date, stream_name_temp
                )
            else:
                event_followup_handler.stop_following_event(current_event, stream_name_temp)

            self.logger.debug(f"Event has {len(current_event.follow_ups)} FollowUps left.")

        # Finally we are updating the Event Status accordingly
        if next_contribution_date:
            self.logger.debug("Event will have contributions")
            new_event_status = EventStatus.UPCOMING
            for follow_up in current_event.follow_ups:
                # If none of the follow ups is on_air, then make it Upcoming
                if follow_up.on_air:
                    new_event_status = EventStatus.LIVE

            current_event.status = new_event_status
        else:
            self.logger.debug("Event WONT have contributions")
            # if not current_event.follow_ups:
            current_event.is_visible = True
            current_event.status = EventStatus.ARCHIVED
            current_event.is_follow_up = False

        current_event.timestamp_ended = datetime.datetime.now()
        current_event.is_synchronized = False

        db.session.commit()
