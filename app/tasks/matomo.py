import logging
import time

from app.extensions import celery
from app.services.matomo.matomo_service import MatomoService

logger = logging.getLogger("job.tasks")


@celery.task()
def matomo_register_visit(ua: str, ip: str, lang: str, url: str, action_name: str):
    print("Matomo task")
    matomo_service = MatomoService()
    matomo_service.make_request_to_matomo(ua, ip, lang, url, action_name)
    logger.info(f"Start : {time.ctime()}")
    logger.info(f"End : {time.ctime()}")
    return "OK"
