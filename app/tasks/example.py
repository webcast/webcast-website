import logging
import time

from app.extensions import celery

logger = logging.getLogger("job.tasks")


@celery.task()
def dummy_task():
    print("Dummy task")
    logger.info(f"Start : {time.ctime()}")
    logger.info(f"End : {time.ctime()}")
    return "OK"
