"""
Celery tasks for the webcast lite service.
"""

import logging
from typing import List

from flask import current_app

from app.extensions import celery, db
from app.models.events import Event, EventStatus
from app.services.webcast_lite.webcast_lite_service import WebcastLiteService

logger = logging.getLogger("job.webcast_lite")


@celery.task()
def task_create_webcast_lite_upcoming_events():
    upcoming_events: List[Event] = Event.query.filter(Event.status == EventStatus.UPCOMING).all()

    actions = []
    messages = ["Add/Update multiple events"]
    service = WebcastLiteService(logger=logger)

    for event in upcoming_events:
        event_actions, event_messages = service.create_actions_for_event(event.id)

        actions += event_actions
        messages += event_messages

    # Create the Gitlab actions for this event
    if not current_app.config["TESTING"] and not current_app.config["IS_LOCAL_INSTALLATION"]:  # type: ignore
        logger.debug("Requesting creation of commit on webcast lite")
        logger.debug(f"Actions: {actions}")
        logger.debug(f"Messages: {messages}")
        committed = service.create_commit_for_actions(actions, messages)

        if committed:
            logger.info("Successfully created commit on webcast lite")
            return "OK"

        logger.warning("Failed to create commit on webcast lite")
        return "FAILED"


@celery.task()
def task_create_webcast_lite_event(
    event_id: int,
):
    """
    Creates a new event folder in the GitLab repository and
    adds a config file and an optional htaccess file.

    Args:
        event_id (int): The ID of the event to create.
    """
    logger.info(f"Creating webcast lite event {event_id}")

    service = WebcastLiteService(logger=logger)
    actions, messages = service.create_actions_for_event(event_id)
    committed = service.create_commit_for_actions(actions, messages)
    if committed:
        event = Event.query.get(event_id)
        event.webcast_lite_settings.event_generated = True
        db.session.commit()

    return committed


def create_webcast_lite_event(event_id: int):
    """Creates a webcast lite event

    Args:
        event (Event): The event to be created
        live_stream (LiveStream): The live stream of the event
    """

    if not current_app.config["TESTING"] and not current_app.config["IS_LOCAL_INSTALLATION"]:  # type: ignore
        logger.debug("Requesting creation of event on webcast lite")
        task_create_webcast_lite_event.delay(event_id)
    else:
        logger.debug("Not requesting creation of event on webcast lite")
