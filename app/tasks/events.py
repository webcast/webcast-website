import logging

from flask import current_app

from app.extensions import celery
from app.handlers.events import EventUpcomingHandler
from app.plugins.ces_client import CesClient

logger = logging.getLogger("job.tasks")


@celery.task()
def fetch_upcoming():
    print("Dummy task")
    with current_app.app_context():
        EventUpcomingHandler(logger=logger).fetch_events()
    return "OK"


@celery.task()
def fetch_cds():
    print("Dummy task")
    with current_app.app_context():
        CesClient(logger=logger).get_recent_events()
    return "OK"
