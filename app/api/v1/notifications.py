import logging

from flask import current_app, jsonify, make_response, request
from flask_restful import Resource, inputs, reqparse
from sqlalchemy.orm.exc import NoResultFound

from app.api.v1.utils import is_signature_valid
from app.extensions import db
from app.handlers.follow_ups import FollowUpHandler
from app.models.events import Event, FollowUp
from app.services.wowza import WowzaService

logger = logging.getLogger("webapp.api")


class StreamFollowUpNotification(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            "originServer",
            type=str,
            required=True,
            help="No originServer provided",
            location="args",
        )
        self.reqparse.add_argument(
            "appName",
            type=str,
            required=True,
            help="No appName provided",
            location="args",
        )
        self.reqparse.add_argument(
            "streamName",
            type=str,
            required=True,
            help="No streamName provided",
            location="args",
        )
        self.reqparse.add_argument(
            "onAir",
            type=inputs.boolean,
            required=True,
            help="No onAir provided",
            location="args",
        )

        super(StreamFollowUpNotification, self).__init__()

    def get(self):
        """
        Used to unPublish an event and to send an email notification.

        :return:
        """
        logger.debug("Stream Notification...")
        remote_address = request.environ.get("HTTP_X_FORWARDED_FOR", request.remote_addr)

        remote_address = remote_address.split(",")[0]

        logger.info(
            "New Stream FollowUp Notification api request from '{}' to '{}' with query string '{}'".format(
                remote_address,
                request.environ.get("PATH_INFO", "Unknown"),
                request.environ.get("QUERY_STRING", "Unknown"),
            )
        )

        allowed_ips = current_app.config.get("ALLOWED_API_IPS", [])
        # We need this to bypass problems with IP 6
        allowed_ips_pref = ["::ffff:" + ip for ip in allowed_ips]

        args = self.reqparse.parse_args()
        origin_server = args.get("originServer")
        app_name = args.get("appName")
        stream_name = args.get("streamName")
        on_air = args.get("onAir")

        logger.info(f"Remote IP is: {remote_address}")
        """
        We only allow some IPS
        """
        if (
            remote_address in allowed_ips
            or remote_address in allowed_ips_pref
            or current_app.config["IS_LOCAL_INSTALLATION"]
        ):
            logger.info("Remote address in allowed ips")
            logger.info(
                f"New request: origin={origin_server} appName={app_name} streamName={stream_name} status={on_air}"
            )

            # TODO Generate Follow Up
            """
            We search for the event with the ID on the stream_name to retrieve its followup.

            The stream_name can be something like:
                - <indico_id>_camera
                - <indico_id>_slides
                - <string> (like kakac)

            """

            indico_id = stream_name.split("_")
            indico_id = indico_id[0]

            events = Event.query.filter_by(indico_id=indico_id).all()
            if events:
                for event in events:
                    if event.follow_ups:
                        follow_ups = [follow_up.id for follow_up in event.follow_ups]
                        for follow_up_id in follow_ups:
                            follow_up = FollowUp.query.get(follow_up_id)
                            if follow_up:
                                """
                                If the stream_name matches, we have to notify the follow up
                                """
                                if stream_name == follow_up.stream_name:
                                    logger.info("Will notify the FollowUp with on_air: {}".format(on_air))
                                    followup_handler = FollowUpHandler(logger=logger)
                                    followup_handler.notify(follow_up, on_air=on_air)
                            else:
                                logger.info("FollowUp with stream_name {} doesn't exist".format(stream_name))
                    else:
                        logger.debug("Event {} doesn't have follow ups".format(indico_id))
                event.origin_server = origin_server
                db.session.commit()
            else:
                logger.info("No event found for ID {}.".format(indico_id))
                """
                If the  stream name is different to the event indico ID, we can try to find the
                followup by its stream_name
                """
                try:
                    follow_ups = FollowUp.query.filter_by(stream_name=indico_id).all()
                    for follow_up in follow_ups:
                        followup_handler = FollowUpHandler(logger=logger)
                        followup_handler.notify(follow_up, on_air=on_air)
                except NoResultFound:
                    logger.info(
                        "Tried to find FollowUp by stream_camera_name: {}. But It wasn't found.".format(indico_id)
                    )
                return make_response(
                    jsonify(result="No Event or FollowUp found with indico_id: {}".format(indico_id)),
                    403,
                )
        else:
            logger.warning("IP not allowed to use the service: {}".format(request.remote_addr))
            return make_response(jsonify(result="IP not allowed to use the service"), 403)

        return jsonify(result="ok")


class StreamUpdateNotification(Resource):
    """
    params['api'] = 'update-stream'
    params['ak'] = conf.WEBCAST_WEBSITE_ACCESS_KEY
    params['timestamp'] = int(time.time())
    items = sorted(params.items())
    url = '%s?%s' % (conf.WEBCAST_WEBSITE_API_PATH, urllib.urlencode(items))
    signature = hmac.new(conf.WEBCAST_WEBSITE_SECRET_KEY, url, hashlib.sha1).hexdigest()
    items.append(('signature', signature))
    """

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("ak", type=str, required=True, help="No Api Key provided", location="args")
        self.reqparse.add_argument(
            "timestamp",
            type=int,
            required=True,
            help="No Timestamp provided",
            location="args",
        )
        self.reqparse.add_argument("api", type=str, required=True, help="No API type", location="args")
        self.reqparse.add_argument(
            "signature",
            type=str,
            required=True,
            help="No signature provided",
            location="args",
        )

        # Event info
        self.reqparse.add_argument(
            "indico_id",
            type=str,
            required=True,
            help="No indico_id provided",
            location="args",
        )
        self.reqparse.add_argument(
            "room_video_quality",
            type=str,
            required=False,
            help="No indico_id provided",
            location="args",
        )

        self.reqparse.add_argument(
            "app_name",
            type=str,
            required=False,
            help="No app_name provided",
            location="args",
        )

        self.reqparse.add_argument(
            "camera_stream",
            type=str,
            required=False,
            help="No camera_stream provided",
            location="args",
        )

        self.reqparse.add_argument(
            "slides_stream",
            type=str,
            required=False,
            help="No slides_stream provided",
            location="args",
        )

    def get(self):
        """
        Will update the values of a LiveStream

        :return: The JSON response
        """
        logger.debug("Stream Update Notification...")
        logger.info(
            "New Stream Update api request from '{}' to '{}' with query string '{}'".format(
                request.environ.get("REMOTE_ADDR", "Unkwnown"),
                request.environ.get("PATH_INFO", "Unknown"),
                request.environ.get("QUERY_STRING", "Unknown"),
            )
        )

        args = self.reqparse.parse_args()
        timestamp = args.get("timestamp")
        api_key = args.get("ak")
        api_type = args.get("api")
        signature = args.get("signature")

        # Event info
        indico_id = args.get("indico_id", None)
        room_video_quality = args.get("room_video_quality", None)
        app_name = args.get("app_name", None)
        camera_stream = args.get("camera_stream", None)
        slides_stream = args.get("slides_stream", None)

        new_list = ()
        if api_key:
            new_list += (("ak", api_key),)
        if api_type:
            new_list += (("api", api_type),)
        if timestamp:
            new_list += (("timestamp", timestamp),)
        if indico_id:
            new_list += (("indico_id", indico_id),)
        if room_video_quality:
            new_list += (("room_video_quality", room_video_quality),)
        if app_name:
            new_list += (("app_name", app_name),)
        if camera_stream:
            new_list += (("camera_stream", camera_stream),)
        else:
            new_list += (("camera_stream", ""),)
        if slides_stream:
            new_list += (("slides_stream", slides_stream),)
        else:
            new_list += (("slides_stream", ""),)

        try:
            logger.debug("Checking if signature is valid...")
            signature_valid, message = is_signature_valid(api_key, signature, timestamp, args=new_list)

            if signature_valid:
                logger.debug("Signature is valid.")

                # Get the event with the indico_id
                try:
                    event = Event.query.filter_by(indico_id=indico_id).one()
                    first_stream = event.live_stream
                    if event.is_synchronized:
                        """
                        We set the data of the event LiveStream
                        """
                        if room_video_quality:
                            first_stream.room_video_quality = room_video_quality
                        if app_name:
                            first_stream.app_name = app_name
                        if camera_stream:
                            # Generate camera_src from camera stream
                            first_stream.stream_camera_name = camera_stream
                            logger.debug("CAMERA Stream: {}".format(camera_stream))
                        if slides_stream:
                            # Generate slides_src from slides stream
                            first_stream.stream_slides_name = slides_stream

                        # We set the default values for the Live Stream
                        first_stream.set_default_values(event)
                        db.session.commit()

                        logger.info(
                            f"LiveStream {first_stream.id} (Indico ID: {event.indico_id}) was updated without errors: "
                            f"app_name:{first_stream.app_name}"
                            f"room_video_quality:{first_stream.room_video_quality}"
                            f"stream_camera_name:{first_stream.stream_camera_name}"
                            f"stream_slides_name:{first_stream.stream_slides_name}"
                        )
                    else:
                        logger.info(f"Event {event.indico_id} is not synchronized. Won't update the LiveStream.")

                    wowza_service = WowzaService(logger=logger)

                    logger.info("Fetching camera and slides smil...")
                    (slides_result, slides_path, camera_result, camera_path,) = wowza_service.get_camera_slides_smil(
                        app_name,
                        stream_id=first_stream.id,
                        stream_camera_name=camera_stream,
                        stream_slides_name=slides_stream,
                    )

                    logger.info(
                        f"Smil fetched for stream {first_stream.id} "
                        f"(Indico ID: {event.indico_id}): Slides: {slides_result} / {slides_path} - "
                        f"Camera: {camera_result} / {camera_path}"
                    )

                except NoResultFound:
                    logger.warning("Event with indico_id: {} does not exist".format(indico_id))
                    return jsonify(
                        valid=False,
                        message="Event with indico_id: {} does not exist".format(indico_id),
                    )
                    # TODO Does the event always exist at this point?

                return jsonify(result="ok")
            logger.warning("Error using API: {}".format(message))
            return make_response(jsonify(valid=False, message="Error using API: {}".format(message)), 403)

        except Exception as e:
            logger.warning("Error using API: {}".format(e))
            return make_response(jsonify(valid=False, message="Error using API: {}".format(e)), 403)
