import logging

from flask import current_app, jsonify, make_response, request
from flask_restful import Resource, reqparse

from app.handlers.events import EventUpcomingHandler

logger = logging.getLogger("webapp.api")


class UpdateUpcomingEventsAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument("ak", type=str, required=True, help="No Api Key provided", location="args")

    def get(self):
        """
        Will update the values of a LiveStream

        :return: The JSON response
        """
        logger.debug("Export API...")
        logger.info(
            "New UpdateUpcomingEvents api request from '{}' to '{}' with query string '{}'".format(
                request.environ.get("REMOTE_ADDR", "Unkwnown"),
                request.environ.get("PATH_INFO", "Unknown"),
                request.environ.get("QUERY_STRING", "Unknown"),
            )
        )

        args = self.reqparse.parse_args()
        api_key = args.get("ak")

        event_upcoming_handler = EventUpcomingHandler(logger=logger)

        if api_key == current_app.config["FETCH_UPCOMING_API_KEY"]:
            event_upcoming_handler.fetch_events()
            return jsonify(result="ok")
        else:
            return make_response(
                jsonify(valid=False, message="Error using API: API key is incorrect"),
                403,
            )
