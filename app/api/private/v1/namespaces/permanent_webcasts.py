import logging

from flask import g
from flask_restx import Namespace, Resource

from app.api.api_authorizations import authorizations
from app.common.authentication.event_authentication import is_user_authorized
from app.extensions import cache
from app.models.streams import Category, PermanentStream
from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api.webcasts")

namespace = Namespace(
    "permanent-webcasts",
    description="Permanent Webcasts related operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)

THIRTY_SECONDS = 30


@cache.memoize(THIRTY_SECONDS)
def get_permanent_webcast_for_user(username, webcast_id):
    """
    Get a permanent webcast for the current user if this user has access to it.

    :param username: The username of the current logged in user
    :type username: str
    :param webcast_id: The ID of the webcast
    :type webcast_id: int
    :return: The dictionary with the permanent webcast details and 'success' True,
    otherwise an empty dict with success 'False'
    :rtype: dict
    """
    logger.info("Get permanent webcast for user (Not cached)")
    logger.debug(f"Get permanent webcast for user {username} on webcast {webcast_id}(Not cached)")

    webcast = PermanentStream.query.get(webcast_id)

    if is_user_authorized(webcast):
        return_dict = {"success": True, "result": webcast.to_json_details()}
    else:
        return_dict = {"success": False, "result": {}}
    return return_dict


@namespace.doc(security=["Bearer Token"])
@namespace.route("/permanent-categories/")
class PermanentWebcastsCategoriesEndpoint(Resource):
    @namespace.doc("categories")
    def get(self):
        """
        Get all the permanent categories
        :return: A list of Category as dict
        :rtype: dict
        """
        results_list = []
        logger.info("Get permanent categories endpoint")
        categories = Category.query.all()
        for cat in categories:
            results_list.append(cat.to_json_v2())

        return_dict = {
            "success": True,
            "count": len(categories),
            "results": results_list,
        }

        return return_dict


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<int:webcast_id>")
class PermanentWebcastsEndpoint(Resource):
    @namespace.doc("permanent_webcast")
    @oidc_validate
    def get(self, webcast_id):
        """
        Get a permanent webcast event (protected endpoint)
        :param webcast_id: The webcast internal ID
        :type webcast_id: int
        :return: A dict with the Permanent Webcast details
        :rtype: dict
        """
        logger.info("Get permanent webcast for user endpoint")
        user = g.user

        if user:
            return_dict = get_permanent_webcast_for_user(user, webcast_id)

            return return_dict
        else:
            return "No user set", 401
