from flask import Blueprint
from flask_restx import Api

from app.api.private.v1.namespaces.permanent_webcasts import namespace as permanent_webcasts_namespace
from app.api.private.v1.namespaces.webcasts import namespace as webcasts_namespace


def load_api_namespaces():
    bp = Blueprint("api", __name__)
    api = Api(bp)
    api.add_namespace(webcasts_namespace)
    api.add_namespace(permanent_webcasts_namespace)

    return bp
