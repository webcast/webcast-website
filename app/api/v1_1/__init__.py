import flask_restful
from flask import Blueprint

from app.api.v1_1.export import ExportAPI

API_VERSION_V1_1 = "1.1"
API_VERSION = API_VERSION_V1_1

api_v1_1_bp = Blueprint("api_v1_1", __name__)
api_v1_1 = flask_restful.Api(api_v1_1_bp)
api_v1_1.add_resource(ExportAPI, "/export/")
