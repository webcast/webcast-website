import logging

from flask import current_app, g, request
from flask_restx import abort
from itsdangerous import BadData, SignatureExpired

from app.utils.auth.oidc import user_info_from_app_token

logger = logging.getLogger("webapp.require_token")


def require_token(func):
    def wrapper(*args, **kwargs):
        g.user = None
        auth = request.headers.get("Authorization")
        token = None
        if auth and auth.startswith("Bearer "):
            token = auth[7:]
        if not token:
            view_func = current_app.view_functions[request.endpoint]
            if getattr(view_func, "_allow_anonymous", False):
                return
            abort(401, error="token_missing")
        try:
            user = user_info_from_app_token(token)
        except SignatureExpired:
            abort(401, error="token_expired")
        except BadData:
            abort(401, error="token_invalid")
        g.user = user
        return func(*args, **kwargs)

    wrapper.__doc__ = func.__doc__
    wrapper.__name__ = func.__name__
    return wrapper
