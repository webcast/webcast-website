import logging
import os
from datetime import datetime
from importlib import import_module
from typing import Optional
from urllib import parse

import sentry_sdk
from flask import Flask
from flask_cors import CORS
from sentry_sdk.integrations.flask import FlaskIntegration
from werkzeug.middleware.proxy_fix import ProxyFix

from app.api.v1 import API_VERSION_V1, api_v1_bp
from app.api.v1_1 import API_VERSION_V1_1, api_v1_1_bp
from app.api_loader import load_api_private_blueprints
from app.cli import initialize_cli
from app.common.authentication.cern_openid import load_cern_openid
from app.extensions import cache, celery, db, migrate
from app.models.events import EventStatus
from app.utils.celery_probes import LivenessProbe
from app.utils.logger import setup_logs
from app.views.blueprints import all_blueprints

logger = logging.getLogger("webapp.app_factory")


def create_app(
    config_filename: str = "config/config.py",
    static_url_path: str = "/static-files",
):
    """
    Factory to create the application using a file

    :param config_filename: The name of the file that will be used for configuration.
    :return: The created application
    """
    app = Flask(__name__, static_url_path=static_url_path)
    app.config.from_pyfile(config_filename)

    if app.config["USE_PROXY"]:
        app.wsgi_app = ProxyFix(app.wsgi_app)

    print("Initializing logs and cli...", end=" ")

    setup_logs(
        app,
        "webapp",
        to_remote=app.config.get("LOG_REMOTE_ENABLED", False),
        to_file=app.config.get("LOG_FILE_ENABLED", False),
    )
    setup_logs(
        app,
        "job",
        to_remote=app.config.get("LOG_REMOTE_ENABLED", False),
        to_file=app.config.get("LOG_FILE_ENABLED", False),
    )
    initialize_cli(app)
    print("ok")

    print("Initializing CORS...", end=" ")
    CORS(app)
    print("ok")
    init_sentry(app)

    _initialize_models(app)

    # Enable the CERN Oauth Authentication
    app.openid = load_cern_openid(app)

    # Initialize the database and the migrations
    setup_database(app)

    _initialize_views(app)

    _initialize_api(app)

    load_private_api_v1(app)

    # Initialize Cache
    init_redis_cache(app)

    # Setup Celery
    init_celery(app)

    @app.template_filter("url_decode")
    def url_decode(string):
        return parse.unquote(string)

    @app.context_processor
    def inject_conf_var():
        """
        Injects all the application's global configuration variables

        :return: A dict with all the environment variables
        """

        current_datetime = datetime.now()
        date = current_datetime.date()
        year = date.strftime("%Y")

        return dict(
            #
            # Analytics
            #
            ANALYTICS_PIWIK_ID=app.config["ANALYTICS_PIWIK_ID"],
            IS_LOCAL_INSTALLATION=app.config["IS_LOCAL_INSTALLATION"],
            DEBUG=app.config["DEBUG"],
            LIVE_STATUS=EventStatus.LIVE,
            ARCHIVED_STATUS=EventStatus.ARCHIVED,
            VIDEO_PLAYER_URL=app.config["VIDEO_PLAYER_URL"],
            NOW=datetime.utcnow(),
            CURRENT_YEAR=year,
        )

    logger.debug("Application finished loading.")

    return app


def setup_database(app):
    """
    Initialize the database and the migrations
    """

    if app.config.get("DB_ENGINE", None) == "postgresql":
        print("Initializing Postgres Database... ", end="")
        app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://{0}:{1}@{2}:{3}/{4}".format(
            app.config["DB_USER"],
            app.config["DB_PASS"],
            app.config["DB_SERVICE_NAME"],
            app.config["DB_PORT"],
            app.config["DB_NAME"],
        )

    elif app.config.get("DB_ENGINE", None) == "mysql":
        service_name = app.config["DB_SERVICE_NAME"]
        print(f"Initializing Mysql Database on {service_name}...", end="")
        app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://{0}:{1}@{2}:{3}/{4}?charset=utf8mb4".format(
            app.config["DB_USER"],
            app.config["DB_PASS"],
            app.config["DB_SERVICE_NAME"],
            app.config["DB_PORT"],
            app.config["DB_NAME"],
        )

    else:
        print("Initializing Sqlite Database...", end="")
        _basedir = os.path.abspath(os.path.dirname(__file__))
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(_basedir, "webapp.db")

    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    db.init_app(app)
    db.app = app
    print("ok")

    print("Initializing Database Migrations... ", end="")
    migrate.init_app(app, db)
    print("ok")


def _initialize_models(app):
    """
    Load all the available models
    """

    db_models_imports = (
        "app.models.api",
        "app.models.events",
        "app.models.streams",
        "app.models.users",
        "app.models.settings",
        "app.models.webcast_lite_settings",
    )

    with app.app_context():
        for module in db_models_imports:
            import_module(module)


def _initialize_views(application):
    """
    Initializes all the application blueprints

    :param application: Application where the blueprints will be registered
    :return:
    """

    for blueprint in all_blueprints:
        import_module(blueprint.import_name)
        application.register_blueprint(blueprint)


def _initialize_api(application):
    """
    Initializes all the application blueprints

    :param application: Application where the blueprints will be registered
    :return:
    """
    prefix = application.config["API_URL_PREFIX"]
    api_v1_version = API_VERSION_V1

    application.register_blueprint(
        api_v1_bp,
        url_prefix=f"{prefix}/v{api_v1_version}",
    )

    api_v1_1_version = API_VERSION_V1_1

    application.register_blueprint(
        api_v1_1_bp,
        url_prefix=f"{prefix}/v{api_v1_1_version}",
    )


def init_redis_cache(app):
    cache_config = {"CACHE_TYPE": "null"}
    if app.config.get("CACHE_ENABLE", False):
        print("Initializing Redis Caching... ", end="")
        cache_config = {
            "CACHE_TYPE": "redis",
            "CACHE_REDIS_HOST": app.config.get("CACHE_REDIS_HOST", None),
            "CACHE_REDIS_PASSWORD": app.config.get("CACHE_REDIS_PASSWORD", None),
            "CACHE_REDIS_PORT": app.config.get("CACHE_REDIS_PORT", None),
        }
    else:
        print("Caching disabled... ", end="")
    cache.init_app(app, config=cache_config)
    print("ok")


def load_private_api_v1(application):
    app_blueprints = load_api_private_blueprints()

    prefix = "/api"
    version = "v1"
    for blueprint in app_blueprints:
        application.register_blueprint(
            blueprint,
            url_prefix=f"{prefix}/private/{version}",
        )


def init_sentry(application: Flask = None) -> None:
    if application.config["ENABLE_SENTRY"]:
        print("Initializing Sentry... ", end="")
        # pylint: disable=abstract-class-instantiated
        sentry_sdk.init(
            dsn=application.config["SENTRY_DSN"],
            integrations=[FlaskIntegration()],
            environment=application.config["SENTRY_ENVIRONMENT"],
        )
        print("OK")


def init_celery(
    app: Optional[Flask] = None,
):
    """
    Initialize the Celery tasks support.

    :param app: The current Flask application.
    :return: The Celery instance
    """
    print("Initializing Celery... ", end="")

    tasks_modules = ["app.tasks.webcast_lite", "app.tasks.example", "app.tasks.events", "app.tasks.matomo"]

    app = app or create_app()
    celery.conf.broker_url = app.config["CELERY_BROKER_URL"]  # type: ignore
    celery.conf.result_backend = app.config["CELERY_RESULT_BACKEND"]  # type: ignore
    celery.conf.beat_schedule = app.config["CELERY_BEAT_SCHEDULE"]
    celery.conf.include = tasks_modules
    celery.conf.task_serializer = "json"
    celery.conf.result_serializer = "json"

    celery.conf.broker_connection_retry_on_startup = True
    celery.conf.timezone = "Europe/Zurich"

    print("ok")

    print("Adding Celery Liveness Probe... ", end="")
    celery.steps["worker"].add(LivenessProbe)

    class ContextTask(celery.Task):
        """Make celery tasks work with Flask app context"""

        def __call__(self, *args, **kwargs):
            if app:
                with app.app_context():
                    return self.run(*args, **kwargs)

    celery.Task = ContextTask
    print("ok")
    return celery
