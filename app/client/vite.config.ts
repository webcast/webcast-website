/// <reference types="vitest" />
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import viteTsconfigPaths from "vite-tsconfig-paths";
import svgr from "vite-plugin-svgr";
import eslint from "vite-plugin-eslint";
import vitePluginRequire from "vite-plugin-require";

export default ({ command, mode }) => {
  return defineConfig({
    plugins: [
      react(),
      viteTsconfigPaths(),
      vitePluginRequire(),
      svgr(),
      eslint(),
    ],
    test: {
      globals: true,
      environment: "jsdom",
      setupFiles: "./src/setupTests.ts",
      threads: false,
      environmentOptions: {
        jsdom: {
          resources: "usable",
        },
      },
    },
    server: {
      port: 3000,
    },
    preview: {
      port: 3000,
    },
    define: { "process.env.NODE_ENV": `"${mode}"` },
  });
};
