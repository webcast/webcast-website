import React from "react";
import { render, screen } from "@testing-library/react";
import EventDateText from "./EventDateText";

describe("EventDateText component tests", () => {
  it("renders all the texts", () => {
    render(
      <EventDateText
        startDate="2021-11-16"
        timezone="Europe/Zurich"
        startTime="17:15:00"
        endDate="2021-11-16"
        endTime="18:15:00"
      />,
    );
    let element = screen.getByText(/Tuesday 16 Nov 2021, 17:15/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Europe\/Zurich/i);
    expect(element).toBeInTheDocument();
  });
});
