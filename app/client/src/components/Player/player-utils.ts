import { ILayoutType } from "types/webcast";
import {
  createLiveCameraManifest,
  createLiveCameraSlidesManifest,
  createLiveSlidesManifest,
} from "utils/paella-manifests";

export async function customLoadVideoManifestFunction(
  cameraUrl: string | undefined,
  slidesUrl: string | undefined,
  layoutType: ILayoutType,
  webcastType: string,
  indicoId: string,
  webcastStatus: string,
) {
  console.log("Using custom loadVideoManifest function");
  function getManifest() {
    let usedType = "";
    const usedTitle = "Webcast";
    let usedCameraUrl = "";
    let usedSlidesUrl = "";

    console.log(
      `Using custom getManifest function: ${layoutType} ${webcastType}`,
    );
    if (
      cameraUrl &&
      (layoutType === "camera" || layoutType === "dual") &&
      (webcastType === "Camera" || webcastType === "Camera & Slides")
    ) {
      usedType = "camera";
      usedCameraUrl = cameraUrl;
    }
    if (
      slidesUrl &&
      (layoutType === "slides" || layoutType === "dual") &&
      (webcastType === "Slides" || webcastType === "Camera & Slides")
    ) {
      usedType = "slides";
      usedSlidesUrl = slidesUrl;
    }
    if (
      cameraUrl &&
      slidesUrl &&
      layoutType === "dual" &&
      webcastType === "Camera & Slides"
    ) {
      usedType = "camera_slides";
      usedCameraUrl = cameraUrl;
      usedSlidesUrl = slidesUrl;
    }
    console.log(`Indico ID: ${indicoId}`);
    console.log(`Webcast Status: ${webcastStatus}`);
    const manifests: {
      [key: string]: () => string;
    } = {
      camera_slides: () =>
        createLiveCameraSlidesManifest(
          usedTitle,
          usedCameraUrl,
          usedSlidesUrl,
          webcastStatus.toLowerCase() === "live" &&
            !usedCameraUrl.includes("/vod/"),
        ),
      camera: () =>
        createLiveCameraManifest(
          usedTitle,
          usedCameraUrl,
          webcastStatus.toLowerCase() === "live" &&
            !usedCameraUrl.includes("/vod/"),
        ),
      slides: () =>
        createLiveSlidesManifest(
          usedTitle,
          usedSlidesUrl,
          webcastStatus.toLowerCase() === "live" &&
            !usedSlidesUrl.includes("/vod/"),
        ),
    };
    const manifestFunc = manifests[usedType];
    const result = manifestFunc();
    return result;
  }
  const manifest = getManifest();
  console.debug(manifest);
  return JSON.parse(manifest);
}

export function getStreamUrl(
  withCdn: boolean,
  streamSrc: string | undefined,
  originServer: string | undefined,
) {
  if (streamSrc && withCdn) {
    let cdnBaseUrl = "cern2.vo.llnwd.net/e20";
    if (originServer === "wowza21.cern.ch") {
      cdnBaseUrl = "cern2.vo.llnwd.net/e21";
    }
    // Remove from the url: https://wowza.cern.ch/ and replace it with the CDN base URL
    return streamSrc.replace("wowza.cern.ch", cdnBaseUrl);
  }
  return streamSrc;
}

export async function customtGetVideoIdFunction() {
  console.log("Using CUSTOM getVideoId function");
  return "/";
}
