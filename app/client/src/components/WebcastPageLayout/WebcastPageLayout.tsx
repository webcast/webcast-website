import { useEffect } from "react";
import { Outlet, useParams } from "react-router-dom";
import { Grid, Segment } from "semantic-ui-react";
import RestrictedProvider from "components/RestrictedProvider/RestrictedProvider";
import TopMenu from "components/TopMenu/TopMenu";
import { useMatomoPageView } from "hooks/matomo/use-matomo";
import ResponsiveContainer from "pages/ResponsiveContainer";

export default function WebcastPageLayout() {
  const { id } = useParams();
  useMatomoPageView();

  useEffect(() => {
    document.body.style.backgroundColor = "#060606";
  }, []);

  if (id) {
    return (
      <RestrictedProvider indicoId={id}>
        <ResponsiveContainer>
          <TopMenu />
          <Segment basic padded>
            <Grid.Row>
              <Grid.Column>
                <Outlet />
              </Grid.Column>
            </Grid.Row>
          </Segment>
        </ResponsiveContainer>
      </RestrictedProvider>
    );
  }
  return <div>No indico id provided</div>;
}
