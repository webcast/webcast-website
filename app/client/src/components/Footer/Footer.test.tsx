import { Footer } from "./Footer";
import { renderWithAllProviders, screen } from "utils/test-utils";

describe("Footer component tests", () => {
  it("renders all the footer texts", () => {
    renderWithAllProviders(<Footer />);
    let element = screen.getByText(/About/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Webcast Service/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Webcast Test Channel/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Subscribe/i);
    expect(element).toBeInTheDocument();
    const liveEvents = screen.getAllByText(/Live events/i);
    expect(liveEvents).toHaveLength(2);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Upcoming events/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Contact$/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Report an issue/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Contact CERN/i);
    expect(element).toBeInTheDocument();
  });
});
