import { useEffect, useMemo, useRef } from "react";
import getBasicPluginContext from "paella-basic-plugins";
import { Paella, PlayerState, utils } from "paella-core";
import getUserTrackingPluginsContext from "paella-user-tracking";
import { Container } from "semantic-ui-react";
import {
  customLoadVideoManifestFunction,
  customtGetVideoIdFunction,
} from "./player-utils";
// import getCernCustomPluginsContext from "plugins";
import { ILayoutType } from "types/webcast";

interface IPlayerProps {
  layoutType: ILayoutType;
  showSelector?: boolean;
  isEmbed?: boolean;
  configPath: string;
  indico_id: string;
  displayFluid?: boolean;
  cameraStreamUrl: string;
  slidesStreamUrl: string;
  streamType: string;
  eventType: string;
}

export default function Player({
  layoutType,
  showSelector = true,
  isEmbed = false,
  configPath,
  indico_id,
  displayFluid = false,
  cameraStreamUrl,
  slidesStreamUrl,
  streamType,
  eventType,
}: Readonly<IPlayerProps>) {
  const videoRef = useRef<any>(null);
  const playerRef = useRef<any>(null);

  // Change the width to fluid when the user clicks on the full screen button
  const initParams = useMemo(() => {
    return {
      // Initialization callbacks
      configUrl: configPath,
      configResourcesUrl: "/images/",
      getVideoId: customtGetVideoIdFunction, // get the video identifier
      loadVideoManifest: () =>
        customLoadVideoManifestFunction(
          cameraStreamUrl,
          slidesStreamUrl,
          layoutType,
          streamType,
          indico_id,
          eventType,
        ), // get the manifest file content
      customPluginContext: [
        // getCernCustomPluginsContext(),
        getBasicPluginContext(),
        getUserTrackingPluginsContext(),
      ],
    };
  }, [
    cameraStreamUrl,
    configPath,
    indico_id,
    layoutType,
    slidesStreamUrl,
    streamType,
    eventType,
  ]);

  useEffect(() => {
    const handleDisplayFluid = () => {
      const player = playerRef.current;
      if (player) {
        console.log(`Player state: ${player.state}`);
        if (player.state === PlayerState.LOADED) {
          console.log("Resizing player...");
          player.resize();
        }
      }
    };
    handleDisplayFluid();
  }, [displayFluid]);

  useEffect(() => {
    const loadPaellaPlayer = async () => {
      if (!videoRef.current) {
        return;
      }
      if (!playerRef.current) {
        const player = new Paella(videoRef.current, initParams);
        playerRef.current = player;
        await playerRef.current.loadManifest();
        await utils.loadStyle("/config/style.css");
      }
    };
    loadPaellaPlayer();
    return () => {
      if (playerRef.current) {
        if (playerRef.current.state === PlayerState.LOADED) {
          playerRef.current.unload();
          playerRef.current = null;
        }
      }
    };
  }, [initParams]);

  if (!videoRef) {
    return <div>Loading player...</div>;
  }

  if (isEmbed) {
    return (
      <div
        data-testid="player-container"
        ref={videoRef}
        className="player-container"
        style={{
          width: "100%",
          marginTop: "60px",
          height: "100vh",
        }}
      />
    );
  }

  return (
    <Container
      fluid={displayFluid}
      style={{ marginTop: showSelector ? "" : 60 }}
    >
      <div
        data-testid="player-container"
        ref={videoRef}
        className="player-container"
        style={{
          minHeight: "600px",
          paddingBottom: displayFluid ? "56.25%" : 0,
        }}
      />
    </Container>
  );
}
