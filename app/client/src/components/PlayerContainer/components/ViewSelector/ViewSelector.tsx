import { NavLink } from "react-router-dom";
import { Icon, Menu } from "semantic-ui-react";
import { ILayoutType, IWebcastType } from "types/webcast";

interface IProps {
  layoutType: ILayoutType;
  streamType: IWebcastType;
  handleDisplayFluid: () => void;
  displayFluid: boolean;
  indico_id: string;
}
export default function ViewSelector({
  layoutType,
  streamType,
  handleDisplayFluid,
  displayFluid,
  indico_id,
}: IProps) {
  const toggleWidthAction = () => {
    handleDisplayFluid();
  };

  return (
    <Menu inverted>
      {streamType.toLowerCase() === "camera & slides" && (
        <Menu.Item as={NavLink} to={`/event/i${indico_id}`} end>
          <Icon name="video" />
          <Icon name="film" /> Main view
        </Menu.Item>
      )}
      {(streamType.toLowerCase() === "camera & slides" ||
        layoutType === "camera") && (
        <Menu.Item as={NavLink} to={`/event/i${indico_id}/camera`}>
          <Icon name="video" /> Camera
        </Menu.Item>
      )}
      {(streamType.toLowerCase() === "camera & slides" ||
        layoutType === "slides") && (
        <Menu.Item as={NavLink} to={`/event/i${indico_id}/slides`}>
          <Icon name="film" /> Slides
        </Menu.Item>
      )}
      <Menu.Menu position="right">
        <Menu.Item onClick={toggleWidthAction}>
          <Icon name={displayFluid ? "compress" : "expand"} />
        </Menu.Item>
      </Menu.Menu>
    </Menu>
  );
}
