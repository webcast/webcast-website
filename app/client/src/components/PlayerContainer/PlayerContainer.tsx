import { useMemo, useState } from "react";
import { Container, Grid } from "semantic-ui-react";
import { WebcastNotLiveContent } from "../../pages/WebcastPage/components/WebcastNotLiveContent/WebcastNotLiveContent";
import Player from "components/PlayerContainer/components/Player/Player";
import { getStreamUrl } from "components/PlayerContainer/components/Player/player-utils";
import ViewSelector from "components/PlayerContainer/components/ViewSelector/ViewSelector";
import config from "config";
import { ILayoutType, IWebcast } from "types/webcast";

interface Props {
  event: IWebcast;
  layoutType: ILayoutType;
  withCdn: boolean;
  code: string | undefined;
  showSelector?: boolean;
}

export default function PlayerContainer({
  code,
  layoutType,
  withCdn,
  event,
  showSelector = true,
}: Props) {
  const [displayFluid, setDisplayFluid] = useState(false);

  const handleDisplayFluid = () => {
    setDisplayFluid(!displayFluid);
  };

  const configPath = useMemo(() => {
    if (config.ENVIRONMENT === "DEV" || config.ENVIRONMENT === "QA") {
      return "/config/config-qa.json";
    }
    return "/config/config.json";
  }, []);

  const cameraStreamUrl = useMemo(() => {
    return getStreamUrl(withCdn, event.stream.camera_src, event.origin_server);
  }, [event.origin_server, event.stream.camera_src, withCdn]);

  const slidesStreamUrl = useMemo(() => {
    return getStreamUrl(withCdn, event.stream.slides_src, event.origin_server);
  }, [event.origin_server, event.stream.slides_src, withCdn]);

  if (cameraStreamUrl && slidesStreamUrl && configPath) {
    if (
      event.type.toLowerCase() === "live" ||
      (event.type.toLowerCase() === "recent" && event.has_dvr) ||
      (event.type.toLowerCase() === "upcoming" && code)
    ) {
      return (
        <Grid.Row>
          <Grid.Column>
            {showSelector && (
              <Container style={{ marginBottom: 20, marginTop: 20 }}>
                <ViewSelector
                  streamType={event.stream.type}
                  layoutType={layoutType}
                  handleDisplayFluid={handleDisplayFluid}
                  displayFluid={displayFluid}
                  indico_id={event.indico_id}
                />
              </Container>
            )}
            <Player
              configPath={configPath}
              layoutType={layoutType}
              cameraStreamUrl={cameraStreamUrl}
              slidesStreamUrl={slidesStreamUrl}
              streamType={event.stream.type}
              indico_id={event.indico_id}
              displayFluid={displayFluid}
              eventType={event.type.toLowerCase()}
            />
          </Grid.Column>
        </Grid.Row>
      );
    }
  }

  return (
    <Grid.Row>
      <Grid.Column>
        <WebcastNotLiveContent
          day={event.startDate.day}
          time={event.startDate.time}
          tz={event.startDate.tz}
          webcastType={event.type.toLowerCase()}
        />
      </Grid.Column>
    </Grid.Row>
  );
}
