import DevBanner from "./DevBanner";
import { renderWithAllProviders, screen } from "utils/test-utils";

describe("DevBanner component tests", () => {
  it("renders all the texts", () => {
    renderWithAllProviders(<DevBanner />);
    const element = screen.getByText(/This is a DEV Installation/i);
    expect(element).toBeInTheDocument();
  });
});
