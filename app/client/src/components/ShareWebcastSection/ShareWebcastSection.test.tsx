import React from "react";
import { render, screen } from "@testing-library/react";
import ShareWebcastSection from "./ShareWebcastSection";

describe("DevBanner component tests", () => {
  it("renders all the texts", () => {
    render(
      <ShareWebcastSection
        title="This is the embed URL"
        embedCode="https://webcast.web.cern.ch/embed/i12345"
      />,
    );
    let element = screen.getByText(/Twitter/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/https:\/\/webcast.web.cern.ch\/embed\/i12345/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Share/i);
    expect(element).toBeInTheDocument();
  });
});
