import React, { createContext, useEffect, useState } from "react";
import { useIsProtectedWebcast } from "hooks/webcasts/use-webcasts";
import WebcastRestrictionsError from "pages/WebcastPage/components/WebcastRestrictionsError/WebcastRestrictionsError";
import { IProtectedWebcastCheck } from "types/webcast";

type Props = {
  indicoId: string | undefined;
  children: React.ReactNode;
};

export const RestrictedContext = createContext<
  IProtectedWebcastCheck | undefined
>(undefined);

export default function RestrictedProvider({ indicoId, children }: Props) {
  const [cleanedIndicoId, setCleanedIndicoId] = useState<string | undefined>(
    undefined,
  );

  useEffect(() => {
    if (!indicoId) return;
    setCleanedIndicoId(indicoId.replace("i", ""));
  }, [indicoId]);

  const { data, error, isLoading, isError } =
    useIsProtectedWebcast(cleanedIndicoId);

  if (isError) {
    return (
      <WebcastRestrictionsError error={error} indicoId={cleanedIndicoId} />
    );
  }
  if (isLoading) {
    return <div>Checking webcast restrictions...</div>;
  }
  return (
    <RestrictedContext.Provider value={data}>
      {children}
    </RestrictedContext.Provider>
  );
}
