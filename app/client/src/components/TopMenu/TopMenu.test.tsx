import { render, screen } from "@testing-library/react";
import TopMenu from "./TopMenu";

describe("TopMenu component tests", () => {
  it("renders all the texts", () => {
    render(<TopMenu />);
    let element = screen.getByText(/CERN/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Accelerating Science/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Login/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Directory/i);
    expect(element).toBeInTheDocument();
  });
});
