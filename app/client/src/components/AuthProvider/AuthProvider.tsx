import React, { createContext, useEffect } from "react";
import AuthService from "services/auth-service";

type Props = {
  children: React.ReactNode;
};

export const AuthContext = createContext<boolean>(false);

export function AuthProvider({ children }: Props) {
  const [initialized, setInitialized] = React.useState(false);

  useEffect(() => {
    const onLoadCallback = () => {
      console.log("Keycloak initialized");
      setInitialized(true);
    };
    console.log("Initializing Keycloak...");
    AuthService.initKeycloak(onLoadCallback);
  }, []);

  return (
    <AuthContext.Provider value={initialized}>{children}</AuthContext.Provider>
  );
}
