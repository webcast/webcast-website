import React from "react";
import * as Sentry from "@sentry/react";
import ReactDOM from "react-dom/client";
import reportWebVitals from "./reportWebVitals";
import "semantic-ui-css-offline/semantic.min.css";
import "./index.css";
import setupInterceptors from "api/axios-interceptors";
import App from "App";
import { MediaContextProvider } from "app-media";
import { AuthProvider } from "components/AuthProvider/AuthProvider";
import config from "config";

if (import.meta.env.PROD === true) {
  console.debug = () => {
    /* do nothing */
  };
  Sentry.init({
    dsn: config.SENTRY_DSN,
    environment: config.SENTRY_ENV,
    // Performance Monitoring
    tracesSampleRate: 1.0, //  Capture 100% of the transactions
  });
}

if (import.meta.env.DEV === true) {
  // eslint-disable-next-line @typescript-eslint/no-require-imports
  const axe = require("@axe-core/react");
  axe(React, ReactDOM, 1000);
}

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement,
);

setupInterceptors();

root.render(
  <AuthProvider>
    <React.StrictMode>
      <MediaContextProvider>
        <App />
      </MediaContextProvider>
    </React.StrictMode>
  </AuthProvider>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
