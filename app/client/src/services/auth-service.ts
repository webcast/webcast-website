import keycloak from "keycloak";

const initKeycloak = (onLoadCallback: (success: boolean) => void) => {
  keycloak.onReady = (authenticated: boolean) => {
    console.log("Keycloak is ready:", authenticated);
    onLoadCallback(authenticated);
  };
  keycloak
    .init({
      onLoad: "check-sso",
      silentCheckSsoRedirectUri: encodeURI(
        `${window.location.origin}/silent-check-sso.html`,
      ),
    })
    .then((authenticated: boolean) => {
      if (!authenticated) {
        console.log("User is not authenticated");
      }
    })
    .catch(console.error);
};

const doLogin = keycloak.login;

const doLogout = keycloak.logout;

const getToken = () => {
  return keycloak.token;
};

const isLoggedIn = () => {
  return !!keycloak.token;
};

const updateToken = (successCallback: () => void) => {
  return keycloak.updateToken(30).then(successCallback).catch(doLogin);
};

const getUsername = () => {
  const tokenParsed = keycloak.tokenParsed;
  return tokenParsed?.preferred_username;
};

const getFirstName = () => {
  const tokenParsed = keycloak.tokenParsed;
  return tokenParsed?.given_name;
};

const getLastName = () => {
  const tokenParsed = keycloak.tokenParsed;
  return tokenParsed?.family_name;
};

const hasRole = (roles: string[]) => {
  return roles.some((role) => keycloak.hasRealmRole(role));
};

export default {
  initKeycloak,
  doLogin,
  doLogout,
  isLoggedIn,
  getToken,
  updateToken,
  getUsername,
  getFirstName,
  getLastName,
  hasRole,
};
