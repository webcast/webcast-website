import { useMemo } from "react";
import { Link, useParams } from "react-router-dom";
import {
  Dimmer,
  Header,
  Icon,
  Label,
  Loader,
  Segment,
} from "semantic-ui-react";
import ShareWebcastSection from "components/ShareWebcastSection/ShareWebcastSection";
import {
  usePermanentWebcast,
  usePermanentWebcastCategories,
} from "hooks/permanent-webcasts/use-permanent-webcasts";
import PermanentWebcastPlayer from "pages/PermanentWebcastsPage/components/PermanentWebcastPlayer/PermanentWebcastPlayer";

export default function PermanentWebcastDetailsPage() {
  const { streamId } = useParams();
  const {
    data: categoriesData,
    isLoading: isLoadingCategory,
    isError: isErrorCategories,
    error: errorCategories,
  } = usePermanentWebcastCategories();
  const { data, isError, isLoading, error } = usePermanentWebcast(streamId);

  const selectedCategory = useMemo(() => {
    if (
      categoriesData &&
      categoriesData.results.length > 0 &&
      streamId !== undefined
    ) {
      return categoriesData.results.find(
        (x) => x.id === parseInt(streamId, 10),
      );
    }
    return undefined;
  }, [categoriesData, streamId]);

  if (isLoading || isLoadingCategory) {
    return (
      <Segment basic textAlign="center">
        <Dimmer active inverted>
          <Loader />
        </Dimmer>
      </Segment>
    );
  }

  if (isError || isErrorCategories) {
    return (
      <Segment basic textAlign="center">
        Unable to fetch the webcast details
        {isError && error.message}
        {isErrorCategories && errorCategories.message}
      </Segment>
    );
  }

  if (data && data.result && !data.success) {
    return (
      <Segment basic textAlign="center">
        You cannot access the permanent stream.
      </Segment>
    );
  }

  if (data && selectedCategory && data.result && data.success) {
    return (
      <div>
        <Header as="h3">
          <Icon name="video" />
          <Header.Content>
            {data.result.title} <Label>{selectedCategory.name}</Label>
          </Header.Content>
        </Header>
        <p>
          <Link to={`/permanent-webcasts/${selectedCategory.id}`}>
            <Icon name="arrow left" /> Back to category
          </Link>
        </p>
        <PermanentWebcastPlayer webcast={data.result} />

        <ShareWebcastSection
          title={data.result.title}
          embedCode={data.result.stream.embed_code}
          inverted={false}
        />
      </div>
    );
  }
  return null;
}
