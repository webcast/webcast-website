import { ReactNode, useEffect } from "react";
import ResponsiveContainer from "../ResponsiveContainer";
import HomepageHeading from "./components/HomePageHeading/HomePageHeading";

interface IProps {
  children: ReactNode;
}

function HomeResponsiveContainer({ children }: IProps) {
  useEffect(() => {
    document.body.style.backgroundColor = "white";
  }, []);
  return (
    <ResponsiveContainer headerComponent={<HomepageHeading />}>
      {children}
    </ResponsiveContainer>
  );
}

export default HomeResponsiveContainer;
