import UpcomingWebcasts from "./UpcomingWebcasts";
import { mockWebcastList } from "mocks/responses/webcasts";
import { renderWithAllProviders, screen } from "utils/test-utils";

describe("UpcomingWebcasts component tests", () => {
  it("renders all the texts", async () => {
    renderWithAllProviders(<UpcomingWebcasts />);
    let element = await screen.findByText(mockWebcastList.results[0].title);
    expect(element).toBeInTheDocument();
    element = screen.getByText(mockWebcastList.results[1].title);
    expect(element).toBeInTheDocument();

    let elements = screen.getAllByText(/Europe\/Zurich/);
    expect(elements.length).toBe(6);

    elements = screen.getAllByText(/More Information/);
    expect(elements.length).toBe(6);

    elements = screen.getAllByText(/by/);
    expect(elements.length).toBe(4);
  });
});
