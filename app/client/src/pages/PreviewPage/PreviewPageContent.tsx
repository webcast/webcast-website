import PublicWebcastProvider from "pages/WebcastPage/components/PublicWebcast/PublicWebcastProvider";

type Props = {
  indicoId: string;
  code: string;
};

export default function PreviewPageContent({ indicoId, code }: Props) {
  return (
    <PublicWebcastProvider
      indicoId={`p${code}-${indicoId}`}
      layoutType="dual"
      code={code}
    />
  );
}
