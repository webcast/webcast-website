import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { Segment, Grid } from "semantic-ui-react";
import PreviewPageContent from "./PreviewPageContent";
import TopMenu from "components/TopMenu/TopMenu";
import { useMatomoPageView } from "hooks/matomo/use-matomo";
import ResponsiveContainer from "pages/ResponsiveContainer";

export default function PreviewPage() {
  const { id, code } = useParams();
  useMatomoPageView();

  useEffect(() => {
    document.body.style.backgroundColor = "#060606";
  }, []);

  if (id && code) {
    return (
      <ResponsiveContainer>
        <TopMenu />
        <Segment basic padded>
          <Grid.Row>
            <Grid.Column>
              <PreviewPageContent indicoId={id} code={code} />
            </Grid.Column>
          </Grid.Row>
        </Segment>
      </ResponsiveContainer>
    );
  }
  return <div>No indico id provided</div>;
}
