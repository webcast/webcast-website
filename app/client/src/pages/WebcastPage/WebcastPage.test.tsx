import WebcastPage from "./WebcastPage";
import { RestrictedContext } from "components/RestrictedProvider/RestrictedProvider";
import { renderWithAllProviders, screen, waitFor } from "utils/test-utils";

vi.mock("react-router-dom", () => ({
  useParams: () => ({
    id: "1127820",
  }),
  MemoryRouter: vi.fn().mockImplementation((props) => props.children),
  Link: vi.fn().mockImplementation(({ children }) => children),
}));
// vi.mock('providers/SomeContextProvider');
vi.mock("components/RestrictedProvider/RestrictedProvider");

describe("WebcastPage component tests", () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it("renders all the texts", async () => {
    // vi.spyOn(Router, "useParams").mockReturnValue({ indicoId: "1160270" });

    renderWithAllProviders(
      <RestrictedContext.Provider
        value={{
          success: true,
          result: {
            is_restricted: false,
          },
        }}
      >
        <WebcastPage />
      </RestrictedContext.Provider>,
    );
    await waitFor(() => {
      const element = screen.getByText(
        /Nonperturbative Methods in Quantum Field Theory/i,
      );
      expect(element).toBeInTheDocument();
    });
  });
});
