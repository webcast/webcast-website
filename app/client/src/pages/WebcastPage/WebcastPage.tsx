import { useContext } from "react";
import { useParams } from "react-router-dom";
import ProtectedWebcastProvider from "./components/ProtectedWebcast/ProtectedWebcastProvider";
import PublicWebcastProvider from "./components/PublicWebcast/PublicWebcastProvider";
import { RestrictedContext } from "components/RestrictedProvider/RestrictedProvider";
import { ILayoutType } from "types/webcast";

interface WebcastPageProps {
  withCdn?: boolean;
  layoutType?: ILayoutType;
}

export default function WebcastPage({
  withCdn,
  layoutType = "dual",
}: WebcastPageProps) {
  const { id } = useParams();

  const restrictions = useContext(RestrictedContext);

  if (!restrictions || !restrictions.success) {
    console.log("No restrictions");
    return null;
  }

  if (restrictions.result.is_restricted && id) {
    console.log("Restricted");
    return (
      <ProtectedWebcastProvider
        indicoId={id}
        layoutType={layoutType}
        withCdn={withCdn}
      />
    );
  }

  if (!restrictions.result.is_restricted && id) {
    console.log("Not restricted");
    return (
      <PublicWebcastProvider
        indicoId={id}
        layoutType={layoutType}
        withCdn={withCdn}
      />
    );
  }
  return <div>No indico id provided</div>;
}
