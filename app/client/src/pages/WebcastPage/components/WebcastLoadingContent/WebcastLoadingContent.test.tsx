import WebcastLoadingContent from "./WebcastLoadingContent";
import { renderWithAllProviders, screen } from "utils/test-utils";

describe("WebcastLoadingContent component tests", () => {
  it("renders the loader", async () => {
    renderWithAllProviders(<WebcastLoadingContent />);

    const element = screen.getByTestId(/loader/i);
    expect(element).toBeInTheDocument();
  });
});
