import { render, screen } from "@testing-library/react";
import WebcastErrorContent from "./WebcastErrorContent";

describe("WebcastErrorContent component tests", () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it("renders the login error", async () => {
    const error: any = { response: { status: 401 } };
    render(<WebcastErrorContent error={error} indicoId="12345" />);

    const element = screen.getByText(
      /You need to log in to access this webcast/i,
    );
    expect(element).toBeInTheDocument();
  });
  it("renders the normal error", async () => {
    const error: any = { response: { status: 500 } };

    render(<WebcastErrorContent error={error} indicoId="12345" />);

    const element = screen.getByText(/Unable to fetch the selected webcast/i);
    expect(element).toBeInTheDocument();
  });
});
