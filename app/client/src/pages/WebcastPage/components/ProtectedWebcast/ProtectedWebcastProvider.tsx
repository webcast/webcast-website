import { useEffect } from "react";
import WebcastErrorContent from "../WebcastErrorContent/WebcastErrorContent";
import WebcastLoadingContent from "../WebcastLoadingContent/WebcastLoadingContent";
import ProtectedWebcast from "./ProtectedWebcast";
import { useWebcast } from "hooks/webcasts/use-webcasts";
import AuthService from "services/auth-service";
import { ILayoutType } from "types/webcast";

interface Props {
  indicoId: string;
  layoutType: ILayoutType;
  withCdn?: boolean;
}

export default function ProtectedWebcastProvider({
  indicoId,
  layoutType,
  withCdn = false,
}: Readonly<Props>) {
  const { data, error, isLoading, isError, refetch } = useWebcast(indicoId);

  useEffect(() => {
    if (data) {
      document.title = `${data.result.title} - CERN Live Events Website (Webcast)`;
    }
  }, [data]);

  useEffect(() => {
    // Refetch the webcast after the user logs in
    if (AuthService.isLoggedIn()) {
      refetch();
    }
  }, [refetch]);

  if (isError) {
    return <WebcastErrorContent error={error} indicoId={indicoId} />;
  }

  if (!data?.success) {
    // Is not allowed
    return (
      <div data-testid="protected-webcast">
        <WebcastErrorContent error={error} indicoId={indicoId} />
      </div>
    );
  }

  if (isLoading) {
    return <WebcastLoadingContent />;
  }

  if (data && data.result) {
    return (
      <div data-testid="public-webcast">
        <ProtectedWebcast
          webcast={data.result}
          layoutType={layoutType}
          withCdn={withCdn}
        />
      </div>
    );
  }
}
