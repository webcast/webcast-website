import WebcastContent from "../WebcastContent/WebcastContent";
import { ILayoutType, IWebcast } from "types/webcast";

interface Props {
  webcast: IWebcast;
  layoutType: ILayoutType;
  withCdn?: boolean;
}

export default function ProtectedWebcast({
  webcast,
  layoutType,
  withCdn = false,
}: Readonly<Props>) {
  return (
    <div data-testid="protected-webcast">
      <WebcastContent
        webcast={webcast}
        layoutType={layoutType}
        withCdn={withCdn}
      />
    </div>
  );
}
