import ProtectedWebcast from "./ProtectedWebcast";
import { webcastDetailsMockRestricted } from "mocks/responses/webcast-details";
import { renderWithAllProviders, waitFor, screen } from "utils/test-utils";

// vi.mock("react-router-dom", () => ({
//   useParams: () => ({
//     indicoId: "1160270",
//   }),
//   MemoryRouter: vi.fn().mockImplementation((props) => props.children),
// }));

describe("ProtectedWebcast component tests recent", () => {
  // afterEach(() => {
  //   vi.clearAllMocks();
  // });

  it("renders the webcast texts", async () => {
    renderWithAllProviders(
      <ProtectedWebcast
        webcast={webcastDetailsMockRestricted.result}
        layoutType={"dual"}
      />,
    );

    await waitFor(() => {
      const element = screen.getByText(/ATLAS Weekly/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/Abstract/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/iframe/i);
      expect(element).toBeInTheDocument();
    });

    await waitFor(() => {
      const element = screen.getByText(/This event has not started yet/i);
      expect(element).toBeInTheDocument();
    });
  });
});
