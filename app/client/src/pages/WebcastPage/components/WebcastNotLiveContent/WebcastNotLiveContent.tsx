import { DateTime } from "luxon";
import { Container, Icon, Message, Segment } from "semantic-ui-react";

interface NotStartedProps {
  webcastType: string;
  day: string;
  time: string;
  tz: string;
}

export function WebcastNotLiveContent({
  webcastType,
  day,
  time,
  tz,
}: Readonly<NotStartedProps>) {
  // Build the date and time of the event
  const eventDateTime = DateTime.fromISO(`${day}T${time}`);
  // const finished = nowPlus2Hours > eventDateTime;
  const finished = webcastType === "recent" || webcastType === "archived";

  let message = "";
  let text = `If the event has been recorded, it will be available shortly on CDS.`;
  if (finished) {
    message = "This event has already finished";
  } else {
    message = "This event has not started yet";
    text = `This event will start on ${eventDateTime.toFormat(
      "dd-MM-yyyy",
    )} at ${eventDateTime.toFormat(
      "HH:mm",
    )} (${tz}). You will need to reload the page.`;
  }

  return (
    <Container>
      <Segment inverted>
        <Message info size="large">
          <Message.Header>
            <Icon name="info" /> {message}
          </Message.Header>
          <p>{text}</p>
        </Message>
      </Segment>
    </Container>
  );
}

export default WebcastNotLiveContent;
