import React from "react";
import { render, screen } from "@testing-library/react";
import WebcastInfoSection from "./WebcastInfoSection";

describe("AbstractSection component tests", () => {
  it("renders all the texts", async () => {
    const webcast = {
      abstract: "This is the abstract of the webcast",
      title: "This is the title of the webcast",
      url: "https://webcast.web.cern.ch",
      startDate: {
        day: "2021-11-16",
        tz: "Europe/Zurich",
        time: "11:00:00",
      },
      endDate: {
        day: "2021-11-16",
        tz: "Europe/Zurich",
        time: "12:00:00",
      },
      speakers: "Speaker 1, Speaker 2",
      room_name: "Room A",
      indico_id: "123456789",
      indico_link: "https://indico.cern.ch/event/123456789/",
    };
    render(<WebcastInfoSection webcast={webcast} />);
    let element = screen.getByText(/Location/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Room A/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Speaker\(s\)/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Speaker 1/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Speaker 2/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Date/i);
    expect(element).toBeInTheDocument();
  });
});
