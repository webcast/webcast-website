import AbstractSection from "./AbstractSection";
import { render, screen } from "utils/test-utils";

describe("AbstractSection component tests", () => {
  it("renders all the texts", async () => {
    const webcast = {
      abstract: "This is the abstract of the webcast",
      title: "This is the title of the webcast",
      indico_link: "https://indico.cern.ch/event/123456789/",
      embed_code: "",
      streams: [],
    };

    render(<AbstractSection webcast={webcast} inverted />);

    let element = screen.getByText(/Abstract$/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/This is the abstract of the webcast/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/You can visit the/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/Indico page/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(/for more information about this event./i);
    expect(element).toBeInTheDocument();
  });
});
