import { useEffect } from "react";
import WebcastErrorContent from "../WebcastErrorContent/WebcastErrorContent";
import WebcastLoadingContent from "../WebcastLoadingContent/WebcastLoadingContent";
import PublicWebcast from "./PublicWebcast";
import { usePublicWebcast } from "hooks/webcasts/use-webcasts";
import { ILayoutType } from "types/webcast";

interface Props {
  indicoId: string;
  layoutType: ILayoutType;
  withCdn?: boolean;
  code?: string;
}

export default function PublicWebcastProvider({
  indicoId,
  layoutType,
  withCdn = false,
  code = undefined,
}: Readonly<Props>) {
  const { data, error, isLoading, isError } = usePublicWebcast(indicoId);

  useEffect(() => {
    if (data) {
      document.title = `${data.result.title} - CERN Live Events Website (Webcast)`;
    }
  }, [data]);

  if (isError) {
    return <WebcastErrorContent error={error} indicoId={indicoId} />;
  }

  if (isLoading) {
    return <WebcastLoadingContent />;
  }

  if (data && data.result) {
    return (
      <div data-testid="public-webcast">
        <PublicWebcast
          webcast={data.result}
          layoutType={layoutType}
          withCdn={withCdn}
          code={code}
        />
      </div>
    );
  }
}
