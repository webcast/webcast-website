import WebcastContent from "../WebcastContent/WebcastContent";
import { ILayoutType, IWebcast } from "types/webcast";

interface Props {
  webcast: IWebcast;
  layoutType: ILayoutType;
  withCdn?: boolean;
  code?: string;
}

export default function PublicWebcast({
  webcast,
  layoutType,
  withCdn = false,
  code = undefined,
}: Readonly<Props>) {
  if (webcast) {
    return (
      <div data-testid="public-webcast">
        <WebcastContent
          webcast={webcast}
          layoutType={layoutType}
          withCdn={withCdn}
          code={code}
        />
      </div>
    );
  }
}
