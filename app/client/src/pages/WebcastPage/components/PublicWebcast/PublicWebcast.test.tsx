import { screen, waitFor } from "@testing-library/react";
import PublicWebcast from "./PublicWebcast";
import { webcastDetailsMockUnrestricted } from "mocks/responses/webcast-details";
import { renderWithAllProviders } from "utils/test-utils";

// vi.mock("react-router-dom", () => ({
//   useParams: () => ({
//     indicoId: "1127820",
//   }),
//   MemoryRouter: vi.fn().mockImplementation((props) => props.children),
// }));

describe("PublicWebcast component tests not expired", () => {
  it("renders the webcast texts", async () => {
    renderWithAllProviders(
      <PublicWebcast
        webcast={webcastDetailsMockUnrestricted.result}
        layoutType="camera"
      />,
    );

    await waitFor(() => {
      const element = screen.getByText(
        /Nonperturbative Methods in Quantum Field Theory/i,
      );
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/Abstract/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getByText(/iframe/i);
      expect(element).toBeInTheDocument();
    });
    await waitFor(() => {
      const element = screen.getAllByText(/Europe\/Zurich/i);
      expect(element).toHaveLength(1);
    });
    await waitFor(() => {
      const element = screen.getByText(/This event has not started yet/i);
      expect(element).toBeInTheDocument();
    });
  });
});
