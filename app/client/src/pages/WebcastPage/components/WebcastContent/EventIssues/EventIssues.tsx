import { Icon, Message, MessageItem, MessageList } from "semantic-ui-react";

export default function EventIssues() {
  return (
    <Message style={{ marginTop: 20, marginBottom: 20 }} color="blue">
      <Message.Header>
        <Icon name="warning" /> Issues during the playback?
      </Message.Header>
      <MessageList>
        <MessageItem>
          <a
            href="https://cern.service-now.com/service-portal?id=sc_cat_item&name=issue-live-events-website&se=webcast"
            title="CERN Live Events website report form"
            target="_blank"
            rel="noreferrer"
            style={{ textDecoration: "underline" }}
          >
            Contact us using this form.
          </a>
        </MessageItem>
      </MessageList>
    </Message>
  );
}
