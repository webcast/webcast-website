import React from "react";
import { Media } from "app-media";
import ShareWebcastSection from "components/ShareWebcastSection/ShareWebcastSection";
import AbstractSection from "pages/WebcastPage/components/AbstractSection/AbstractSection";
import WebcastInfoSection from "pages/WebcastPage/components/WebcastInfoSection/WebcastInfoSection";
import { IWebcast } from "types/webcast";

type Props = {
  webcast: IWebcast;
};

export default function EventMeta({ webcast }: Props) {
  return (
    <>
      <Media lessThan="tablet">
        <WebcastInfoSection webcast={webcast} />
      </Media>
      <Media greaterThanOrEqual="tablet">
        <WebcastInfoSection webcast={webcast} horizontal />
      </Media>
      <AbstractSection webcast={webcast} inverted />
      <ShareWebcastSection
        title={webcast.title}
        embedCode={webcast.embed_code}
        inverted
      />
    </>
  );
}
