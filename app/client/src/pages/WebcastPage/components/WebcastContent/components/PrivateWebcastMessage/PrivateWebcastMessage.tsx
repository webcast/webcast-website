import { Message } from "semantic-ui-react";

export function PrivateWebcastMessage() {
  return (
    <Message color="yellow">
      <span aria-label="Warning icon">⚠️ </span>
      <strong>This is a private view of the event.</strong>
    </Message>
  );
}
