import { List, Image } from "semantic-ui-react";

type Props = { indicoLink: string };

export default function EventIndico({ indicoLink }: Props) {
  return (
    <List inverted>
      <List.Item>
        <List.Content>
          <List.Header>Event page</List.Header>
          <a href={indicoLink} title="Event page on Indico">
            <Image
              src="/images-client/indico_white.png"
              alt="Indico Logo"
              size="small"
              className="indicoImageLink"
            />
          </a>
        </List.Content>
      </List.Item>
    </List>
  );
}
