import { Container, Grid, Icon, Message } from "semantic-ui-react";

type Props = { extra_html: string };

export default function HtmlMessage({ extra_html }: Props) {
  return (
    <Container>
      <Grid columns={1}>
        <Grid.Row>
          <Grid.Column>
            {extra_html !== "" && extra_html !== null && (
              <Message info>
                <Icon name="info" />{" "}
                <span
                  // eslint-disable-next-line react/no-danger
                  dangerouslySetInnerHTML={{
                    __html: extra_html,
                  }}
                />
              </Message>
            )}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}
