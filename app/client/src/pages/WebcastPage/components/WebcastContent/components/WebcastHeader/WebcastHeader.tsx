import Latex from "react-latex-next";
import { Container, Grid, Header, Icon } from "semantic-ui-react";
import { PrivateWebcastMessage } from "../PrivateWebcastMessage/PrivateWebcastMessage";
import { IWebcast } from "types/webcast";

export function WebcastHeader({
  webcast,
  code,
}: Readonly<{
  webcast: IWebcast;
  code: string | undefined;
}>) {
  return (
    <Container>
      <Grid columns={1}>
        <Grid.Row>
          <Grid.Column>
            {code && <PrivateWebcastMessage />}
            <Header inverted as="h2" dividing>
              <Icon name="video" />
              <Header.Content>
                <Latex>{webcast.title}</Latex>
                {webcast.speakers && (
                  <Header.Subheader>by {webcast.speakers}</Header.Subheader>
                )}
              </Header.Content>
            </Header>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}
