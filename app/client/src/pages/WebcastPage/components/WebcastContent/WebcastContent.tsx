import { Container, Grid } from "semantic-ui-react";
import PlayerContainer from "../../../../components/PlayerContainer/PlayerContainer";
import EventIndico from "./components/EventIndico/EventIndico";
import EventMeta from "./components/EventMeta/EventMeta";
import HtmlMessage from "./components/HtmlMessage/HtmlMessage";
import { WebcastHeader } from "./components/WebcastHeader/WebcastHeader";
import EventIssues from "./EventIssues/EventIssues";
import { ILayoutType, IWebcast } from "types/webcast";

interface Props {
  webcast: IWebcast;
  code?: string;
  layoutType: ILayoutType;
  withCdn?: boolean;
}

export default function WebcastContent({
  webcast,
  code = undefined,
  layoutType,
  withCdn = false,
}: Readonly<Props>) {
  return (
    <>
      <WebcastHeader webcast={webcast} code={code} />
      <HtmlMessage extra_html={webcast.extra_html} />
      <PlayerContainer
        code={code}
        layoutType={layoutType}
        withCdn={withCdn}
        event={webcast}
      />
      <Container>
        <Grid stackable>
          <Grid.Row>
            <Grid.Column width={12}>
              <EventMeta webcast={webcast} />
            </Grid.Column>
            <Grid.Column width={4}>
              <EventIssues webcast={webcast} />
              <EventIndico indicoLink={webcast.indico_link} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </>
  );
}
