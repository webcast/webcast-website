import { useEffect } from "react";
import { NavLink, Outlet } from "react-router-dom";
import {
  Grid,
  Menu,
  Segment,
  Container,
  Header,
  Dimmer,
  Loader,
} from "semantic-ui-react";
import TopMenu from "components/TopMenu/TopMenu";
import { useMatomoPageView } from "hooks/matomo/use-matomo";
import { usePermanentWebcastCategories } from "hooks/permanent-webcasts/use-permanent-webcasts";
import ResponsiveContainer from "pages/ResponsiveContainer";

export default function PermanentWebcasts() {
  const { data, isLoading, isError, error } = usePermanentWebcastCategories();
  useMatomoPageView();

  useEffect(() => {
    document.body.style.backgroundColor = "white";
  }, []);

  if (isLoading) {
    return (
      <Segment basic textAlign="center">
        <Dimmer active inverted>
          <Loader />
        </Dimmer>
      </Segment>
    );
  }

  if (isError) {
    return (
      <Segment basic textAlign="center">
        Unable to fetch the webcast details
        {error.message}
      </Segment>
    );
  }

  if (data && data.results) {
    return (
      <ResponsiveContainer>
        <TopMenu />
        <Segment basic>
          <Container>
            <Grid columns={1}>
              <Grid.Row>
                <Grid.Column>
                  <Header
                    as="h2"
                    content="Permanent Live Events"
                    icon="video"
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={4}>
                  <Menu fluid vertical stackable>
                    {data &&
                      data.results.map((category) => (
                        <Menu.Item
                          key={`cat-${category.id.toString()}`}
                          as={NavLink}
                          to={`${category.id}`}
                          name={`${category.name} ${category.id.toString()}`}
                        />
                      ))}
                  </Menu>
                </Grid.Column>

                <Grid.Column stretched width={12}>
                  <Segment>
                    <Outlet />
                  </Segment>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
        </Segment>
      </ResponsiveContainer>
    );
  }
}
