import { useEffect } from "react";
import { useParams } from "react-router-dom";
import PermanentEmbedPageContent from "./PermanentEmbedPageContent";
import RestrictedProvider from "components/RestrictedProvider/RestrictedProvider";
import TopMenu from "components/TopMenu/TopMenu";

export default function EmbedPage() {
  const { id } = useParams();

  useEffect(() => {
    document.body.style.backgroundColor = "#060606";
  }, []);
  if (id) {
    return (
      <RestrictedProvider indicoId={id}>
        <TopMenu />
        <PermanentEmbedPageContent indicoId={id} />
      </RestrictedProvider>
    );
  }
  return <div>No indico id provided</div>;
}
