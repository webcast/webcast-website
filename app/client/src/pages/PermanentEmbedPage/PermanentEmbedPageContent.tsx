import { useContext } from "react";
import ProtectedPermanentEmbedProvider from "./components/ProtecedPermanentEmbed/ProtectedPermanentEmbedProvider";
import PublicPermanentEmbedProvider from "./components/PublicPermanentEmbed/PublicPermanentEmbedProvider";
import RestrictedProvider, {
  RestrictedContext,
} from "components/RestrictedProvider/RestrictedProvider";
import TopMenu from "components/TopMenu/TopMenu";

interface Props {
  indicoId: string;
}

export default function PermanentEmbedPageContent({ indicoId }: Props) {
  const restrictions = useContext(RestrictedContext);

  if (!restrictions || !restrictions.success) {
    console.log("No restrictions");
    return null;
  }

  if (restrictions.result.is_restricted) {
    console.log("Restricted");
    return (
      <RestrictedProvider indicoId={indicoId}>
        <TopMenu />
        <ProtectedPermanentEmbedProvider indicoId={indicoId} />
      </RestrictedProvider>
    );
  }

  if (!restrictions.result.is_restricted) {
    console.log("Not restricted");
    return (
      <RestrictedProvider indicoId={indicoId}>
        <TopMenu />
        <PublicPermanentEmbedProvider indicoId={indicoId} />
      </RestrictedProvider>
    );
  }
}
