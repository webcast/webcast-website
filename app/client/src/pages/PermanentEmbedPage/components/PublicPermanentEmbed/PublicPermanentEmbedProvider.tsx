import { useEffect } from "react";
import PublicPermanentEmbed from "./PublicPermanentEmbed";
import { usePermanentWebcast } from "hooks/permanent-webcasts/use-permanent-webcasts";
import WebcastErrorContent from "pages/WebcastPage/components/WebcastErrorContent/WebcastErrorContent";
import WebcastLoadingContent from "pages/WebcastPage/components/WebcastLoadingContent/WebcastLoadingContent";

interface Props {
  indicoId: string;
}
export default function PublicPermanentEmbedProvider({ indicoId }: Props) {
  const { data, error, isLoading, isError } = usePermanentWebcast(indicoId);

  useEffect(() => {
    if (data) {
      document.title = `${data.result.title} - CERN Live Events Website (Webcast)`;
    }
  }, [data]);

  if (isError) {
    return <WebcastErrorContent error={error} indicoId={indicoId} />;
  }

  if (isLoading) {
    return <WebcastLoadingContent />;
  }

  if (data && data.result) {
    return (
      <div data-testid="public-webcast">
        <PublicPermanentEmbed webcast={data.result} />
      </div>
    );
  }
}
