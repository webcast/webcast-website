import { useEffect } from "react";
import ProtectedPermanentEmbed from "./ProtectedPermanentEmbed";
import { usePermanentWebcast } from "hooks/permanent-webcasts/use-permanent-webcasts";
import WebcastErrorContent from "pages/WebcastPage/components/WebcastErrorContent/WebcastErrorContent";
import WebcastLoadingContent from "pages/WebcastPage/components/WebcastLoadingContent/WebcastLoadingContent";
import AuthService from "services/auth-service";

interface Props {
  indicoId: string;
}

export default function ProtectedEmbedProvider({ indicoId }: Props) {
  const { data, error, isError, isLoading, refetch } =
    usePermanentWebcast(indicoId);

  useEffect(() => {
    // Refetch the webcast after the user logs in
    if (AuthService.isLoggedIn()) {
      refetch();
    }
  }, [refetch]);

  if (isError) {
    return <WebcastErrorContent error={error} indicoId={indicoId} />;
  }

  if (!data?.success) {
    // Is not allowed
    return (
      <div data-testid="protected-webcast">
        <WebcastErrorContent error={error} indicoId={indicoId} />
      </div>
    );
  }

  if (isLoading) {
    return <WebcastLoadingContent />;
  }

  if (data && data.result) {
    return <ProtectedPermanentEmbed webcast={data.result} />;
  }
  return null;
}
