import { useEffect } from "react";
import PermanentEmbedPlayer from "../PermanentEmbedPlayer/PermanentEmbedPlayer";
import { PermanentWebcast } from "types/permanent-webcast";

interface Props {
  webcast: PermanentWebcast;
}

export default function ProtectedPermanentEmbed({ webcast }: Props) {
  useEffect(() => {
    document.body.style.backgroundColor = "#060606";
    if (webcast) {
      document.title = `${webcast.title} - CERN Live Events Website (Webcast)`;
    }
  }, [webcast]);

  return (
    <div data-testid="public-webcast">
      <div>
        <PermanentEmbedPlayer
          title={webcast.title}
          streamSrc={webcast.stream.camera_src}
        />
      </div>
    </div>
  );
}
