import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { Card } from "semantic-ui-react";
import { usePermanentWebcastCategories } from "hooks/permanent-webcasts/use-permanent-webcasts";
import {
  PermanentWebcastCategoryItem,
  PermanentWebcastStreamItem,
} from "types/permanent-webcast";

function PermanentWebcastsListPage() {
  const { categoryId } = useParams();
  const { data } = usePermanentWebcastCategories();

  const [selectedCategory, setSelectedCategory] = useState<
    PermanentWebcastCategoryItem | undefined
  >(undefined);

  useEffect(() => {
    if (data && data.results.length > 0 && categoryId !== undefined) {
      const category = data.results.find(
        (x) => x.id === parseInt(categoryId, 10),
      );
      setSelectedCategory(category);
    }
  }, [data, categoryId]);

  if (!selectedCategory) {
    return null;
  }

  if (selectedCategory) {
    return (
      <div>
        {selectedCategory &&
          selectedCategory.streams.map(
            (webcast: PermanentWebcastStreamItem) => (
              <Card
                key={`webcast-${webcast.id}`}
                as={Link}
                to={`stream/${webcast.id}`}
                header={webcast.title}
                image={webcast.default_img}
              />
            ),
          )}
      </div>
    );
  }
  return null;
}

export default PermanentWebcastsListPage;
