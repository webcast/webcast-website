import { useContext } from "react";
import ProtectedEmbedProvider from "./components/ProtecedEmbed/ProtectedEmbedProvider";
import PublicEmbedProvider from "./components/PublicEmbed/PublicEmbedProvider";
import { RestrictedContext } from "components/RestrictedProvider/RestrictedProvider";

type Props = { indicoId: string };

export default function EmbedPageContent({ indicoId }: Props) {
  const restrictions = useContext(RestrictedContext);

  if (!restrictions || !restrictions.success) {
    return null;
  }

  if (restrictions.result.is_restricted) {
    return <ProtectedEmbedProvider indicoId={indicoId} layoutType="dual" />;
  }

  if (!restrictions.result.is_restricted) {
    return <PublicEmbedProvider indicoId={indicoId} layoutType="dual" />;
  }

  return null;
}
