import { useEffect } from "react";
import { useParams } from "react-router-dom";
import EmbedPageContent from "./EmbedPageContent";
import RestrictedProvider from "components/RestrictedProvider/RestrictedProvider";
import TopMenu from "components/TopMenu/TopMenu";
import { useMatomoPageView } from "hooks/matomo/use-matomo";

export default function EmbedPage() {
  const { id } = useParams();
  useMatomoPageView();

  useEffect(() => {
    document.body.style.backgroundColor = "#060606";
  }, []);
  if (id) {
    return (
      <RestrictedProvider indicoId={id}>
        <TopMenu />
        <EmbedPageContent indicoId={id} />
      </RestrictedProvider>
    );
  }
  return <div>No indico id provided</div>;
}
