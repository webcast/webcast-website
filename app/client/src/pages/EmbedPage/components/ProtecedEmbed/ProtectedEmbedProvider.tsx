import { useEffect } from "react";
import ProtectedEmbed from "./ProtectedEmbed";
import { useWebcast } from "hooks/webcasts/use-webcasts";
import WebcastErrorContent from "pages/WebcastPage/components/WebcastErrorContent/WebcastErrorContent";
import WebcastLoadingContent from "pages/WebcastPage/components/WebcastLoadingContent/WebcastLoadingContent";
import AuthService from "services/auth-service";
import { ILayoutType } from "types/webcast";

interface Props {
  indicoId: string;
  layoutType: ILayoutType;
}

export default function ProtectedEmbedProvider({
  indicoId,
  layoutType,
}: Props) {
  const { data, error, isLoading, isError, refetch } = useWebcast(
    `i${indicoId}`,
  );

  useEffect(() => {
    // Refetch the webcast after the user logs in
    if (AuthService.isLoggedIn()) {
      refetch();
    }
  }, [refetch]);

  if (isError) {
    return <WebcastErrorContent error={error} indicoId={indicoId} />;
  }

  if (!data?.success) {
    // Is not allowed
    return (
      <div data-testid="protected-webcast">
        <WebcastErrorContent error={error} indicoId={indicoId} />
      </div>
    );
  }

  if (isLoading) {
    return <WebcastLoadingContent />;
  }

  if (data && data.result) {
    return <ProtectedEmbed webcast={data.result} layoutType={layoutType} />;
  }
  return null;
}
