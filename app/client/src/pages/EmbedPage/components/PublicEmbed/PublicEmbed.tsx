import { useEffect } from "react";
import PlayerContainer from "components/PlayerContainer/PlayerContainer";
import { ILayoutType, IWebcast } from "types/webcast";

interface Props {
  webcast: IWebcast;
  layoutType: ILayoutType;
}

export default function PublicEmbed({ webcast, layoutType }: Props) {
  useEffect(() => {
    if (webcast) {
      document.title = `${webcast.title} - CERN Live Events Website (Webcast)`;
    }
  }, [webcast]);

  return (
    <div data-testid="public-webcast">
      <PlayerContainer
        layoutType={layoutType}
        event={webcast}
        withCdn={false}
        code={undefined}
      />
    </div>
  );
}
