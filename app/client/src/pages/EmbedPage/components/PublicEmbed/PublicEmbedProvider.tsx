import { useEffect } from "react";
import PublicEmbed from "./PublicEmbed";
import { usePublicWebcast } from "hooks/webcasts/use-webcasts";
import WebcastErrorContent from "pages/WebcastPage/components/WebcastErrorContent/WebcastErrorContent";
import WebcastLoadingContent from "pages/WebcastPage/components/WebcastLoadingContent/WebcastLoadingContent";
import { ILayoutType } from "types/webcast";

interface Props {
  indicoId: string;
  layoutType: ILayoutType;
}
export default function PublicEmbedProvider({ indicoId, layoutType }: Props) {
  const { data, error, isLoading, isError } = usePublicWebcast(indicoId);

  useEffect(() => {
    if (data) {
      document.title = `${data.result.title} - CERN Live Events Website (Webcast)`;
    }
  }, [data]);

  if (isError) {
    return <WebcastErrorContent error={error} indicoId={indicoId} />;
  }

  if (isLoading) {
    return <WebcastLoadingContent />;
  }

  if (data && data.result) {
    return (
      <div data-testid="public-webcast">
        <PublicEmbed webcast={data.result} layoutType={layoutType} />
      </div>
    );
  }
}
