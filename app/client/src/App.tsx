import { useContext } from "react";
import { QueryClientProvider } from "react-query";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { Dimmer, Loader } from "semantic-ui-react";
import queryClient from "./api/query-client";
import config from "./config";
import HomePage from "./pages/HomePage/HomePage";
import { AuthContext } from "components/AuthProvider/AuthProvider";
import ErrorPage from "components/ErrorPage/ErrorPage";
import WebcastPageLayout from "components/WebcastPageLayout/WebcastPageLayout";
import { initMatomo } from "hooks/matomo/use-matomo";
import EmbedPage from "pages/EmbedPage/EmbedPage";
import PermanentEmbedPage from "pages/PermanentEmbedPage/PermanentEmbedPage";
import PermanentWebcastDetailsPage from "pages/PermanentWebcastDetailsPage/PermanentWebcastDetailsPage";
import PermanentWebcastsListPage from "pages/PermanentWebcastsListPage/PermanentWebcastsListPage";
import PermanentWebcasts from "pages/PermanentWebcastsPage/PermanentWebcasts";
import PreviewPage from "pages/PreviewPage/PreviewPage";
import WebcastPage from "pages/WebcastPage/WebcastPage";
const { PIWIK_ID } = config;

initMatomo("https://webanalytics.web.cern.ch", PIWIK_ID);

const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
    errorElement: <ErrorPage />,
  },
  {
    path: "view/:code/:id",
    element: <PreviewPage />,
  },
  {
    path: "event/*",
    element: <WebcastPageLayout />,
    children: [
      {
        path: ":id/*",
        children: [
          {
            path: "",
            element: <WebcastPage />,
          },
          {
            path: "camera/*",
            children: [
              {
                path: "",
                element: <WebcastPage layoutType="camera" />,
              },
            ],
          },
          {
            path: "slides",
            children: [
              {
                path: "",
                element: <WebcastPage layoutType="slides" />,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    path: "embed/:id",
    index: true,
    element: <EmbedPage />,
  },
  {
    path: "permanent-webcasts/*",
    element: <PermanentWebcasts />,
    children: [
      {
        path: ":categoryId",
        element: <PermanentWebcastsListPage />,
      },
      {
        path: ":categoryId/stream/:streamId",
        element: <PermanentWebcastDetailsPage />,
      },
      {
        path: "permanent-embed/:id",
        element: <PermanentEmbedPage />,
      },
    ],
  },
]);

function App() {
  const initialized = useContext(AuthContext);
  if (!initialized) {
    return (
      <div style={{ height: 100, width: 100 }}>
        <Dimmer active inverted>
          <Loader inverted>Contacting SSO...</Loader>
        </Dimmer>
      </div>
    );
  }

  return (
    <QueryClientProvider client={queryClient}>
      <RouterProvider router={router} />
    </QueryClientProvider>
  );
}

export default App;
