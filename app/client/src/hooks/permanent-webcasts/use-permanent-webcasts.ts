import { useQuery } from "react-query";
import { AxiosError } from "axios";
import apiClient from "api/api-headers";
import {
  PermanentWebcasApiResponse,
  PermanentWebcastCategoryItemApiResponse,
} from "types/permanent-webcast";

export const fetchPermanentWebcast = async (webcastId: string | undefined) => {
  const { data } = await apiClient.get<PermanentWebcasApiResponse>(
    `/permanent-webcasts/${webcastId}`,
  );
  return data;
};

export function usePermanentWebcast(
  webcastId: string | undefined,
  options = {},
) {
  return useQuery<PermanentWebcasApiResponse, AxiosError>(
    ["permanent-webcasts", webcastId],
    () => fetchPermanentWebcast(webcastId),
    {
      enabled: !!webcastId,
      ...options,
    },
  );
}

export const fetchPermanentWebcastCategories = async () => {
  const { data } = await apiClient.get<PermanentWebcastCategoryItemApiResponse>(
    `/permanent-webcasts/permanent-categories/`,
  );
  return data;
};

export function usePermanentWebcastCategories(options = {}) {
  return useQuery<PermanentWebcastCategoryItemApiResponse, AxiosError>(
    ["categories"],
    () => fetchPermanentWebcastCategories(),
    {
      ...options,
    },
  );
}
