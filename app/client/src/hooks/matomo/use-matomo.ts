declare global {
  interface Window {
    _paq: string[][];
  }
}

import { useEffect } from "react";
import { useLocation } from "react-router-dom";

export function initMatomo(matomoURL: string, siteID: string) {
  window._paq = window._paq || [];

  window._paq.push(["setTrackerUrl", `${matomoURL}/matomo.php`]);
  window._paq.push(["setSiteId", siteID]);

  const scriptElement = document.createElement("script");
  scriptElement.type = "text/javascript";
  scriptElement.async = true;
  scriptElement.defer = true;
  scriptElement.src = `${matomoURL}/matomo.js`;

  document.getElementsByTagName("head")[0].appendChild(scriptElement);
}

export function trackPageView(customUrl?: string) {
  if (window._paq) {
    window._paq.push(["setCustomUrl", customUrl || window.location.href]);
    window._paq.push(["trackPageView"]);
  }
}

export function trackEvent(
  category: string,
  action: string,
  name: string,
  value: string,
) {
  if (window._paq) {
    window._paq.push(["trackEvent", category, action, name, value]);
  }
}

// Hooks
export function useMatomoPageView() {
  const { pathname } = useLocation();

  useEffect(() => {
    trackPageView();
  }, [pathname]);
}
