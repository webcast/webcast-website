export interface IAppConfig {
  api: {
    ENDPOINT: string;
  };
  IS_DEV_INSTALL: boolean;
  ENVIRONMENT: string;
  PIWIK_ID: string;
  ALLOWED_DOMAINS: string[];
  SENTRY_DSN: string;
  SENTRY_ENV: string;
  CLIENT_ID: string;
  WEBCAST_LITE_BASE_URL: string;
}
