export interface PermanentWebcastCategoryItemApiResponse {
  results: PermanentWebcastCategoryItem[];
  success: boolean;
}

export interface PermanentWebcast {
  id: number;
  title: string;
  stream: {
    camera_src: string;
    default_img: string;
    custom_img: string;
    player_url: string;
    embed_code: string;
  };
}

export interface PermanentWebcasApiResponse {
  result: PermanentWebcast;
  success: boolean;
}

export interface PermanentWebcastStreamItem {
  id: number;
  title: string;
  default_img: string;
  custom_img: string;
}
export interface PermanentWebcastCategoryItem {
  id: number;
  name: string;
  default_image: string;
  custom_image: string;
  priority: number;
  streams: PermanentWebcastStreamItem[];
}
