export interface ICdsRecord {
  id: number;
  title: string;
  image_url: string;
  speakers: string;
  published_date: string;
  link: string;
  is_protected: boolean;
}

export interface ICdsRecordListApiResponse {
  results: ICdsRecord[];
  success: boolean;
}
