export type ILayoutType = "camera" | "slides" | "dual";
export type IWebcastType = "camera" | "slides" | "Camera & Slides";

export type IStream = {
  type: IWebcastType;
  app_name: string;
  camera_stream: string;
  slides_stream: string;
  edge_url: string;
  camera_src: string;
  slides_src: string;
  player_url: string;
};

export interface IWebcast {
  id: number;
  embed_code: string;
  title: string;
  speakers: string;
  abstract: string;
  room_name: string;
  indico_link: string;
  extra_html: string;
  img: string;
  restricted: boolean;
  indico_id: string;
  type: string;
  ical_link: string;
  has_dvr: boolean;
  stream: IStream;
  origin_server: string;
  startDate: {
    day: string;
    tz: string;
    time: string;
  };
  endDate: {
    day: string;
    tz: string;
    time: string;
  };
}

export interface IWebcastApiResponse {
  result: IWebcast;
  success: boolean;
}

export interface IWebcastListApiResponse {
  results: IWebcast[];
  success: boolean;
}

interface IProtectedWebcastResult {
  is_restricted: boolean;
}

export interface IProtectedWebcastCheck {
  success: boolean;
  result: IProtectedWebcastResult;
}
