export interface PaellaManifest {
  metadata: {
    title: string;
    duration: number;
    preview: string;
    visibleTimeLine: boolean;
  };
  streams: {
    sources: {
      [key: string]: {
        src: string;
        mimetype: string;
        isLiveStream: boolean;
        res: {
          w: string;
          h: string;
        };
      }[];
    };
    content: string;
    role?: string;
  }[];
}
