// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
// eslint-disable-next-line import/no-extraneous-dependencies
// eslint-disable-next-line import/no-extraneous-dependencies
import "@testing-library/jest-dom";
// eslint-disable-next-line import/no-extraneous-dependencies
import matchers from "@testing-library/jest-dom/matchers";
import { server } from "./mocks/server";

// extends Vitest's expect method with methods from react-testing-library
expect.extend(matchers);

Object.defineProperty(window, "matchMedia", {
  writable: true,
  value: vi.fn().mockImplementation((query) => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: vi.fn(),
    removeListener: vi.fn(),
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })),
});

vi.mock("paella-core", () => ({
  Paella: vi.fn(),
  utils: vi.fn(),
}));

vi.mock("paella-basic-plugins", () => {
  return {
    default: vi.fn(),
    namedExport: vi.fn(),
    // etc...
  };
});

vi.mock("paella-user-tracking", () => {
  return {
    default: vi.fn(),
    namedExport: vi.fn(),
    // etc...
  };
});

// vi.mock("paella-user-tracking", () => vi.fn());

// Establish API mocking before all tests.
beforeAll(() => server.listen());
// Reset any request handlers that we may add during the tests,
// so they don't affect other tests.
afterEach(() => server.resetHandlers());
// Clean up after the tests are finished.
afterAll(() => server.close());

// global.matchMedia =
//   global.matchMedia ||
//   function (media) {
//     return {
//       matches: media === "(min-width:1920px)",
//       addListener: vi.fn(),
//       removeListener: vi.fn(),
//     };
//   };
