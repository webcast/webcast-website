import { IAppConfig } from "types/config";

const allowed_domains = [
  "wowza.cern.ch", // CERN
  "wowza05.cern.ch",
  "wowzaqaedge.cern.ch", // CERN QA
  "cern2.vo.llnwd.net", // Limelight
  "eu-west-2b.hls.falcon.eegcloud.tv", // AI Media
];

const dev: IAppConfig = {
  api: {
    ENDPOINT: "http://localhost:5000",
  },
  IS_DEV_INSTALL: import.meta.env.DEV === true,
  ENVIRONMENT: import.meta.env.VITE_ENVIRONMENT
    ? import.meta.env.VITE_ENVIRONMENT
    : "DEV",
  PIWIK_ID: "3722",
  ALLOWED_DOMAINS: allowed_domains,
  SENTRY_DSN: import.meta.env.VITE_SENTRY_DSN
    ? import.meta.env.VITE_SENTRY_DSN
    : "",
  SENTRY_ENV: import.meta.env.VITE_SENTRY_ENV
    ? import.meta.env.VITE_SENTRY_ENV
    : "",
  CLIENT_ID: import.meta.env.VITE_CLIENT_ID
    ? import.meta.env.VITE_CLIENT_ID
    : "webcast_website_test_fe",
  WEBCAST_LITE_BASE_URL: import.meta.env.VITE_WEBCAST_LITE_BASE_URL
    ? import.meta.env.VITE_WEBCAST_LITE_BASE_URL
    : "https://localhost:3000",
};

const prod: IAppConfig = {
  api: {
    ENDPOINT: import.meta.env.VITE_API_ENDPOINT
      ? import.meta.env.VITE_API_ENDPOINT
      : "",
  },
  IS_DEV_INSTALL: import.meta.env.DEV === true,
  ENVIRONMENT: import.meta.env.VITE_ENVIRONMENT
    ? import.meta.env.VITE_ENVIRONMENT
    : "DEV",
  PIWIK_ID: import.meta.env.VITE_PIWIK_ID ? import.meta.env.VITE_PIWIK_ID : "0",
  ALLOWED_DOMAINS: allowed_domains,
  SENTRY_DSN: import.meta.env.VITE_SENTRY_DSN
    ? import.meta.env.VITE_SENTRY_DSN
    : "",
  SENTRY_ENV: import.meta.env.VITE_SENTRY_ENV
    ? import.meta.env.VITE_SENTRY_ENV
    : "",
  CLIENT_ID: import.meta.env.VITE_CLIENT_ID
    ? import.meta.env.VITE_CLIENT_ID
    : "",
  WEBCAST_LITE_BASE_URL: import.meta.env.VITE_WEBCAST_LITE_BASE_URL
    ? import.meta.env.VITE_WEBCAST_LITE_BASE_URL
    : "https://localhost:3000",
};

const testConfig: IAppConfig = {
  api: {
    ENDPOINT: "http://localhost:3000",
  },
  IS_DEV_INSTALL: true,
  ENVIRONMENT: import.meta.env.VITE_ENVIRONMENT
    ? import.meta.env.VITE_ENVIRONMENT
    : "DEV",
  PIWIK_ID: "0",
  ALLOWED_DOMAINS: allowed_domains,
  SENTRY_DSN: import.meta.env.VITE_SENTRY_DSN
    ? import.meta.env.VITE_SENTRY_DSN
    : "",
  SENTRY_ENV: import.meta.env.VITE_SENTRY_ENV
    ? import.meta.env.VITE_SENTRY_ENV
    : "",
  CLIENT_ID: import.meta.env.VITE_CLIENT_ID
    ? import.meta.env.VITE_CLIENT_ID
    : "webcast_website_test_fe",
  WEBCAST_LITE_BASE_URL: import.meta.env.VITE_WEBCAST_LITE_BASE_URL
    ? import.meta.env.VITE_WEBCAST_LITE_BASE_URL
    : "https://localhost:3000",
};

let tempConfig = testConfig;

if (import.meta.env.PROD === true) {
  tempConfig = prod;
}

if (import.meta.env.DEV === true) {
  tempConfig = dev;
}

if (process.env.NODE_ENV === "test") {
  tempConfig = testConfig;
}

const appConfig = tempConfig;

export default appConfig;
