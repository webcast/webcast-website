import { IWebcastApiResponse } from "types/webcast";

export const webcastDetailsMockRestricted: IWebcastApiResponse = {
  success: true,
  result: {
    id: 5,
    title: "ATLAS Weekly",
    speakers: "Marumi Kado",
    abstract: "",
    room_name: "40/S2-C01",
    extra_html: "",
    indico_link: "https://indico.cern.ch/event/1160270/",
    img: "http://localhost:5000None",
    restricted: true,
    indico_id: "1160270",
    type: "Upcoming",
    ical_link: "http://localhost:5000/utils/ical/5/",
    has_dvr: false,
    origin_server: "https://wowza33.cern.ch",
    stream: {
      type: "Camera & Slides",
      app_name: "livehd",
      camera_stream: "1160270_camera_all",
      slides_stream: "1160270_slides_all",
      edge_url: "https://wowza.cern.ch",
      camera_src:
        "https://wowza.cern.ch/livehd/smil:1160270_camera_all.smil/playlist.m3u8",
      slides_src:
        "https://wowza.cern.ch/livehd/smil:1160270_slides_all.smil/playlist.m3u8",
      player_url: "https://video-player.web.cern.ch",
    },
    embed_code: `<iframe type="text/html" width="720" height="360" src="https://video-player.web.cern.ch/index.html?mode=embed&cameraAppName=livehd&cameraStream=1160270_camera_all&slidesAppName=livehd&slidesStream=1160270_slides_all&dvr=true" allowfullscreen="" frameborder="0"</iframe>`,
    startDate: {
      day: "2022-05-17",
      tz: "Europe/Zurich",
      time: "14:00:00",
    },
    endDate: {
      day: "2022-05-17",
      tz: "Europe/Zurich",
      time: "15:40:00",
    },
  },
};

export const webcastDetailsMockUnrestricted: IWebcastApiResponse = {
  success: true,
  result: {
    id: 1,
    title: "Nonperturbative Methods in Quantum Field Theory",
    speakers: "",
    abstract: "",
    room_name: "503/1-001",
    extra_html: "",
    indico_link: "https://indico.cern.ch/event/1127820/",
    img: "http://localhost:5000None",
    restricted: false,
    indico_id: "1127820",
    type: "Upcoming",
    ical_link: "http://localhost:5000/utils/ical/1/",
    has_dvr: false,
    origin_server: "https://wowza33.cern.ch",
    stream: {
      type: "Camera & Slides",
      app_name: "livehd",
      camera_stream: "1127820_camera_all",
      slides_stream: "1127820_slides_all",
      edge_url: "https://wowza.cern.ch",
      camera_src:
        "https://wowza.cern.ch/livehd/smil:1127820_camera_all.smil/playlist.m3u8",
      slides_src:
        "https://wowza.cern.ch/livehd/smil:1127820_slides_all.smil/playlist.m3u8",
      player_url: "https://video-player.web.cern.ch",
    },
    embed_code: `<iframe type="text/html" width="720" height="360" src="https://video-player.web.cern.ch/index.html?mode=embed&cameraAppName=livehd&cameraStream=1127820_camera_all&slidesAppName=livehd&slidesStream=1127820_slides_all&dvr=true" allowfullscreen="" frameborder="0"</iframe>`,
    startDate: {
      day: "2022-05-23",
      tz: "Europe/Zurich",
      time: "09:00:00",
    },
    endDate: {
      day: "2022-06-03",
      tz: "Europe/Zurich",
      time: "19:00:00",
    },
  },
};

export const webcastDetailsMockRecent = {
  success: true,
  result: {
    id: 5,
    title: "ATLAS Weekly",
    speakers: "Marumi Kado",
    abstract: "",
    room_name: "40/S2-C01",
    extra_html: "",
    indico_link: "https://indico.cern.ch/event/11602702/",
    img: "http://localhost:5000None",
    restricted: true,
    indico_id: "11602702",
    type: "Recent",
    ical_link: "http://localhost:5000/utils/ical/5/",
    stream: {
      type: "Camera & Slides",
      app_name: "livehd",
      camera_stream: "11602702_camera_all",
      slides_stream: "11602702_slides_all",
      edge_url: "https://wowza.cern.ch",
      camera_src:
        "https://wowza.cern.ch/livehd/smil:11602702_camera_all.smil/playlist.m3u8",
      slides_src:
        "https://wowza.cern.ch/livehd/smil:11602702_slides_all.smil/playlist.m3u8",
      player_url: "https://video-player.web.cern.ch",
    },
    embed_code: `<iframe type="text/html" width="720" height="360" src="https://video-player.web.cern.ch/index.html?mode=embed&cameraAppName=livehd&cameraStream=1160270_camera_all&slidesAppName=livehd&slidesStream=1160270_slides_all&dvr=true" allowfullscreen="" frameborder="0"</iframe>`,
    startDate: {
      day: "2022-05-17",
      tz: "Europe/Zurich",
      time: "14:00:00",
    },
    endDate: {
      day: "2022-05-17",
      tz: "Europe/Zurich",
      time: "15:40:00",
    },
  },
};

export const tokenMissingResponse = {
  error: "token_missing",
};

export default {
  webcastDetailsMockRestricted,
  webcastDetailsMockUnrestricted,
  webcastDetailsMockRecent,
  tokenMissingResponse,
};
