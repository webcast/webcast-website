// src/mocks/handlers.js
import { rest } from "msw";
import {
  isProtectedNotProtectedResponse,
  isProtectedProtectedResponse,
} from "./responses/is-protected";
import { userMe } from "./responses/me";
import {
  tokenMissingResponse,
  webcastDetailsMockRestricted,
  webcastDetailsMockUnrestricted,
  webcastDetailsMockRecent,
} from "./responses/webcast-details";
import { mockWebcastList } from "./responses/webcasts";
import config from "config";

const {
  api: { ENDPOINT },
} = config;

export const handlers = [
  rest.get(`${ENDPOINT}/api/private/v1/webcasts`, (req, res, ctx) => {
    // If authenticated, return a mocked webcasts details
    return res(ctx.status(200), ctx.json(mockWebcastList));
  }),
  rest.get(
    `${ENDPOINT}/api/private/v1/webcasts/public/1127820`,
    (req, res, ctx) => {
      // If authenticated, return a mocked webcasts details
      return res(ctx.status(200), ctx.json(webcastDetailsMockUnrestricted));
    },
  ),
  rest.get(`${ENDPOINT}/api/private/v1/webcasts/1160270`, (req, res, ctx) => {
    // If authenticated, return a mocked webcasts details
    return res(ctx.status(200), ctx.json(webcastDetailsMockRestricted));
  }),
  rest.get(`${ENDPOINT}/api/private/v1/webcasts/1160270`, (req, res, ctx) => {
    // If authenticated, return a mocked webcasts details
    return res(ctx.status(401), ctx.json(tokenMissingResponse));
  }),
  rest.get(`${ENDPOINT}/api/private/v1/webcasts/11602701`, (req, res, ctx) => {
    // The first matching "GET /book/:bookId" request
    // will receive this mocked response.
    return res.once(ctx.status(401), ctx.json({ message: "Restricted" }));
  }),
  rest.get(`${ENDPOINT}/api/private/v1/webcasts/11602702`, (req, res, ctx) => {
    // If authenticated, return a mocked webcasts details
    return res(ctx.status(200), ctx.json(webcastDetailsMockRecent));
  }),
  rest.get(`${ENDPOINT}/api/private/v1/users/me`, (req, res, ctx) => {
    // If authenticated, return a mocked user details
    return res(ctx.status(200), ctx.json(userMe));
  }),
  rest.get(
    `${ENDPOINT}/api/private/v1/webcasts/1160270/is-protected`,
    (req, res, ctx) => {
      return res(
        // Respond with a 200 status code
        ctx.status(200),
        ctx.json(isProtectedProtectedResponse),
      );
    },
  ),
  rest.get(
    `${ENDPOINT}/api/private/v1/webcasts/i1127820/is-protected`,
    (req, res, ctx) => {
      return res(
        // Respond with a 200 status code
        ctx.status(200),
        ctx.json(isProtectedNotProtectedResponse),
      );
    },
  ),
  rest.get("/user", (req, res, ctx) => {
    // Check if the user is authenticated in this session
    const isAuthenticated = sessionStorage.getItem("is-authenticated");

    if (!isAuthenticated) {
      // If not authenticated, respond with a 403 error
      return res(
        ctx.status(403),
        ctx.json({
          errorMessage: "Not authorized",
        }),
      );
    }

    // If authenticated, return a mocked user details
    return res(
      ctx.status(200),
      ctx.json({
        username: "admin",
      }),
    );
  }),
];

export default handlers;
