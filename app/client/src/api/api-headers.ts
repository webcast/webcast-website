import axios from "axios";
import config from "config";

const apiClient = axios.create({
  baseURL: `${config.api.ENDPOINT}/api/private/v1`,
});

export default apiClient;
