import config from "config";

export default function buildWebcastLiteLink(
  indicoId: string | undefined,
): string {
  if (!indicoId) {
    return "";
  }
  return `${config.WEBCAST_LITE_BASE_URL}/event/i${indicoId}`;
}
