import { ICdsRecord } from "types/cds-record";
import { IWebcast } from "types/webcast";

export const chunkCdsRecords = (
  arr: ICdsRecord[],
  chunkSize = 1,
  cache: ICdsRecord[][] = [],
) => {
  const tmp = [...arr];
  if (chunkSize <= 0) return cache;
  while (tmp.length) cache.push(tmp.splice(0, chunkSize));
  return cache;
};

export const chunkWebcasts = (
  arr: IWebcast[],
  chunkSize = 1,
  cache: IWebcast[][] = [],
) => {
  const tmp = [...arr];
  if (chunkSize <= 0) return cache;
  while (tmp.length) cache.push(tmp.splice(0, chunkSize));
  return cache;
};
