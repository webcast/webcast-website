import { breakPoints } from "app-media";

export const getWidth = (): number => {
  // const result = 0;
  const isSSR = typeof window === "undefined";
  const { innerWidth } = window;
  const result = isSSR ? breakPoints.tablet : innerWidth;
  return result as number;
};

export default getWidth;
