from flask_wtf import FlaskForm
from wtforms_alchemy import ModelForm

from app.models.api import ApiKey


class ApiKeyForm(ModelForm, FlaskForm):
    """
    ModelForm for ApiKey Model
    """

    class Meta:
        model = ApiKey
        exclude = ["access_key", "secret_key"]
