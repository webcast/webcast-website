from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed
from wtforms import FileField, SelectField
from wtforms_alchemy import ModelForm

from app.extensions import ALLOWED_IMAGES_EXTENSIONS
from app.models.audiences import Audience
from app.models.streams import Category, LiveStream, PermanentStream


class LiveStreamForm(ModelForm, FlaskForm):
    """
    ModelForm for LiveStream
    """

    class Meta:
        model = LiveStream
        exclude = ["camera_smil_fetched", "slides_smil_fetched"]


class PermanentStreamForm(ModelForm, FlaskForm):
    """
    ModelForm for Stream
    """

    class Meta:
        model = PermanentStream
        exclude = ["custom_img"]

    custom_img = FileField(
        "Custom Image",
        validators=[FileAllowed(ALLOWED_IMAGES_EXTENSIONS, "Images only!")],
    )

    audience = SelectField("Audience", choices=[], coerce=int)
    category = SelectField("Category", choices=[], coerce=int)

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        self.audience.choices = [(audience.id, audience.name) for audience in Audience.query.all()]
        self.category.choices = [(category.id, category.name) for category in Category.query.all()]


class CategoryForm(ModelForm, FlaskForm):
    """
    ModelForm for Category
    """

    class Meta:
        model = Category
        exclude = ["custom_img"]

    custom_img = FileField(
        "Custom Image",
        validators=[FileAllowed(ALLOWED_IMAGES_EXTENSIONS, "Images only!")],
    )


def process_permanent_stream_form(stream, permanent_stream_id, form):
    """
    Relationships
    """
    stream.audience = Audience.query.get(form.audience.data)
    stream.category = Category.query.get(form.category.data)

    """
    PermanentStream data
    """
    stream.title = form.title.data
    stream.player_src = form.player_src.data
    stream.player_width = form.player_width.data
    stream.player_height = form.player_height.data
    stream.default_img = form.default_img.data

    stream.is_sharable = form.is_sharable.data
    stream.is_embeddable = form.is_embeddable.data
    stream.is_visible = form.is_visible.data
    stream.use_default_img = form.use_default_img.data

    return stream
