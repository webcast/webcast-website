import logging
import os

import requests
from flask import current_app
from requests.auth import HTTPDigestAuth

from app.extensions import db
from app.models.streams import LiveStream


class WowzaService(object):
    def __init__(self, logger=None):
        self.urls = current_app.config.get("WOWZA_ORIGIN_URLS")
        self.username = current_app.config.get("WOWZA_USERNAME")
        self.password = current_app.config.get("WOWZA_PASSWORD")

        if not logger:
            self.logger = logging.getLogger("webapp.wowza")
        else:
            self.logger = logger

    def _make_request(self, path=None):
        """
        Makes a http request to the Wowza server to retrieve a SMIL file.
        It requires HTTPDigestAuth
        :param path: path of the SMIL file
        :return: The request response
        """
        result = None
        """
        Will iterate through all the wowza origins to fetch the smil file.
        If any of them returns a 200 code, means that the streaming is taking place there,
        so no need to continue iterating.
        """
        for url in self.urls:
            smil_url = f"{url}{path}"
            self.logger.info(f"Fetching SMIL file from: {smil_url}")

            result = requests.get(
                smil_url,
                auth=HTTPDigestAuth(self.username, self.password),
                timeout=5,
            )

            if result.status_code != 200:
                self.logger.info(f"Unable to fetch SMIL file from: {smil_url}")

            if result.status_code == 200:
                return result

        return result

    def get_camera_smil(self, app_name, indico_id, stream_camera_name=None):
        """

        :param event_id:
        :param indico_id:
        :param app_name:
        :param stream_camera_name:
        :return:
        """
        self.logger.debug("Getting camera SMIL for indico ID {} ...".format(indico_id))

        camera_result, camera_path = self._get_camera_smil(app_name, indico_id, stream_camera_name)

        return camera_result, camera_path

    def get_camera_slides_smil(self, app_name, stream_id=None, stream_camera_name=None, stream_slides_name=None):
        """

        :param logger:
        :param event_id:
        :param indico_id:
        :param app_name:
        :param stream_id:
        :param stream_camera_name:
        :param stream_slides_name:
        :return:
        """

        self.logger.debug(
            "Getting camera SMIL for Camera Stream {} and Slides Stream {} ...".format(
                stream_camera_name, stream_slides_name
            )
        )

        camera_result, camera_path = self._get_camera_smil(
            app_name, stream_id=stream_id, stream_name=stream_camera_name
        )

        slides_result, slides_path = self._get_slides_smil(
            app_name, stream_id=stream_id, stream_name=stream_slides_name
        )

        return slides_result, slides_path, camera_result, camera_path

    def _get_camera_smil(self, app_name, stream_id=None, stream_name=None):
        """
         Makes a request to the Wowza server to retrieve a camera SMIL file


        :param app_name:
        :param stream_id:
        :param indico_id:
        :param stream_name:
        :return:
        """
        # None by default
        postfix_camera = ""
        if app_name == "livesd":
            if not stream_name or (stream_name and not stream_name.endswith("_camera_camera")):
                postfix_camera = "_camera"
        if app_name == "livehd":
            if not stream_name or (stream_name and not stream_name.endswith("_all")):
                postfix_camera = "_all"

        camera_stream = "{}{}".format(stream_name, postfix_camera)
        camera_path = "/{}/ngrp:{}/medialist.smil".format(app_name, camera_stream)

        try:
            camera_result = self._make_request(path=camera_path)
            if camera_result.status_code == 200:
                with open(
                    current_app.config.get("WOWZA_SMIL_FOLDER") + "/" + camera_stream + ".smil",
                    "w+",
                ) as file:
                    file.write(camera_result.text)
                live_stream = LiveStream.query.get(stream_id)
                live_stream.camera_smil_fetched = True
                db.session.commit()

            return camera_result.status_code, camera_path
        except requests.exceptions.ConnectionError as error:
            self.logger.error(error, exc_info=True)
            return 500, camera_path

    def _get_slides_smil(self, app_name, stream_name=None, stream_id=None):
        """

        :param app_name:
        :param indico_id:
        :param stream_slides_name:
        :return:
        """
        postfix_slides = ""
        if app_name == "livesd":
            if not stream_name or (stream_name and not stream_name.endswith("_slides_slides")):
                postfix_slides = "_slides"
        if app_name == "livehd":
            if not stream_name or (stream_name and not stream_name.endswith("_all")):
                postfix_slides = "_all"

        slides_stream = "{}{}".format(stream_name, postfix_slides)
        # if stream_name:
        #     slides_stream = '{stream_slides_name}'.format(stream_slides_name=stream_name)
        slides_path = "/{app_name}/ngrp:{stream}/medialist.smil".format(app_name=app_name, stream=slides_stream)
        try:
            slides_result = self._make_request(path=slides_path)
            if slides_result.status_code == 200:
                with open(
                    current_app.config.get("WOWZA_SMIL_FOLDER") + "/" + slides_stream + ".smil",
                    "w+",
                ) as file:
                    file.write(slides_result.text)
                live_stream = LiveStream.query.get(stream_id)
                live_stream.slides_smil_fetched = True
                db.session.commit()

            return slides_result.status_code, slides_path
        except requests.exceptions.ConnectionError as error:
            self.logger.error(error, exc_info=True)
            return 500, slides_path

    def smil_file_exists(self, stream_name, stream_type=None, app_name=None):
        """
        Removes a SMIL file with the parameters stream_name

        :param stream_name:
        :param stream_type:
        :param app_name:
        :return:
        """
        if stream_type == "slides":
            postfix_slides = "_slides"
        else:
            postfix_slides = "_camera"
        if app_name == "livehd":
            postfix_slides = "_all"
        smil_file_name = "{}{}".format(stream_name, postfix_slides)

        path = os.path.join(current_app.config.get("WOWZA_SMIL_FOLDER"), smil_file_name + ".smil")

        if os.path.isfile(path):
            return True
        else:
            self.logger.error("Unable to find smil file for {} on path: {}".format(stream_name, path))
            return False
