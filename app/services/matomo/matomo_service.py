from flask import current_app

from app.services.matomo.matomo_api_client import MatomoApiClient


class MatomoService:
    def __init__(self):
        self.matomo_base_url = current_app.config["MATOMO_BASE_URL"]
        self.idsite = current_app.config["MATOMO_SITEID"]
        self.auth_token = current_app.config["MATOMO_AUTH_TOKEN"]

    def make_request_to_matomo(self, ua, ip, lang, url, action_name):

        matomo_api_client = MatomoApiClient(self.matomo_base_url, self.idsite, self.auth_token)
        matomo_api_client.make_request_to_matomo(ua, ip, lang, url, action_name)
