from flask import current_app, request

from app.tasks.matomo import matomo_register_visit


def request_matomo_task(client_remote_ip, url, action_name):

    ua = request.user_agent.string
    lang = request.accept_languages[0][0]

    if current_app.config.get("MATOMO_ENABLED", False):
        matomo_register_visit.delay(ua, client_remote_ip, lang, url, action_name)
