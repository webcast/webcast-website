import datetime
import hashlib
import random
import sys

import requests
from ua_parser import user_agent_parser
from werkzeug.user_agent import UserAgent
from werkzeug.utils import cached_property


class ParsedUserAgent(UserAgent):
    @cached_property
    def _details(self):
        return user_agent_parser.Parse(self.string)

    @property
    def platform(self):
        return self._details["os"]["family"]

    @property
    def browser(self):
        return self._details["user_agent"]["family"]

    @property
    def version(self):
        return ".".join(
            part for key in ("major", "minor", "patch") if (part := self._details["user_agent"][key]) is not None
        )


class MatomoApiClient:

    #: Length of the visitor ID
    LENGTH_VISITOR_ID = 16

    def __init__(self, matomo_base_url: str, idsite: int, auth_token: str):
        self.auth_token = auth_token
        self.matomo_base_url = matomo_base_url
        self.idsite = idsite
        self.apiv = 1

    def generate_user_id(self, ip_address):
        # Use MD5 hash function
        hash_object = hashlib.md5()  # nosec
        # Encode the IP address to bytes
        hash_object.update(ip_address.encode())
        # Get the hexadecimal digest of the hash
        hashed_ip = hash_object.hexdigest()
        # Truncate to the first 16 characters
        return hashed_ip[: self.LENGTH_VISITOR_ID]

    def make_request_to_matomo(self, ua: str, ip: str, lang: str, url: str, action_name: str):

        request_params_dict = {
            "_id": self.generate_user_id(ip),
            "idsite": self.idsite,
            "rec": 1,
            "apiv": self.apiv,
            "url": url,
            "urlref": url,
            "action_name": action_name,
            "rand": random.randint(0, sys.maxsize),  # nosec
            "token_auth": self.auth_token,
            "cip": ip,
            "ua": ua,
            "lang": lang,
            "h": datetime.datetime.now().hour,
            "m": datetime.datetime.now().minute,
            "s": datetime.datetime.now().second,
        }

        # Convert the dictionary to a query string
        request_params = "&".join(f"{key}={value}" for key, value in request_params_dict.items())
        request_url = f"{self.matomo_base_url}/matomo.php?{request_params}"

        # Make the request to the Matomo server
        response = requests.get(request_url, timeout=1)
        response.raise_for_status()
