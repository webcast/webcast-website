import datetime
import json
import logging

from dateutil import tz
from dateutil.relativedelta import relativedelta

from app.common.datetime_helper import extract_future_events, generate_timezone_aware_datetime
from app.plugins.indico_api_client import IndicoAPIClient

logger = logging.getLogger("webapp.indico")


class IndicoService(object):
    def __init__(self, logger=None):
        if not logger:
            self.logger = logging.getLogger("webapp.indico")
        else:
            self.logger = logger

    def fetch_upcoming_events(self, number_of_months=2):
        """
        Gets the events from the following months

        :param number_of_months: Number of months to get the events
        :return: The events matching the query
        """

        self.logger.debug("Fetching Upcoming Events from Indico")

        """
        We want to fetch the events from today to <number_of_months> in advance.
        """

        return self._get_indico_webcasts(number_of_months=number_of_months)

    def get_next_contribution(self, indico_id, from_date=None, number_of_months=3):
        """
        Query Indico by id to get data for an event and retrieve the next contribution after
        a given starting time if present
        $startingTime must be a DateTime object

        :param indico_id:
        :param from_date:
        :param number_of_months:
        :return:
        """
        logger.debug("Getting next contribution for Indico ID: {}".format(indico_id))

        events_json = self._get_indico_webcasts(from_date, number_of_months)

        future_events = extract_future_events(events_json, after_date=from_date)

        logger.debug("{} events found for the next {} months".format(len(future_events), number_of_months))

        for event in future_events:
            event_id = event["event_id"]

            if indico_id == event_id:
                logger.debug("Matching Event {} with {}".format(event_id, indico_id))

                logger.debug("Getting next date for event {}".format(indico_id))

                to_zone = tz.gettz("Europe/Zurich")
                timezone = event["startDate"]["tz"]
                start_date = event["startDate"]["date"]
                start_time = event["startDate"]["time"]

                event_start_date = generate_timezone_aware_datetime(start_date, start_time, timezone)
                if timezone != "Europe/Zurich":
                    event_start_date = event_start_date.astimezone(tz=to_zone)

                    from_date = datetime.datetime(
                        year=from_date.year,
                        month=from_date.month,
                        day=from_date.day,
                        hour=from_date.hour,
                        minute=from_date.minute,
                        tzinfo=to_zone,
                    )

                if event_start_date > from_date:
                    logger.debug("Next contribution will take place on {}".format(event_start_date))

                    return event_start_date

                logger.debug("Event {} won't have a next contribution".format(indico_id))

        return None

    def _get_indico_webcasts(self, from_date=None, number_of_months=3):
        """
        Retrieves all the Indico Webcasts from a date to number_of_months offset

        :param from_date: Start date
        :param number_of_months: Number of months that will be retrieved from that date
        :return: JSON list of events
        """

        today = from_date
        if not from_date:
            today = datetime.datetime.now()
            from_date = datetime.datetime(today.year, today.month, today.day, 0, 0).isoformat()[:-3]
        else:
            from_date = datetime.datetime(today.year, today.month, today.day, today.hour, today.minute).isoformat()[:-3]

        to_date = today + relativedelta(months=+number_of_months)
        to_date = to_date.isoformat()[:-10]  # Removing milliseconds: 2017-09-30T07:03:13.162776

        logger.debug("Getting events from {} to {}".format(from_date, to_date))

        result = IndicoAPIClient().get_webcasts(from_date=from_date, to_date=to_date)

        events_json = {}
        try:
            fetched_events = json.loads(result[1])
        except ValueError as e:
            logger.warning(
                "Unable to fetch upcoming events from Indico from: {} to: {}. Error: {}".format(
                    from_date, to_date, str(e)
                )
            )
            return events_json
        try:
            events_json = fetched_events["results"]
        except KeyError as e:
            logger.warning(
                "Unable to fetch events. 'results' key not found on response: {} {}".format(fetched_events, e)
            )
            raise (Exception("Unable to fetch events. 'results' key not found on response: {}".format(fetched_events)))
        return events_json
