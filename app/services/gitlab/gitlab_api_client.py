import urllib.parse
from typing import Any, Dict, List

import requests


class GitlabApiClient:
    def __init__(self, gitlab_url: str, token: str, project_id: str, branch: str = "master"):
        """Initialize GitlabService with Gitlab URL, token and project ID.

        Args:
            gitlab_url (str): Gitlab URL.
            token (str): Gitlab token.
            project_id (str): Gitlab project ID.
        """
        if gitlab_url is None or gitlab_url == "":
            raise ValueError("Gitlab URL is required")
        if token is None or token == "":  # nosec B105
            raise ValueError("Gitlab token is required")
        if project_id is None or project_id == "":
            raise ValueError("Gitlab project ID is required")

        if gitlab_url.endswith("/"):
            gitlab_url = gitlab_url[:-1]
        self.gitlab_url = gitlab_url
        self.token = token
        self.project_id = project_id
        self.branch = branch

    def create_commit(
        self,
        commit_message: str,
        actions: List[Dict[str, str]],
    ) -> Dict[str, Any]:
        """Create a commit.

        Args:
            commit_message (str): Commit message.
            actions (List[Dict[str, str]]): List of actions to perform.
            branch (str, optional): Branch to commit to. Defaults to "master".

        Returns:
            Dict[str, Any]: Response JSON.
        """
        url = f"{self.gitlab_url}/api/v4/projects/{self.project_id}/repository/commits"
        payload = {
            "branch": self.branch,
            "commit_message": commit_message,
            "actions": actions,
        }
        response = requests.post(
            url,
            headers={
                "PRIVATE-TOKEN": self.token,
            },
            json=payload,
            timeout=10,
        )
        response.raise_for_status()
        return response.json()

    def check_file_exists(
        self,
        file_path: str,
    ) -> Dict[str, Any]:
        url_encoded_file_path = urllib.parse.quote(file_path, safe="")

        base_url = f"{self.gitlab_url}/api/v4/projects/{self.project_id}/repository/files/"
        full_url = base_url + f"{url_encoded_file_path}?ref={self.branch}"
        response = requests.get(
            full_url,
            headers={
                "PRIVATE-TOKEN": self.token,
            },
            timeout=10,
        )
        response.raise_for_status()
        return response.json()
