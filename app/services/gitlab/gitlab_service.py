"""Gitlab service"""

import base64
import logging
from typing import Any, Dict

from flask import current_app
from requests import HTTPError

from app.services.gitlab.gitlab_api_client import GitlabApiClient


def encode_string_base64(string: str) -> str:
    """Encode string to base64"""
    return base64.b64encode(string.encode("utf-8")).decode("utf-8")


class GitlabService:
    """A class representing a Gitlab service"""

    def __init__(self, logger=None):
        self.gitlab_url = current_app.config["GITLAB_URL"]
        self.gitlab_token = current_app.config["GITLAB_TOKEN"]
        self.gitlab_project_id = current_app.config["GITLAB_PROJECT_ID"]
        self.gitlab_branch = current_app.config["GITLAB_BRANCH"]

        self.client = GitlabApiClient(
            gitlab_url=self.gitlab_url,
            token=self.gitlab_token,
            project_id=self.gitlab_project_id,
            branch=self.gitlab_branch,
        )

        if not logger:
            self.logger = logging.getLogger("webapp.gitlab")
        else:
            self.logger = logger

    def create_custom_commit_action(
        self,
        file_path: str,
        content: str,
        action: str,
    ) -> Dict[str, str]:
        """Create a commit with a custom action.

        Args:
            file_path (str): File path.
            content (str): File content.

        Returns:
            Dict[str, str]: Commit action.
        """
        return {
            "action": action,
            "file_path": file_path,
            "content": encode_string_base64(content),
            "encoding": "base64",
        }

    def create_commit_action(
        self,
        file_path: str,
        content: str,
    ) -> Dict[str, str]:
        """Create a commit action.

        Args:
            file_path (str): File path.
            content (str): File content.

        Returns:
            Dict[str, str]: Commit action.
        """
        return {
            "action": "create",
            "file_path": file_path,
            "content": encode_string_base64(content),
            "encoding": "base64",
        }

    def update_commit_action(
        self,
        file_path: str,
        content: str,
    ) -> Dict[str, str]:
        """Create a commit action.

        Args:
            file_path (str): File path.
            content (str): File content.

        Returns:
            Dict[str, str]: Commit action.
        """
        return {
            "action": "update",
            "file_path": file_path,
            "content": encode_string_base64(content),
            "encoding": "base64",
        }

    def delete_commit_action(
        self,
        file_path: str,
    ) -> Dict[str, str]:
        """Create a commit action.

        Args:
            file_path (str): File path.
            content (str): File content.

        Returns:
            Dict[str, str]: Commit action.
        """
        return {
            "action": "delete",
            "file_path": file_path,
        }

    def create_commit_file(
        self,
        file_path: str,
        content: str,
        commit_message: str,
    ) -> Dict[str, Any]:
        """Create a commit file.

        Args:
            file_path (str): File path.
            content (str): File content.
            commit_message (str): Commit message.
            branch (str, optional): Branch to commit to. Defaults to "master".

        Returns:
            Dict[str, Any]: Response JSON.
        """
        actions = [
            self.create_commit_action(
                file_path=file_path,
                content=content,
            ),
        ]
        return self.client.create_commit(
            commit_message=commit_message,
            actions=actions,
        )

    def check_file_exists(
        self,
        file_path: str,
    ) -> bool:
        try:
            self.client.check_file_exists(file_path=file_path)
            return True
        except HTTPError as error:
            if error.response and error.response.status_code == 404:
                return False
        return False
