import logging
from typing import List, Optional

import click
from flask.cli import AppGroup

from app.services.webcast_lite.webcast_lite_service import WebcastLiteService

webcast_lite_cli = AppGroup("webcast_lite")

logger = logging.getLogger("webapp.cli")


@click.option(
    "--role-identifier",
    default="",
    prompt="Role Identifier, leave empty if not needed",
    help="The role identifier for the event.",
    required=False,
)
@click.option(
    "--slides-url",
    prompt="Slides URL, leave empty if using camera only",
    help="The slides URL for the event.",
    default="",
    required=True,
)
@click.option(
    "--camera-url",
    prompt="Camera URL, leave empty if using slides only",
    help="The camera URL for the event.",
    default="",
    required=True,
)
@click.option(
    "--streams",
    prompt=True,
    show_choices=True,
    type=click.Choice(["camera", "slides", "camera_slides"]),
    help="The stream URL for the event.",
    required=True,
)
@click.option(
    "--title",
    prompt=True,
    help="The title for the event.",
    required=True,
)
@click.option(
    "--event-id",
    prompt=True,
    help="The ID of the event to create.",
    type=int,
    required=True,
)
@webcast_lite_cli.command(
    "generate-config",
)
def generate_webcast_lite_config(
    event_id: int,
    streams: str,
    camera_url: str,
    slides_url: str,
    title: str,
    role_identifier: Optional[List[str]],
):
    """
    Generates the webcast lite config file for a given event.

    Could be use interactively or with the following arguments:
        --event-id
        --default-streams
        --default-camera-url
        --default-slides-url
        --default-title
        --role-identifier
    """
    if role_identifier == "":
        role_identifier = None

    if isinstance(role_identifier, str):
        role_identifier = role_identifier.split(",")

    if role_identifier is not None:
        role_identifier = [role.strip() for role in role_identifier]

    logger.info(
        "Creating webcast lite event %s with streams (%s), "
        "camera url (%s), slides url (%s), title (%s) and role identifier (%s)",
        event_id,
        streams,
        camera_url,
        slides_url,
        title,
        role_identifier,
    )

    service = WebcastLiteService(logger=logger)
    if event_id:
        actions, messages = service.create_actions_for_event(int(event_id))
        committed = service.create_commit_for_actions(actions, messages)

        if committed:
            click.echo("Successfully created webcast lite event")
        else:
            click.echo("Failed to create webcast lite event")
