"""Build config and htaccess files for webcast lite"""

from typing import List

import jinja2

enviroment = jinja2.Environment(loader=jinja2.PackageLoader("app.services.webcast_lite", "templates"), autoescape=True)


def build_config_file(
    streams: str,
    camera_url: str,
    slides_url: str,
    title: str,
):
    """Build config file"""
    template = enviroment.get_template("config.json.j2")

    # I want to scape the all the \ found in the title
    # to make it work with the json format
    new_title = title.replace("\\", "\\\\")

    return template.render(
        streams=streams,
        camera_url=camera_url,
        slides_url=slides_url,
        title=new_title,
    )


def build_htaccess_file(role_identifier: List[str]):
    """Build htaccess file"""
    # Sanitazing role_identifier
    # Remove element with spaces, and email
    sanitized_role_identifier: List[str] = []
    for role in role_identifier:
        if " " in role:
            continue
        if "@" in role:
            continue
        if role in sanitized_role_identifier:
            continue
        if role == "":
            continue

        sanitized_role_identifier.append(role)

    template = enviroment.get_template("htaccess.j2")
    return template.render(
        role_identifiers=sanitized_role_identifier,
    )
