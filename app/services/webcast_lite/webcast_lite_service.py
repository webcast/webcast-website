"""
This module contains the main functions for the webcast lite service.
"""

import logging
import os
from typing import Dict, List, Union

from requests import HTTPError
from sqlalchemy_utils.types.choice import Choice

from app.models.events import Event
from app.services.gitlab.gitlab_service import GitlabService

from .build_files import build_config_file, build_htaccess_file

if os.environ.get("RUN_MODE", False) == "JOB":
    logger = logging.getLogger("job.webcast_lite_service")
else:
    logger = logging.getLogger("webapp.webcast_lite_service")


class WebcastLiteService:
    def __init__(self, logger=None) -> None:
        if os.environ.get("RUN_MODE", False) == "JOB":
            self.logger = logging.getLogger("job.webcast_lite_service")
        else:
            self.logger = logging.getLogger("webapp.webcast_lite_service")

        self.gitlab_service = GitlabService(logger=logger)

    def create_actions_for_event(self, event_id: int):
        actions: List[Dict[str, str]] = []
        messages: List[str] = []
        event: Union[Event, None] = Event.query.get(event_id)

        if event:
            self.logger.debug(f"Creating actions for event {event.indico_id} ({event.title})")

            folder_path = f"src/events/{event.indico_id}"
            htaccess_path = f"{folder_path}/.htaccess"
            config_path = f"{folder_path}/config.json"

            # Create the content for the config file
            config_action, config_message = self.generate_config_action(config_path, event)
            actions.append(config_action)
            messages.append(config_message)

            htaccess_action, htaccess_action_message = self.generate_htaccess_action(htaccess_path, event)
            if htaccess_action:
                actions.append(htaccess_action)
                messages.append(htaccess_action_message)

            return actions, messages

        return [], []

    def create_commit_for_actions(self, actions: List[Dict[str, str]], messages: List[str]):
        # Append all messages with a /n
        commit_message = "\n".join(messages)
        try:
            res = self.gitlab_service.client.create_commit(
                commit_message=commit_message,
                actions=actions,
            )
            logger.info(res)
            return True
        except HTTPError as exc:
            logger.error(
                f"Unable to create a commit for the actions: {commit_message}",
                exc_info=exc,
            )
            return False

    def generate_config_action(self, config_path: str, event: Event):
        stream_type: str
        if isinstance(event.live_stream.type, Choice):
            stream_type = event.live_stream.type.code
        else:
            stream_type = str(event.live_stream.type)

        config_file_content = build_config_file(
            streams=stream_type,
            camera_url=event.live_stream.camera_src,
            slides_url=event.live_stream.slides_src,
            title=event.title,
        )

        config_exists = self.gitlab_service.check_file_exists(config_path)

        config_action_name = "create"
        action_message = f"✏️ Add event {event.id} config"
        if config_exists:
            config_action_name = "update"
            action_message = f"✏️ Update event {event.id} config"

        config_action = self.gitlab_service.create_custom_commit_action(
            file_path=config_path,
            content=config_file_content,
            action=config_action_name,
        )

        return config_action, action_message

    def generate_htaccess_action(self, htaccess_path: str, event: Event):
        authorized_users = event.audience.authorized_users if event.audience else []
        role_identifiers = [authorized_user.name for authorized_user in authorized_users]

        htaccess_exists = self.gitlab_service.check_file_exists(htaccess_path)
        has_restrictions = len(role_identifiers) > 0

        if htaccess_exists:
            if has_restrictions:  # It is an update
                action_message = f"✏️ Update event {event.indico_id} htaccess"

                htaccess_file_content = build_htaccess_file(
                    role_identifier=role_identifiers,
                )
                htaccess_action = self.gitlab_service.create_custom_commit_action(
                    file_path=htaccess_path,
                    content=htaccess_file_content,
                    action="update",
                )
                return htaccess_action, action_message

            else:  # Must remove the htaccess file
                action_message = f"✏️ Delete event {event.indico_id} htaccess"

                htaccess_action = self.gitlab_service.delete_commit_action(
                    file_path=htaccess_path,
                )
                return htaccess_action, action_message
        else:
            if has_restrictions:  # Only create the htaccess file if there are restrictions
                action_message = f"✏️ Add event {event.indico_id} htaccess"

                htaccess_file_content = build_htaccess_file(
                    role_identifier=role_identifiers,
                )
                htaccess_action = self.gitlab_service.create_custom_commit_action(
                    file_path=htaccess_path,
                    content=htaccess_file_content,
                    action="create",
                )

                return htaccess_action, action_message
            return None, ""
