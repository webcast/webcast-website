import logging

from flask import current_app, g
from flask_login import current_user

from app.models.audiences import AuthorizedUser
from app.models.events import Event

logger = logging.getLogger("webapp.auth")


def is_user_authorized(event: Event):
    """
    Checks if a user is authorized to view an Event

    :param event: Event to be checked
    :return: True | False if the user it allowed to view the resource
    """
    # {
    # 'email': 'rene.fernandez@cern.ch',
    # 'first_name': 'Rene',
    # 'last_name': 'Fernandez',
    # 'uid': 'fernanre',
    # 'roles': ['admins', 'cern-accounts-primary']
    # }
    if event.is_protected or event.is_restricted():
        user = g.user
        # If user is admin
        if user is None:
            return False

        user_roles = user["roles"]

        if current_app.config["ADMIN_ROLE"] in user_roles:
            logger.debug("Current user allowed to access the event: is_admin=True")
            return True

        if user is not None:
            # Check on the audience authorized users
            audience_authorized_roles: list[AuthorizedUser] = event.audience.authorized_users

            logger.info(f"User ({user['username']}) roles are {user_roles}.")

            for role in user_roles:
                is_role_authorized = is_role_in_authorized_roles(role, audience_authorized_roles)
                if is_role_authorized:
                    logger.info(f"User ({user['username']}) owns role {role} -> Authorized")
                    return True

            logger.info(f"User ({user['username']}) roles are not in authorized event roles.")

        return False
    return True


def is_user_authorized_for_permanent_stream(stream):
    """
    Checks if a user is authorized to view a PermanentStream

    :param stream: PermanentStream to be checked
    :return: True | False if the user it allowed to view the resource
    """
    user = g.user

    if stream.audience:
        audience_authorized_roles = stream.audience.authorized_users
    else:
        audience_authorized_roles = []

    if len(audience_authorized_roles) == 0:
        return True

    if user.is_authenticated and current_user.is_admin:
        return True

    if user.is_authenticated:

        # Check on the audience authorized users
        user_roles = user["roles"]

        # for egroup in current_user.egroups:
        for role in user_roles:
            is_egroup_authorized = is_role_in_authorized_roles(role, audience_authorized_roles)
            if is_egroup_authorized:
                return True

    return False


def is_role_in_authorized_roles(role: str, authorized_roles: list[AuthorizedUser]):
    """
    Checks all the authorized_users names for egroups and verifies if the current user egroup is among them.

    :param egroup: Current egroup to search
    :param authorized_users:  List with all the authorized users
    :return:  True|False if the egroup is found
    """
    if role in [authorized_user.name for authorized_user in authorized_roles]:
        return True
    return False
