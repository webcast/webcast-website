import glob
import logging
import os
import random

from flask import current_app

from app.models.audiences import Audience
from app.models.events import Event


def str2bool(v):
    """
    Converts a string to boolean

    :param v: Value to convert to boolean
    :return: The boolean result
    """
    return v.lower() in ("yes", "true", "t", "1")


def get_images_from_folder(images_path, logger=None):
    """
    Returns all the images from a folder path

    :param logger:
    :param images_path: Path to look for images
    :return: A list with all the images
    """
    if not logger:
        logger = logging.getLogger("webapp.utils")

    logger.debug("Getting images from folder")
    os.chdir(images_path)
    files_found = []
    # Get jpg files
    for file in glob.glob("*.jpg"):
        file = os.path.join(images_path, file).replace(current_app.config.get("STATIC_FILES_PATH"), "")[1:]
        files_found.append(os.path.join("/", "static-files", file))

    # Get png files
    for file in glob.glob("*.png"):
        file = os.path.join(images_path, file).replace(current_app.config.get("STATIC_FILES_PATH"), "")[1:]
        files_found.append(os.path.join("/", "static-files", file))
    return files_found


def is_subdir(path, directory):
    """
    Checks if a if a directory is int the path
    :param path: Secure path where the directory must be
    :param directory: Directory path to check
    :return:
    """
    path = os.path.realpath(path)
    directory = os.path.realpath(directory)
    relative = os.path.relpath(path, directory)
    return not relative.startswith(os.pardir + os.sep)


def get_event_random_image(audience_id, default_img, logger=None):
    """
    Gets a random image for the event based on It's audience

    It shouldn't get an image already used on the previous three events of the selected audience.

    :param logger:
    :param audience_id:
    :param default_img:
    :return:
    """
    new_image = default_img
    if not logger:
        logger = logging.getLogger("webapp.utils")

    audience = Audience.query.get(audience_id)

    if audience and (not new_image or new_image == ""):
        logger.debug("Setting up random image for Event using Audience '{}'".format(audience.name))

        latest_default_images = []
        if not new_image or new_image == "":
            events = Event.query.filter_by(audience=audience).order_by(Event.id.desc()).limit(3)
            latest_default_images = [
                latest_default_images.append(current_event.default_img) for current_event in events
            ]
        """
        We get all the available images of the audience and generate an
        array with the ones that are not within the latest
        """
        audience_folder = audience.name.replace(" ", "_")

        images_folder_path = os.path.join(
            current_app.config.get("STATIC_FILES_PATH"),
            current_app.config.get("DEFAULT_IMAGES_FOLDER"),
            current_app.config.get("DEFAULT_EVENTS_FOLDER"),
            audience_folder,
        )

        """
        We get all the images in the folder
        """
        try:
            images = get_images_from_folder(images_folder_path, logger=logger)

            """
            Remove from the list the images
            """
            available_images = list(set(images) - set(latest_default_images))
            new_image = random.choice(available_images)  # nosec B311

            if new_image.startswith("http://") or new_image.startswith("https://"):
                result = new_image.partition("/static-files/")
                if len(result) == 3:
                    new_image = result[len(result) - 2] + result[len(result) - 1]

        except FileNotFoundError as e:
            logger.error(
                "No image found for Audience {audience_name}: {error}".format(audience_name=audience.name, error=str(e))
            )
            return None
    else:
        logger.debug(f"Won't set a new image for event with audience ID {audience_id}. Default image: {new_image}")

    return new_image
