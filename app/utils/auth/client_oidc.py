import logging

import requests
from authlib.jose import jwk, jwt
from authlib.jose.errors import ExpiredTokenError
from authlib.oidc.core import ImplicitIDToken, UserInfo
from flask import current_app, g, request
from flask_restx import abort
from jwt import DecodeError

from app.extensions import cache

logger = logging.getLogger("webapp.api")


class ImplicitIDTokenNoNonce(ImplicitIDToken):
    """
    Don't validate the nonce claim as it's not coming with the token
    """

    ESSENTIAL_CLAIMS = ["iss", "sub", "aud", "exp", "iat"]


@cache.memoize(30)
def get_jkws_json():
    jwk_set = requests.get(current_app.config["OIDC_CONFIG"]["OIDC_JWKS_URL"], timeout=3).json()
    return jwk_set


def parse_id_token(id_token):
    def load_key(header, payload):
        jwk_set = get_jkws_json()
        return jwk.loads(jwk_set, header.get("kid"))

    claims_params = {"client_id": current_app.config["OIDC_CONFIG"]["OIDC_CLIENT_ID"]}
    claims_cls = ImplicitIDTokenNoNonce
    claims_options = {
        "iss": {"values": [current_app.config["OIDC_CONFIG"]["OIDC_ISSUER"]]},
        "nonce": {"validate": lambda x: True},
    }
    claims = jwt.decode(
        id_token,
        key=load_key,
        claims_cls=claims_cls,
        claims_options=claims_options,
        claims_params=claims_params,
    )
    claims.validate(leeway=120)
    return UserInfo(claims)


def get_user_frofile_from_token(user_info: UserInfo):
    """Get the user profile from the token

    Args:
        user_info (UserInfo): [description]

    Returns:
        User: [description]
    """
    profile = {
        "email": user_info.email,
        "first_name": user_info.given_name,
        "last_name": user_info.family_name,
        "username": user_info.sub,
        "roles": user_info.get("cern_roles", []),
    }
    profile["is_admin"] = False
    if current_app.config["ADMIN_ROLE"] in profile["roles"]:
        profile["is_admin"] = True

    return profile


def set_dummy_user(*args, **kwargs):
    g.user = {
        "email": "username@cern.ch",
        "first_name": "First Name",
        "last_name": "Last Name",
        "uid": "usernameuid",
        "roles": ["TEST_ROLE"],
    }


def set_dummy_api_key(*args, **kwargs):
    print("Set dummy account_id")
    g.account_id = 1
    g.username = "the_director"
    g.first_name = "Steven"
    g.last_name = "Spielberg"
    g.is_active = True
    g.get_id = lambda: 1


def oidc_validate(func):
    """
    Decorator for validation of the auth token
    """

    def function_wrapper(*args, **kwargs):
        if current_app.config["LOGIN_DISABLED"]:
            set_dummy_user()
            return func(*args, **kwargs)
        try:
            auth_header = request.headers["Authorization"]
            token = auth_header.split("Bearer")[1].strip()
            user_info = parse_id_token(token)
            user = get_user_frofile_from_token(user_info)
            g.user = user
        except (KeyError, ExpiredTokenError, DecodeError) as error:
            logger.warning(f"Authentication error: {error}")
            abort(401, message="Authorization Denied")  # type: ignore
        return func(*args, **kwargs)

    function_wrapper.__name__ = func.__name__
    return function_wrapper
