from datetime import datetime
from typing import Union


def get_date_as_string(date: datetime) -> Union[str, None]:
    """Convert a date to a formatted string

    Args:
        date (datetime.datetime): The date to be converted

    Returns:
        str: The date as a string
    """
    # pylint: disable=no-member
    if date:
        str_date = date.strftime("%d/%m/%Y, %H:%M:%S")
        return str_date
    return None
