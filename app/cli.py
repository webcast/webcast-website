import logging

import click
from flask import Flask

from app.extensions import db
from app.handlers.events import EventUpcomingHandler
from app.models.initializers.audiences import (
    add_initial_audiences_to_database,
    add_initial_authorized_users_to_database,
)
from app.models.initializers.streams import (
    add_initial_categories_to_database,
    add_initial_permanent_streams_to_database,
)
from app.plugins.ces_client import CesClient
from app.services.webcast_lite.webcast_lite_cli import webcast_lite_cli
from app.tasks.example import dummy_task


def initialize_cli(app: Flask):
    """
    Add additional commands to the CLI. These are loaded automatically on the main.py

    :param app: App to attach the cli commands to
    :return: None
    """

    @app.cli.command()
    def init_db():
        """
        Initialize the database.
        """
        click.echo("Initializing the db")
        db.create_all()

    @app.cli.command()
    def clear_db():
        """
        Removes all the tables of the database and their content
        """
        click.echo("Clearing the db")
        db.drop_all()
        click.echo("Initializing the db")
        db.create_all()

    @app.cli.command()
    def initialize_data():
        """
        Removes all the tables of the database and their content
        """
        click.echo("Filling the database with sample data")
        add_initial_categories_to_database()
        add_initial_authorized_users_to_database()
        add_initial_audiences_to_database()
        add_initial_permanent_streams_to_database()

    @app.cli.command()
    def update_cds_records():
        with app.app_context():
            cron_logger = logging.getLogger("webapp.cron")
            cron_logger.info("Updating CDS Records from CLI")
            CesClient(logger=cron_logger).get_recent_events()
            cron_logger.info("Finished Updating CDS Records from CLI")

    @app.cli.command()
    def update_upcoming_events():
        with app.app_context():
            cron_logger = logging.getLogger("webapp.cron")
            cron_logger.info("Fetching upcoming events")
            EventUpcomingHandler(logger=cron_logger).fetch_events()
            cron_logger.info("Finished Fetching upcoming events")

    @app.cli.command()
    def trigger_error():
        division_by_zero = 1 / 0
        return division_by_zero

    @app.cli.command()
    def example_task():
        dummy_task.apply_async()

    app.cli.add_command(webcast_lite_cli)
