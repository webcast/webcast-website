import enum
import logging
import re

from flask import current_app, request, url_for
from sqlalchemy_utils import ChoiceType

from app.extensions import db

logger = logging.getLogger("webapp.models")


def build_vod_embed(stream):
    player_url = f"{current_app.config['VIDEO_PLAYER_URL']}/permanent-embed/{stream.id}/"
    logger.debug("Building VOD embed")

    try:
        if stream.player_src:
            player_url = player_url + "?cameraUrl=" + stream.player_src
            return player_url
    except AttributeError:
        pass

    player_url = player_url + "&cameraUrl=" + stream.camera_src
    player_url = player_url + "&slidesUrl=" + stream.slides_src

    return player_url


def build_webcast_embed(indico_id=None):
    player_url = f"{current_app.config['VIDEO_PLAYER_URL']}/embed/i{indico_id}"

    logger.debug("Loading embed")

    return player_url


def get_stream_name(player_url, param_name, source=None):
    m = re.search("smil:(.+?).smil", source)
    logger.debug(m)
    if m:
        found = m.group(1)
        player_url = player_url + f"&{param_name}={found}"
    else:
        m = re.search("/(.+?).smil", source)
        logger.debug(m)
        if m:
            found = m.group(1)
            player_url = player_url + f"&{param_name}={found}"

    return player_url


class PermanentStream(db.Model):
    """
    Represents a permanent webcast
    """

    __tablename__ = "streams"
    required_attributes = ("title", "default_img", "player_src")

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    default_img = db.Column(db.String(255))
    custom_img = db.Column(db.String(255))
    use_default_img = db.Column(db.Boolean, nullable=False, default=True)
    is_embeddable = db.Column(db.Boolean, nullable=False, default=True)
    is_sharable = db.Column(db.Boolean, nullable=False, default=True)
    is_visible = db.Column(db.Boolean, nullable=False, default=True)
    is_protected = db.Column(db.Boolean, nullable=False, default=True)

    player_src = db.Column(db.String(255))
    player_width = db.Column(db.Integer, nullable=False, default=0)
    player_height = db.Column(db.Integer, nullable=False, default=0)

    audience_id = db.Column(db.Integer, db.ForeignKey("audiences.id"))
    category_id = db.Column(db.Integer, db.ForeignKey("categories.id"))

    def __init__(
        self,
        title=None,
        default_img=None,
        player_src=None,
        player_width=None,
        player_height=None,
        is_sharable=True,
        is_visible=True,
        is_embeddable=True,
        use_default_img=True,
        custom_img=None,
    ):
        self.title = title
        self.default_img = default_img
        self.player_src = player_src
        self.player_width = player_width
        self.player_height = player_height
        self.is_sharable = is_sharable
        self.is_visible = is_visible
        self.is_embeddable = is_embeddable
        self.use_default_img = use_default_img
        self.custom_img = custom_img

        for key in self.required_attributes:
            if self.__dict__[key] is None:
                raise ValueError("All the {} values must be fulfilled: {}".format(self.__class__.__name__, str(key)))

    def get_stream_url(self):
        url_root = request.url_root[:-1]

        return "{}{}".format(url_root, url_for("users.play_webcast", stream_id=self.id))

    def get_stream_embed_code(self):
        player_url = build_vod_embed(stream=self)

        result = (
            "<iframe "
            'type="text/html" width="720" height="360" '
            f'src="{player_url}" allowfullscreen="" frameborder="0">'
            "</iframe>"
        )
        return result

    def to_json(self):
        return {
            "id": self.id,
            "title": self.title,
            "default_img": self.default_img,
            "custom_img": self.custom_img,
        }

    def to_json_details(self):
        return {
            "id": self.id,
            "title": self.title,
            "stream": {
                "camera_src": self.player_src,
                "default_img": self.default_img,
                "custom_img": self.custom_img,
                "player_url": current_app.config.get("VIDEO_PLAYER_URL", ""),
                "embed_code": self.get_stream_embed_code(),
            },
        }


class Category(db.Model):
    """
    Permanent Stream Category
    """

    __tablename__ = "categories"
    required_attributes = ("name", "default_img", "use_default_img", "priority")

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    default_img = db.Column(db.String(255))
    custom_img = db.Column(db.String(255))
    use_default_img = db.Column(db.Boolean, nullable=False, default=True)

    priority = db.Column(db.Integer, nullable=False)

    streams = db.relationship("PermanentStream", backref="category", lazy="dynamic")

    def __init__(self, name=None, default_img=None, use_default_img=True, priority=None):
        self.name = name
        self.default_img = default_img
        self.use_default_img = use_default_img
        self.priority = priority

        for key in self.required_attributes:
            if self.__dict__[key] is None:
                raise ValueError(f"All the {self.__class__.__name__} values must be fulfilled: {str(key)}")

    def __repr__(self):
        return f"<Category {self.name} use_default_img: {self.use_default_img}>"

    def to_json_v2(self):
        return {
            "id": self.id,
            "name": self.name,
            "default_image": self.default_img,
            "custom_image": self.custom_img,
            "priority": self.priority,
            "streams": self.get_streams_json(),
        }

    def get_streams_json(self):
        results = []
        for stream in self.streams:
            results.append(stream.to_json())
        return results


class VideoQuality(enum.Enum):
    """
    Different qualities allowed for the room_video_quality
    """

    HD = "HD"
    SD = "SD"
    OTHER = "OTHER"


LIVESTREAM_TYPES = [
    ("camera", "Camera"),
    ("camera_slides", "Camera & Slides"),
    ("embed", "Embed"),
]


class LiveStream(db.Model):
    """
    Represents an Event LiveStream
    """

    __tablename__ = "live_streams"

    id = db.Column(db.Integer, primary_key=True)

    type = db.Column(ChoiceType(LIVESTREAM_TYPES), default="camera_slides")
    is_enabled = db.Column(db.Boolean, nullable=False, default=True)
    title = db.Column(db.String(255), info={"label": "Title"})

    camera_src = db.Column(db.String(255), info={"label": "Camera Source"})
    slides_src = db.Column(db.String(255), info={"label": "Slides Souce"})
    room_video_quality = db.Column(db.Enum(VideoQuality), default=VideoQuality.HD)
    app_name = db.Column(db.String(100), default="livehd")

    stream_camera_name = db.Column(db.String(100), info={"label": "Camera Stream"})

    smil_camera = db.Column(db.String(100), info={"label": "Camera SMIL file"})

    smil_slides = db.Column(db.String(100), info={"label": "Slides SMIL file"})
    # Used as slides source
    stream_slides_name = db.Column(db.String(100), info={"label": "Slides Stream"})
    smil_camera_mobile = db.Column(db.String(100))
    smil_slides_mobile = db.Column(db.String(100))
    player_version = db.Column(db.String(45))
    embed_code = db.Column(db.Text)
    is_360 = db.Column(db.Boolean, nullable=True, default=False)

    use_smil = db.Column(
        db.Boolean,
        nullable=False,
        default=True,
        info={"label": "Require fetched SMIL files"},
    )
    camera_smil_fetched = db.Column(db.Boolean, nullable=False, default=False)
    slides_smil_fetched = db.Column(db.Boolean, nullable=False, default=False)

    event_id = db.Column(db.Integer, db.ForeignKey("events.id"))

    def __init__(self, type="camera_slides", camera_src=None, slides_src=None):
        self.type = type
        self.camera_src = camera_src
        self.slides_src = slides_src

        if not type:
            raise ValueError("'type' is a required attribute")

    def __repr__(self):
        return "<LiveStream {} type: {} camera_src: {} slides_src: {} use_smil: {}>".format(
            self.id, self.type, self.camera_src, self.slides_src, self.use_smil
        )

    def set_default_values(self, event):
        """
        Sets the values of the LiveStream based on It's attributes

        :param event: Needed to get the indico_id
        :return:
        """
        logger.debug("Room video quality is: {} vs {}".format(self.room_video_quality, VideoQuality.SD.value))
        if not self.room_video_quality:
            """
            If the video quality is HD, all the streams end with '_all'.

            If the quality is not set, we set it to HD by default
            """
            self.room_video_quality = VideoQuality.HD.value
            self.app_name = "livehd"
        if self.room_video_quality == VideoQuality.HD.value:
            self.stream_camera_name = "{}_camera".format(event.indico_id)
            self.stream_slides_name = "{}_slides".format(event.indico_id)

            self.smil_camera = "{}_camera_all".format(event.indico_id)
            self.smil_slides = "{}_slides_all".format(event.indico_id)

        elif self.room_video_quality == VideoQuality.SD.value:
            """
            If the quality is SD, the streams names are twice the type: _camera_camera _slides_slides
            """
            logger.debug("Setting up camera stream and slides for SD")
            self.stream_camera_name = "{}_camera".format(event.indico_id)
            self.stream_slides_name = "{}_slides".format(event.indico_id)

            self.smil_camera = "{}_camera_camera".format(event.indico_id)
            self.smil_slides = "{}_slides_slides".format(event.indico_id)
        else:
            """
            If the quality is OTHER, we won't set s suffix for the stream
            """
            self.stream_camera_name = "{}".format(event.indico_id)
            self.stream_slides_name = "{}".format(event.indico_id)

            self.smil_camera = "{}".format(event.indico_id)
            self.smil_slides = "{}".format(event.indico_id)

        """
        We set the source for the slides and for the camera with the object attributes
        """
        self.camera_src = "{wowza_server}/{app_name}/smil:{smil_camera}.smil/playlist.m3u8".format(
            wowza_server=current_app.config.get("WOWZA_EDGE_URL"),
            app_name=self.app_name,
            smil_camera=self.smil_camera,
        )
        self.slides_src = "{wowza_server}/{app_name}/smil:{smil_slides}.smil/playlist.m3u8".format(
            wowza_server=current_app.config.get("WOWZA_EDGE_URL"),
            app_name=self.app_name,
            smil_slides=self.smil_slides,
        )

    def get_json(self):
        return {
            "type": str(self.type),
            "app_name": self.app_name,
            "camera_stream": self.smil_camera,
            "slides_stream": self.smil_slides,
            "edge_url": current_app.config.get("WOWZA_EDGE_URL"),
            "camera_src": self.camera_src,
            "slides_src": self.slides_src,
            "player_url": current_app.config.get("VIDEO_PLAYER_URL", ""),
        }

    def has_dvr(self):
        dvr_suffix = "?dvr"
        # if the camera_src or slides_src ends with the ?DVR suffixe, return True
        if (self.camera_src and self.camera_src.lower().endswith(dvr_suffix)) or (
            self.slides_src and self.slides_src.lower().endswith(dvr_suffix)
        ):
            return True
        return False
