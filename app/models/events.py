import datetime
import enum
import logging
import operator
import random
import string

import pytz
import sqlalchemy_utils
from flask import request, url_for
from wtforms import widgets

from app.common.datetime_helper import add_suffix_to_date
from app.extensions import db
from app.models.streams import build_webcast_embed

logger = logging.getLogger("webapp.models")

PYTZ_TIMEZONES = {x: x for x in sorted(pytz.all_timezones)}
AVAILABLE_TIMEZONES = sorted(PYTZ_TIMEZONES.items(), key=operator.itemgetter(0))


class DateTimeMixin(object):
    available_timezones = AVAILABLE_TIMEZONES

    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)

    start_date = db.Column(
        db.DateTime,
        nullable=False,
        info={"label": "Start date", "widget": widgets.TextInput()},
    )
    end_date = db.Column(
        db.DateTime,
        nullable=False,
        info={"label": "End date", "widget": widgets.TextInput()},
    )
    timezone = db.Column(
        sqlalchemy_utils.types.choice.ChoiceType(available_timezones),
        nullable=False,
        info={"label": "Timezone"},
    )

    timestamp_started = db.Column(db.TIMESTAMP, nullable=True)
    timestamp_ended = db.Column(db.TIMESTAMP, nullable=True)


class EventStatus(enum.Enum):
    UPCOMING = "Upcoming"
    LIVE = "Live"
    RECENT = "Recent"
    ARCHIVED = "Archived"


class Event(db.Model, DateTimeMixin):
    __tablename__ = "events"

    required_attributes = (
        "indico_id",
        "link",
        "title",
        "audience",
        "timezone",
        "status",
    )

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(
        db.Text,
        default="",
        nullable=False,
        info={"label": "Title", "widget": widgets.TextInput()},
    )
    speakers = db.Column(db.Text, default="", info={"label": "Speakers", "widget": widgets.TextInput()})
    abstract = db.Column(db.Text, default="", info={"label": "Abstract", "widget": widgets.TextArea()})
    room = db.Column(db.String(100), nullable=True)

    link = db.Column(
        db.Text,
        info={
            "label": "Link",
            "description": "leave it blank and won't be displayed",
            "widget": widgets.TextInput(),
        },
    )
    link_label = db.Column(db.Text, default="", info={"label": "Link Label", "widget": widgets.TextInput()})
    default_img = db.Column(db.String(255), nullable=True)
    custom_img = db.Column(db.String(255), nullable=True)
    use_default_img = db.Column(db.Boolean, nullable=False, default=True)

    extra_html = db.Column(db.Text, default="", info={"label": "Extra HTML"})
    status = db.Column(db.Enum(EventStatus), default=EventStatus.UPCOMING)

    indico_id = db.Column(db.String(50), default="", nullable=True)
    indico_category_id = db.Column(db.String(50), default="", nullable=True)
    display_message = db.Column(
        db.Text,
        default="",
        nullable=True,
        info={"label": "Display Message", "widget": widgets.TextInput()},
    )
    is_synchronized = db.Column(db.Boolean, nullable=False, default=True)
    is_embeddable = db.Column(db.Boolean, nullable=False, default=True, info={"label": "Embeddable"})
    is_sharable = db.Column(db.Boolean, nullable=False, default=True, info={"label": "Sharable"})
    is_visible = db.Column(db.Boolean, nullable=False, default=True, info={"label": "Visible"})
    is_follow_up = db.Column(db.Boolean, nullable=False, default=False)
    is_protected = db.Column(db.Boolean, nullable=False, default=False)

    # Used for the preview
    random_string = db.Column(db.String(255))

    origin_server = db.Column(db.String(255), nullable=True)

    # define relationship
    live_stream = db.relationship("LiveStream", backref="event", uselist=False)

    audience_id = db.Column(db.Integer, db.ForeignKey("audiences.id"), nullable=False)

    follow_ups = db.relationship("FollowUp", backref="event", cascade="all, delete-orphan")

    webcast_lite_settings = db.relationship("WebcastLiteSettings", backref="event", uselist=False, cascade="all,delete")

    def __init__(
        self,
        room=None,
        indico_id=None,
        link=None,
        title=None,
        audience=None,
        timezone=None,
        status=None,
    ):
        self.room = room
        self.indico_id = indico_id
        self.link = link
        self.title = title
        self.audience = audience
        self.timezone = timezone
        self.status = status

        self.random_string = self.get_random_string(16)

        for key in self.required_attributes:
            if self.__dict__[key] is None:
                raise ValueError("All the {} values must be fulfilled: {}".format(self.__class__.__name__, str(key)))

    def __repr__(self):
        return "<Event({}, indico_id: {}>".format(self.id, self.indico_id)

    def get_random_string(self, length):
        """
         Get a random string of length

        :param length: The length of the string
        :type length: int
        :return: The random string
        :rtype: str
        """
        letters = string.ascii_lowercase
        result_str = "".join(random.choice(letters) for i in range(length))  # nosec B311
        # print("Random string of length", length, "is:", result_str)
        return result_str

    def get_date_as_indico(self, date):
        try:
            timezone = self.timezone.value
        except AttributeError:
            timezone = self.timezone

        return {
            "day": date.strftime("%Y-%m-%d"),
            "tz": timezone,
            "time": date.strftime("%H:%M:%S"),
        }

    def to_json(self, use_indico_date=False):
        result = {
            "id": self.id,
            "title": self.title,
            "speakers": self.speakers,
            "abstract": self.abstract,
            "room_name": self.room,
            "event_link": self.link,
            "img": self.get_image_full_url(),
            "restricted": self.is_protected or self.is_restricted(),
            "indico_category": self.indico_category_id,
            "indico_id": self.indico_id,
            "type": self.status.value if self.status else "",
            "webcast_link": self.get_event_url(),
            "ical_link": self.get_event_ical_link(),
            "embed_link_camera": self.get_event_embed_link(),
            "embed_link_slides": self.get_event_embed_link("slides"),
        }

        if use_indico_date:
            result["startDate"] = self.get_date_as_indico(self.start_date)
            result["endDate"] = self.get_date_as_indico(self.end_date)
        else:
            result["start_date"] = self.start_date
            result["end_date"] = self.end_date
            result["timezone"] = self.timezone.value

        return result

    def to_json_v2(self, use_indico_date=False):
        result = {
            "id": self.id,
            "title": self.title,
            "speakers": self.speakers,
            "abstract": self.abstract,
            "room_name": self.room,
            "extra_html": self.extra_html,
            "img": self.get_image_full_url(),
            "restricted": self.is_protected or self.is_restricted(),
            "indico_id": self.indico_id,
            "type": self.status.value if self.status else "",
            "indico_link": self.link,
            "ical_link": self.get_event_ical_link(),
            "has_dvr": self.has_dvr(),
        }

        if use_indico_date:
            result["startDate"] = self.get_date_as_indico(self.start_date)
            result["endDate"] = self.get_date_as_indico(self.end_date)
        else:
            result["start_date"] = self.start_date
            result["end_date"] = self.end_date
            result["timezone"] = self.timezone.value

        return result

    def to_json_v2_with_streams(self, use_indico_date=False):
        if self.status.value == "Archived" and not self.is_older_than(7):
            event_type = "recent"
        else:
            event_type = self.status.value

        result = {
            "id": self.id,
            "title": self.title,
            "speakers": self.speakers,
            "abstract": self.abstract,
            "room_name": self.room,
            "extra_html": self.extra_html,
            "indico_link": self.link,
            "img": self.get_image_full_url(),
            "restricted": self.is_protected or self.is_restricted(),
            "indico_id": self.indico_id,
            "type": event_type,
            "ical_link": self.get_event_ical_link(),
            "has_dvr": self.has_dvr(),
            "stream": self.get_live_stream_json(),
            "embed_code": self.get_event_embed_code(),
            "origin_server": self.origin_server,
        }

        if use_indico_date:
            result["startDate"] = self.get_date_as_indico(self.start_date)
            result["endDate"] = self.get_date_as_indico(self.end_date)
        else:
            result["start_date"] = self.start_date
            result["end_date"] = self.end_date
            result["timezone"] = self.timezone.value

        return result

    def get_live_stream_json(self):
        return self.live_stream.get_json()

    def get_formated_date(self):
        return add_suffix_to_date(self.start_date, "%A {th} %B %Y at %H:%M")

    def get_image_full_url(self):
        url_root = request.url_root[:-1]
        return "{}{}".format(url_root, self.default_img)

    def get_event_url(self):
        url_root = request.url_root[:-1]

        return f"{url_root}/event/i{self.indico_id}"

    def get_event_embed_code(self):
        logger.debug(f"Getting embed code for event {self.id}")

        player_url = build_webcast_embed(self.indico_id)

        if self.status is EventStatus.LIVE:
            player_url = player_url + "&live=true"

        result = (
            '<iframe type="text/html" width="720" height="360" '
            f'src="{player_url}" allowfullscreen="" frameborder="0"</iframe>'
        )
        return result

    def get_event_embed_link(self, stream="camera"):
        return ""

    def get_event_ical_link(self):
        url_root = request.url_root[:-1]

        return "{}{}".format(url_root, url_for("users-utils.get_ical", event_id=self.id))

    def is_restricted(self):
        """
        Check if the event is restricted or not
        If the event category is "No Restriction" or there are not authorized users in the event, it means the event
        is public
        """
        has_restricted_audience = self.audience and self.audience.name.lower() != "no restriction"

        if has_restricted_audience:
            return True
        return False

    def is_older_than(self, days=7):
        delta = datetime.datetime.now() - self.start_date
        if delta.days > days:
            return True
        return False

    def has_dvr(self):
        return self.live_stream.has_dvr()


class FollowUp(db.Model):
    """
    Model for FollowUp
    """

    __tablename__ = "follow_ups"

    id = db.Column(db.Integer, primary_key=True)
    app_name = db.Column(db.String(255), nullable=True)
    origin_server = db.Column(db.String(255), nullable=True)
    stream_name = db.Column(db.String(255), nullable=True)
    stream_type = db.Column(db.String(255), nullable=True)
    on_air = db.Column(db.Boolean, nullable=False, default=False)
    date = db.Column(db.DateTime, default=datetime.datetime.now)

    """
    The follow up will be associated to  a event
    """
    event_id = db.Column(db.Integer, db.ForeignKey("events.id"))

    def __repr__(self):
        return (
            f"<FollowUp({self.id}, {self.app_name}, "
            f"stream_name={self.stream_name}): {self.origin_server} {self.on_air}>"
        )


class CDSRecord(db.Model):
    __tablename__ = "cds_records"
    required_attributes = ("cds_record_id", "indico_link")

    id = db.Column(db.Integer, primary_key=True)
    cds_record_id = db.Column(db.String(512))
    indico_id = db.Column(db.String(512), default="", nullable=False)
    published_date = db.Column(db.DateTime)
    indico_link = db.Column(db.Text)
    image_url = db.Column(db.Text)
    speakers = db.Column(db.Text)
    featured = db.Column(db.Boolean, default=False)
    room = db.Column(db.String(512))
    title = db.Column(db.String(512))
    is_protected = db.Column(db.Boolean, nullable=False, default=False)

    def __init__(self, cds_record_id=None, indico_link=None):
        self.cds_record_id = cds_record_id
        self.indico_link = indico_link

        for key in self.required_attributes:
            if self.__dict__[key] is None:
                raise ValueError("All the {} values must be fulfilled: {}".format(self.__class__.__name__, str(key)))

    def get_cds_link(self):
        return "https://cds.cern.ch/record/" + str(self.cds_record_id)

    def to_json_v2(self):
        result = {
            "id": self.id,
            "title": self.title,
            "image_url": self.image_url,
            "speakers": self.speakers,
            "published_date": self.published_date.strftime("%Y-%m-%d"),
            "link": self.get_cds_link(),
            "is_protected": self.is_protected,
        }

        return result
