import datetime
import logging

from sqlalchemy import TIMESTAMP

from app.extensions import db
from app.utils.dates import get_date_as_string

logger = logging.getLogger("webapp.models")


class WebcastLiteSettings(db.Model):
    __tablename__ = "webcast_lite_settings"

    id = db.Column(db.Integer, primary_key=True)

    event_generated = db.Column(db.Boolean, nullable=False, default=False)
    update_onsave = db.Column(db.Boolean, nullable=False, default=True)

    created_on = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_on = db.Column(
        TIMESTAMP(),
        nullable=False,
        default=datetime.datetime.now,
        onupdate=datetime.datetime.now,
    )

    event_id = db.Column(db.Integer, db.ForeignKey("events.id"))

    def to_dict(self) -> dict:
        """Return the values of the object as a dictionary

        Returns:
            dict: The object values
        """
        created_on_str = get_date_as_string(self.created_on)
        updated_on_str = get_date_as_string(self.updated_on)

        return {
            "id": self.id,
            "title": self.title,
            "media_id": self.media_id,
            "language": self.language,
            "state": self.state,
            "comments": self.comments,
            "notification_email": self.notification_email,
            "created_on": created_on_str,
            "updated_on": updated_on_str if self.updated_on else None,
        }

    def __repr__(self):
        return f"<WebcastLiteSettings {self.id} generated={self.event_generated} update_on_save={self.update_onsave}>"
