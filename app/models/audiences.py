from wtforms import widgets

from app.extensions import db

"""
Needed for the relationship between Audience and AuthorizedUser
"""
audiences_authorized_users = db.Table(
    "audiences_authorized_users",
    db.Column("audience_id", db.Integer, db.ForeignKey("audiences.id"), nullable=False),
    db.Column(
        "authorized_user_id",
        db.Integer,
        db.ForeignKey("authorized_users.id"),
        nullable=False,
    ),
    db.PrimaryKeyConstraint("audience_id", "authorized_user_id"),
)


class AuthorizedUser(db.Model):
    """
    Model for the authorization AuthorizedUser
    """

    __tablename__ = "authorized_users"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False, info={"label": "name", "widget": widgets.TextInput()})


class Audience(db.Model):
    """
    Model for authorization Audience
    """

    __tablename__ = "audiences"
    required_attributes = ("name",)

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False, info={"label": "Name"})
    default_application = db.Column(db.String(255), info={"label": "Default Application"})

    authorized_users = db.relationship("AuthorizedUser", secondary=audiences_authorized_users, backref="audiences")

    streams = db.relationship("PermanentStream", backref="audience", lazy="dynamic")
    events = db.relationship("Event", backref="audience", lazy="dynamic")

    def __init__(self, name=None, default_application=None):
        self.name = name
        self.default_application = default_application

        for key in self.required_attributes:
            if self.__dict__[key] is None:
                raise ValueError("All the {} values must be fulfilled: {}".format(self.__class__.__name__, str(key)))
