import datetime
import uuid

from sqlalchemy_utils import ChoiceType

from app.extensions import db

API_RESOURCES = [
    ("notification", "Notification"),
    ("stream_update", "Stream Update"),
    ("export", "Export"),
]


class ApiKey(db.Model):
    """
    Model for the Api Keys management
    """

    __tablename__ = "api_keys"
    required_attributes = ("name", "description", "status", "resources")

    id = db.Column(db.Integer, primary_key=True)
    access_key = db.Column(db.String(255), nullable=False)
    secret_key = db.Column(db.String(255), nullable=False)
    name = db.Column(db.String(50), nullable=False)
    description = db.Column(db.Text, nullable=False)
    status = db.Column(db.Boolean, nullable=False, default=True)
    resources = db.Column(ChoiceType(API_RESOURCES), default="export")
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow())

    def __init__(self, name=None, description=None, status=None, resources=None):
        self.name = name
        self.description = description
        self.status = status
        self.resources = resources

        self.access_key = ApiKey.generate_key()
        self.secret_key = ApiKey.generate_secret()

        for key in self.required_attributes:
            if self.__dict__[key] is None:
                raise ValueError("All the {} values must be fulfilled: {}".format(self.__class__.__name__, str(key)))

    @staticmethod
    def generate_key():
        return uuid.uuid4().hex

    @staticmethod
    def generate_secret():
        return str(uuid.uuid4())

    def __repr__(self):
        return "<ApiKey {} access_key: {} status: {}>".format(self.name, self.access_key, self.status)
