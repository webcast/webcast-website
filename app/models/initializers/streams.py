import click
from sqlalchemy.orm.exc import NoResultFound

from app.extensions import db
from app.models.audiences import Audience
from app.models.streams import Category, PermanentStream


def add_initial_categories_to_database():
    categories = [
        {
            "name": "Atlas",
            "priority": 1,
            "default_img": "/static-files/images/default/categories/atlas/category_atlas.png",
        },
        {
            "name": "CAST",
            "priority": 2,
            "default_img": "/static-files/images/default/categories/cast/category_cast.png",
        },
        {
            "name": "Development",
            "priority": 3,
            "default_img": "/static-files/images/default/categories/Development/permanent_noimage.jpg",
        },
        {
            "name": "Microcosm",
            "priority": 4,
            "default_img": "/static-files/images/default/categories/Microcosm/Microcosm.jpg",
        },
    ]

    for category in categories:
        try:
            Category.query.filter_by(name=category["name"]).one()
            click.echo("Category {} already exists. Skipping.".format(category["name"]))
        except NoResultFound:
            pass
            new_category = Category(
                name=category["name"],
                priority=category["priority"],
                default_img=category["default_img"],
            )
            db.session.add(new_category)
            db.session.commit()
            click.echo("Added Category {}. Ok.".format(category["name"]))


def add_initial_permanent_streams_to_database():
    streams = [
        {
            "title": "Control Room",
            "audience": "atlas",
            "category": "atlas",
            "default_img": "/static-files/images/default/categories/atlas/Control_room.jpg",
            "player_src": "https://wowza.cern.ch/AtlasLive/smil:stream2.smil/playlist.m3u8",
            "player_width": 1280,
            "player_height": 720,
            "is_protected": True,
        },
    ]

    for stream in streams:
        try:
            PermanentStream.query.filter_by(title=stream["title"]).one()
            click.echo("Stream {} already exists. Skipping.".format(stream["title"]))
        except NoResultFound:
            new_stream = PermanentStream(
                title=stream["title"],
                player_src=stream["player_src"],
                player_width=stream["player_width"],
                player_height=stream["player_height"],
                default_img=stream["default_img"],
            )

            try:
                audience = Audience.query.filter_by(name=stream["audience"]).one()
                new_stream.audience_id = audience.id
            except NoResultFound:
                click.echo(
                    "Audience {} doesn't exist. Unable to add it to PermanentStream {}".format(
                        stream["audience"], stream["title"]
                    )
                )

            try:
                category = Category.query.filter_by(name=stream["category"]).one()
                new_stream.category_id = category.id
            except NoResultFound:
                click.echo(
                    "Category {} doesn't exist. Unable to add it to PermanentStream {}".format(
                        stream["category"], stream["title"]
                    )
                )

            db.session.add(new_stream)
            db.session.commit()
            click.echo("Added PermanentStream {}. Ok.".format(stream["title"]))
