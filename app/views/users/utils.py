import logging

import icalendar
import sqlalchemy
from flask import Response, render_template

from app.models.events import Event
from app.views.blueprints import utils_blueprint

logger = logging.getLogger("webapp.ical")


@utils_blueprint.route("/ical/<event_id>/")
def get_ical(event_id):
    """
    View that generates an ical file from the event details
    :param event_id: Event ID to generate the file from
    :return: The ics file
    """
    try:
        event = Event.query.get_or_404(event_id)
    except (sqlalchemy.exc.DataError, sqlalchemy.exc.InternalError) as e:
        logger.warning("Error getting ical for event ID: {}: {}".format(event_id, str(e)))
        return render_template("404.html"), 404

    def generate():
        cal = icalendar.Calendar()
        ical_event = icalendar.Event()

        """
        BEGIN:VCALENDAR
        VERSION:2.0
        PRODID:-//CERN//WEBCAST//EN
        BEGIN:VEVENT
        UID:webcast-577810@cern.ch
        DTSTAMP:20170303T140344
        DTSTART:20170306T080000
        DTEND:20170310T183000
        SUMMARY:CLIC Workshop 2017
        CATEGORIES:Audience: No restriction
        LOCATION:CERN: Several rooms
        URL:https://webcast.web.cern.ch/webcast/play.php?event=577810
        END:VEVENT
        END:VCALENDAR
        """

        ical_event.add("summary", event.title)
        ical_event.add("dtstart", event.start_date)
        ical_event.add("dtend", event.end_date)
        if event.room:
            ical_event.add("location", "CERN:" + event.room)
        ical_event.add("uid", "webcast-" + event.indico_id + "@cern.ch")
        ical_event.add("categories", "Audience: " + event.audience.name)
        ical_event.add("url", event.link)

        cal.add_component(ical_event)

        return cal.to_ical()

    return Response(
        generate(),
        headers={
            "Content-Disposition": "attachment;filename=event.ics",
            "Content-type": "text/calendar; charset=utf-8",
        },
    )
