from urllib.parse import urljoin

from feedwerk.atom import AtomFeed
from flask import request

from app.models.events import CDSRecord, Event, EventStatus
from app.views.blueprints import feeds_blueprint


def make_external(url):
    return urljoin(request.url_root, url)


@feeds_blueprint.route("/")
def events_feed():

    feed_type = request.args.get("type", None)

    if feed_type == "recent":
        return get_feed_for_recent_events()
    if feed_type == "live":
        return get_feed_for_live_events()
    if feed_type == "upcoming":
        return get_feed_for_upcoming_events()

    return get_feed_for_all_events()


def get_feed_for_all_events():
    feed = AtomFeed(
        "CERN Webcast All Events",
        feed_url=request.url,
        url=request.url_root,
        subtitle="All events",
    )
    events = Event.query.filter_by(is_protected=False).order_by(Event.start_date.desc()).limit(15).all()
    for event in events:
        if not event.is_restricted():
            build_feed_element(feed, event, "All")

    return feed.get_response()


def build_feed_element(feed, event, category):
    event_img = event.default_img
    if event.custom_img and not event.use_default_img:
        event_img = event.custom_img

    feed.add(
        event.title,
        "",
        content_type="html",
        summary=(
            f"<p>Start: {event.start_date}, End: {event.end_date}, Timezone: {event.timezone}<p>"
            f"<div>{event.abstract}</div> <img src='{event_img}' alt='{event.title} image' />"
        ),
        summary_type="html",
        author=event.speakers,
        url=make_external(event.get_event_url()),
        updated=event.updated_at,
        published=event.created_at,
        categories=[{"name": category}],
        id=event.indico_id,
    )


def get_feed_for_upcoming_events():
    feed = AtomFeed(
        "CERN Webcast Upcoming Events",
        feed_url=request.url,
        url=request.url_root,
        subtitle="Upcoming Events",
    )
    events = (
        Event.query.filter_by(status=EventStatus.UPCOMING, is_protected=False)
        .order_by(Event.start_date.desc())
        .limit(15)
        .all()
    )
    for event in events:
        if not event.is_restricted():
            build_feed_element(feed, event, "Upcoming")

    return feed.get_response()


def get_feed_for_live_events():
    feed = AtomFeed("CERN Webcast Live Events", feed_url=request.url, url=request.url_root)
    events = Event.query.filter_by(status=EventStatus.LIVE, is_protected=False).order_by(Event.start_date.desc()).all()
    for event in events:
        if not event.is_restricted():
            build_feed_element(feed, event, "Live")

    return feed.get_response()


def get_feed_for_recent_events():
    feed = AtomFeed("CERN Webcast Recent events", feed_url=request.url, url=request.url_root)
    cds_records = CDSRecord.query.order_by(CDSRecord.published_date.desc()).limit(15).all()
    for cds_record in cds_records:
        feed.add(
            cds_record.title,
            "",
            content_type="html",
            summary="<img src={}/> <br/> Speaker: {}".format(cds_record.image_url, cds_record.speakers),
            summary_type="html",
            author=cds_record.speakers,
            url="https://cdsweb.cern.ch/record/{}".format(cds_record.cds_record_id),
            updated=cds_record.published_date,
            published=cds_record.published_date,
            categories=[{"name": "Recent"}],
        )
        pass

    return feed.get_response()
