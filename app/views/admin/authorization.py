import logging

from flask import flash, redirect, render_template, request, url_for
from sqlalchemy.orm.exc import NoResultFound

from app.decorators import admin_required, requires_login
from app.extensions import db
from app.forms.admin.events import AudienceForm, AuthorizedUserForm
from app.models.audiences import Audience, AuthorizedUser
from app.views.blueprints import authorization_blueprint

logger = logging.getLogger("webapp.events")


def view_audience_dlc(*args, **kwargs):
    """
    Will load the information needed for the breadcrums in Audience edit view.

    :param args: Must include audience_id
    :param kwargs: Not used
    :return: A list with a dictionary that includes the text of the breadcrumb link
    """
    audience_id = request.view_args["audience_id"]
    audience = Audience.query.get(audience_id)
    return [{"text": "Edit: " + str(audience.id)}]


def view_authorized_user_dlc(*args, **kwargs):
    """
    Will load the information needed for the breadcrums in AuthorizedUser edit view.

    :param args: Must include authorized_user_id
    :param kwargs: Not used
    :return: A list with a dictionary that includes the text of the breadcrumb link
    """
    authorized_user_id = request.view_args["authorized_user_id"]
    authorized_user = AuthorizedUser.query.get(authorized_user_id)
    return [{"text": "Edit Authorized User: " + str(authorized_user.id)}]


@authorization_blueprint.route("/audiences")
@requires_login
@admin_required
def audiences():
    """
    Lists audiences and authorized_users view

    :return:
    """
    audiences = Audience.query.all()
    authorized_users = AuthorizedUser.query.all()

    return render_template(
        "admin/authorization/audiences.html",
        audiences=audiences,
        authorized_users=authorized_users,
    )


@authorization_blueprint.route("/audiences/authorized-users")
@requires_login
@admin_required
def authorized_users():
    """
    We don't want a view for authorized users only, so we redirect to audiences
    :return:
    """
    return redirect(url_for("admin-authorization.audiences"))


@authorization_blueprint.route("/audiences/add", methods=["GET", "POST"])
@requires_login
@admin_required
def audiences_add():
    """
    View to add a new Audience
    :return:
    """
    form = AudienceForm()

    if form.validate_on_submit():
        audience = Audience(name=form.name.data, default_application=form.default_application.data)

        authorized_users = form.authorized_users.data
        users_array = []
        for user in authorized_users:
            found_user = AuthorizedUser.query.get(user)
            if found_user:
                users_array.append(found_user)

        audience.authorized_users = users_array

        db.session.add(audience)
        db.session.commit()
        flash("Audience was added successfully", "success")
        return redirect(url_for("admin-authorization.audiences"))
    else:
        logger.debug(form.authorized_users.data)

    return render_template("admin/authorization/audiences-add.html", form=form)


@authorization_blueprint.route("/audiences/<audience_id>/edit", methods=["GET", "POST"])
@requires_login
@admin_required
def audiences_edit(audience_id):
    """
    View to edit an existing Audience

    :param audience_id: Audience id to edit
    :return:
    """
    try:
        audience = Audience.query.get(audience_id)

        """
        Don't try to populate the form using the obj= keyword. It is causes too much PAIN.
        """
        form = AudienceForm(
            name=audience.name,
            default_application=audience.default_application,
            authorized_users=[authorized_user.id for authorized_user in audience.authorized_users],
        )

        if form.validate_on_submit():
            audience.name = form.name.data
            audience.default_application = form.default_application.data

            audience.authorized_users = []
            users_array = []
            for user in form.authorized_users.data:
                found_user = AuthorizedUser.query.get(user)
                if found_user:
                    users_array.append(found_user)
            audience.authorized_users = users_array
            db.session.commit()
            flash("Audience was updated successfully", "success")
            return redirect(url_for("admin-authorization.audiences"))

        return render_template("admin/authorization/audiences-edit.html", audience=audience, form=form)

    except NoResultFound:
        flash("The selected audience does not exist", "error")

    return redirect(url_for("admin-authorization.audiences"))


@authorization_blueprint.route("/audiences/<audience_id>/delete/", methods=["POST"])
@requires_login
@admin_required
def audiences_delete(audience_id):
    """
    View to delete an Audience by id

    :param audience_id: Audience id to delete
    :return:
    """
    try:
        audience = Audience.query.get(audience_id)
        db.session.delete(audience)
        db.session.commit()
        flash("Audience deleted successfully", "success")

    except NoResultFound:
        flash("The selected audience does not exist", "error")

    return redirect(url_for("admin-authorization.audiences"))


@authorization_blueprint.route("/audiences/authorized-users/<authorized_user_id>/delete", methods=["POST"])
@requires_login
@admin_required
def authorized_users_delete(authorized_user_id):
    """
    Delete Authorized user view

    :param authorized_user_id: AuthorizedUser id to delete
    :return:
    """
    try:
        authorized_user = AuthorizedUser.query.get(authorized_user_id)
        db.session.delete(authorized_user)
        db.session.commit()
        flash("Authorized User deleted successfully", "success")

    except NoResultFound:
        flash("The selected authorized user does not exist", "error")

    return redirect(url_for("admin-authorization.audiences"))


@authorization_blueprint.route("/audiences/authorized-users/<authorized_user_id>/edit/", methods=["GET", "POST"])
@requires_login
@admin_required
def authorized_users_edit(authorized_user_id):
    """
    View to edit an existing AuthorizedUser

    :param authorized_user_id: AuthorizedUser id
    :return:
    """
    try:
        authorized_user = AuthorizedUser.query.get(authorized_user_id)

        form = AuthorizedUserForm(obj=authorized_user)

        if form.validate_on_submit():
            authorized_user.name = form.name.data
            db.session.commit()
            flash("Authorized User was updated successfully", "success")
            return redirect(url_for("admin-authorization.audiences"))

        return render_template(
            "admin/authorization/authorized-users-edit.html",
            authorized_user=authorized_user,
            form=form,
        )

    except NoResultFound:
        flash("The selected audience does not exist", "error")

    return redirect(url_for("admin-authorization.audiences"))


@authorization_blueprint.route("/audiences/authorized-users/add", methods=["GET", "POST"])
@requires_login
@admin_required
def authorized_users_add():
    """
    View to add a new AuthorizedUser

    :return:
    """
    form = AuthorizedUserForm()

    if form.validate_on_submit():
        """
        AuthorizedUser model is really simple, so we can use directly the populate_obj ModelForm method to
        add its data.
        """
        try:
            authorized_user = AuthorizedUser()
            form.populate_obj(authorized_user)
            db.session.add(authorized_user)
            db.session.commit()
            flash("Authorized user was added successfully", "success")
        except Exception:
            flash("Authorized user cannot be added", "error")
        return redirect(url_for("admin-authorization.audiences"))

    return render_template("admin/authorization/authorized-users-add.html", form=form)
