import os

from flask import current_app, jsonify, redirect, render_template, request, session

from app.common.utils import get_images_from_folder, is_subdir
from app.decorators import admin_required, requires_login
from app.views.blueprints import admin_blueprint


@admin_blueprint.route("/")
@requires_login
@admin_required
def index():
    """
    Admin home view
    :return:
    """

    if session.get("next_url"):
        next_url = session.get("next_url")
        session.pop("next_url", None)
        return redirect(next_url)

    return render_template("admin/home.html")


@admin_blueprint.route("_get_images_path")
@requires_login
@admin_required
def _get_images_path():
    """
    Gets a relative path from the query args and fins all the images in that path, checking if they
    are in a subdirectory of the application default images folder
    :return: A list with relative images in the path or an empty list
    """
    images_path = request.args.get("images_path", 0, type=str)
    files_found = []

    """
    We check if It is a subdir for reasons of security
    """
    if is_subdir(
        images_path,
        os.path.join(
            current_app.config.get("STATIC_FILES_PATH"),
            current_app.config.get("DEFAULT_IMAGES_FOLDER"),
        ),
    ):
        files_found = get_images_from_folder(images_path)
        return jsonify(result=files_found)

    return jsonify(result=files_found)
