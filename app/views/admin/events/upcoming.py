import logging
import os

from flask import abort, current_app, flash, redirect, render_template, request, url_for
from flask_wtf import FlaskForm
from jinja2 import TemplateNotFound
from sqlalchemy.orm.exc import NoResultFound

from app.common.datetime_helper import add_suffix_to_date
from app.common.utils import str2bool
from app.decorators import admin_required, requires_login
from app.forms.admin.events import EventForm, FetchEventForm, process_event_form
from app.handlers.events import EventUpcomingHandler
from app.models.events import Event, EventStatus
from app.models.streams import LiveStream
from app.views import get_redirect_target
from app.views.admin.events.events import build_upcoming_event_edit_page
from app.views.blueprints import events_blueprint

logger = logging.getLogger("webapp.admin")


@events_blueprint.route("/upcoming")
@requires_login
@admin_required
def events_upcoming():
    """
    View to display a list of Upcoming Events
    :return:
    """
    next_page = get_redirect_target()
    fetch_events_form = FlaskForm()
    fetch_single_event_form = FetchEventForm()
    events = Event.query.filter_by(status=EventStatus.UPCOMING).order_by(Event.start_date).all()
    return render_template(
        "admin/events/upcoming.html",
        events=events,
        fetch_events_form=fetch_events_form,
        next=next_page,
        fetch_single_event_form=fetch_single_event_form,
    )


@events_blueprint.route("/upcoming/fetch", methods=["POST"])
@requires_login
@admin_required
def fetch_upcoming_from_indico():
    """
    View used to fetch all the Upcoming Events from Indico
    :return:
    """
    try:
        event_upcoming_handler = EventUpcomingHandler()
        event_upcoming_handler.fetch_events()
        flash("Upcoming events fetched from Indico", "success")
    except Exception as e:
        flash("Error while fetching events from Indico: {}".format(str(e)), "error")

    return redirect(url_for("admin-events.events_upcoming"))


@events_blueprint.route("/upcoming/fetch-single", methods=["POST"])
@requires_login
@admin_required
def fetch_single_event():
    """
    View used to fetch all the Upcoming Events from Indico
    :return:
    """
    form = FetchEventForm()
    if form.validate_on_submit():
        event_id = form.event_id.data
        event_upcoming_handler = EventUpcomingHandler()
        event_found = event_upcoming_handler.fetch_event_by_indico_id(event_id)
        if event_found:
            flash("Upcoming event fetched from Indico", "success")
        else:
            flash("Event not found on Indico", "error")

    return redirect(url_for("admin-events.events_upcoming"))


@events_blueprint.route("/upcoming/<event_id>/fetch", methods=["POST"])
@requires_login
@admin_required
def fetch_upcoming_from_indico_by_id(event_id):
    """
    View used to fetch one Upcoming Event from Indico by It's ID
    :param event_id: The event ID
    :return:
    """
    event = Event.query.get(event_id)
    try:

        if event.indico_id and event.indico_id != "":
            event_upcoming_handler = EventUpcomingHandler()
            event_upcoming_handler.fetch_event_by_id(event_id)
    except NoResultFound:
        flash("No event found with the ID {}".format(event_id), "error")

    return redirect(url_for("admin-events.upcoming_event_edit", event_id=event_id))


@events_blueprint.route("/upcoming/add", methods=["GET", "POST"])
@requires_login
@admin_required
def upcoming_event_add():
    """
    View for adding new Upcoming Events manually
    :return:
    """
    form = EventForm()
    # TODO Move this to a function
    default_images_folders = [
        x[0]
        for x in os.walk(
            os.path.join(
                current_app.config.get("STATIC_FILES_PATH"),
                current_app.config.get("DEFAULT_IMAGES_FOLDER"),
                current_app.config.get("DEFAULT_EVENTS_FOLDER"),
            )
        )
    ]

    if request.method == "POST":
        if form.validate_on_submit():
            event = process_event_form(EventStatus.UPCOMING, form)
            return redirect(url_for("admin-events.upcoming_event_edit", event_id=event.id))

    return render_template(
        "admin/events/events-upcoming-add.html",
        event_type="Upcoming",
        form=form,
        default_images_folders=default_images_folders,
        wowza_origin_url=current_app.config.get("WOWZA_ORIGIN_URLS")[0],
    )


@events_blueprint.route("/upcoming/<event_id>/edit", methods=["GET", "POST"])
@requires_login
@admin_required
def upcoming_event_edit(event_id):
    """
    View for editing an Upcoming Event

    :param event_id: The internal Event ID
    :return: The edit template
    """
    event_type = request.args.get("type", "upcoming")
    go_live = request.args.get("go_live", None)

    if go_live:
        go_live = str2bool(go_live)

    try:
        return build_upcoming_event_edit_page(event_id=event_id, event_type=event_type, go_live=go_live)
    except TemplateNotFound:
        abort(404)


@events_blueprint.route("/upcoming/<event_id>/preview", methods=["GET"])
def upcoming_event_edit_preview(event_id):

    logger.info("Getting event with Internal ID: {}".format(event_id))
    event = Event.query.get_or_404(event_id)

    if event.status == EventStatus.UPCOMING:

        live_status = EventStatus.LIVE

        formated_date = add_suffix_to_date(event.start_date, "%A {th} %B %Y at %H:%M")
        logger.debug(event.live_stream.type)
        if event.live_stream.type == "camera":
            return render_template(
                "users/view-modes/camera_view.html",
                event=event,
                live_status=live_status,
                formated_date=formated_date,
                is_authorized=True,
                preview=True,
            )
        if event.live_stream.type == "slides":
            return render_template(
                "users/view-modes/slides_view.html",
                event=event,
                live_status=live_status,
                formated_date=formated_date,
                is_authorized=True,
                preview=True,
            )
        return render_template(
            "users/view-modes/camera_slides_view.html",
            event=event,
            live_status=live_status,
            formated_date=formated_date,
            is_authorized=True,
            preview=True,
        )
    else:
        abort(404)


@events_blueprint.route("/live-streams/<live_stream_id>/delete/", methods=["POST"])
@requires_login
@admin_required
def delete_live_stream(live_stream_id):
    """
    View for deleting a LiveStream from an event
    :param live_stream_id:
    :return:
    """
    live_stream = None
    try:
        live_stream = LiveStream.query.get(live_stream_id)
        flash("Live Stream cannot be deleted. An Event must have at least one.", "error")
        return redirect(url_for("admin-events.upcoming_event_edit", event_id=live_stream.event_id))

    except NoResultFound:
        flash("The selected live stream does not exist", "error")

    return redirect(url_for("admin-events.upcoming_event_edit", event_id=live_stream.event_id))
