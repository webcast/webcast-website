import logging

from flask import flash, redirect, render_template, request, url_for
from flask_wtf import FlaskForm
from sqlalchemy.orm.exc import NoResultFound

from app.decorators import admin_required, requires_login
from app.extensions import db
from app.forms.admin.events import CDSRecordForm
from app.models.events import CDSRecord
from app.plugins.ces_client import CesClient
from app.views import redirect_back
from app.views.blueprints import events_blueprint

logger = logging.getLogger("webapp.admin")


def view_cds_record_dlc(*args, **kwargs):
    """
    Will load the information needed for the breadcrums in category edit view.

    :param args: Must include category_id
    :param kwargs: Not used
    :return: A list with a dictionary that includes the text of the breadcrumb link
    """
    cds_record_id = request.view_args["cds_record_id"]
    cds_record = CDSRecord.query.get(cds_record_id)
    return [{"text": "Edit: " + str(cds_record.id)}]


@events_blueprint.route("/recent")
@requires_login
@admin_required
def events_recent():
    """
    View for listing Recent Events

    :return:
    """
    fetch_from_cds_form = FlaskForm()
    cds_records = CDSRecord.query.order_by(CDSRecord.published_date.desc()).all()
    return render_template(
        "admin/events/recent.html",
        cds_records=cds_records,
        fetch_from_cds_form=fetch_from_cds_form,
    )


@events_blueprint.route("/recent/fetch", methods=["POST"])
@requires_login
@admin_required
def fetch_from_cds():
    """
    View used to fetch all the Upcoming Events from Indico
    :return:
    """
    try:
        CesClient().get_recent_events()
        flash("Webcasts fetched from CDS and events updated", "success")
    except Exception as e:
        flash("Error while fetching CDS: {}".format(str(e)), "error")
    return redirect(url_for("admin-events.events_recent"))


@events_blueprint.route("/cds-record/<cds_record_id>/delete/", methods=["POST"])
@requires_login
@admin_required
def cds_record_delete(cds_record_id):
    """
    Deletes an event by ID

    :param cds_record_id: Internal CDS Record ID
    :return:
    """
    try:
        cds_record = CDSRecord.query.get(cds_record_id)
        db.session.delete(cds_record)
        db.session.commit()
        logger.debug("CDS Record {} deleted successfully".format(cds_record_id))
        flash("Event deleted successfully", "success")

    except NoResultFound:
        flash("The selected CDS Record does not exist", "error")

        logger.debug("Redirecting to admin-events.events_recent...")
    return redirect_back("admin-events.events_recent")


@events_blueprint.route("/cds-records/<cds_record_id>/edit", methods=["GET", "POST"])
@requires_login
@admin_required
def cds_records_edit(cds_record_id):
    """
    View for permanent stream category edit

    :param cds_record_id: Integer with the id of the category to edit.
    :return:
    """
    cds_record = CDSRecord.query.get(cds_record_id)
    form = CDSRecordForm(obj=cds_record)

    if form.validate_on_submit():
        logger.debug("CDS Record Form is valid")

        cds_record.title = form.title.data
        cds_record.cds_record_id = form.cds_record_id.data
        cds_record.published_date = form.published_date.data
        cds_record.indico_id = form.indico_id.data
        cds_record.indico_link = form.indico_link.data
        cds_record.speakers = form.speakers.data

        #
        #     """
        #     bools have to be treated manually
        #     """
        #     category.use_default_img = form.use_default_img.data
        #
        db.session.commit()
        #
        flash("CDS Record updated successfully", category="success")
        return redirect(url_for("admin-events.cds_records_edit", cds_record_id=cds_record_id))

    return render_template("admin/events/cds-records-edit.html", form=form, cds_record=cds_record)
