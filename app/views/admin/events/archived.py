from flask import abort, flash, redirect, render_template, request, url_for
from jinja2 import TemplateNotFound

from app.common.utils import str2bool
from app.decorators import admin_required, requires_login
from app.extensions import db
from app.models.events import Event, EventStatus
from app.views.admin.events.events import build_archived_events_edit_page
from app.views.blueprints import events_blueprint

ROWS_PER_PAGE = 50


@events_blueprint.route("/archived")
@requires_login
@admin_required
def events_archived():
    """
    View for listing Archived Events

    :return:
    """
    # Set the pagination configuration
    page = request.args.get("page", 1, type=int)

    events = (
        Event.query.filter(Event.status == EventStatus.ARCHIVED)
        .order_by(Event.start_date.desc())
        .paginate(page=page, per_page=ROWS_PER_PAGE)
    )

    return render_template("admin/events/archived.html", events=events)


@events_blueprint.route("/archived/<event_id>/restore", methods=["POST"])
@requires_login
@admin_required
def restore_event(event_id):
    """
    View for listing Archived Events

    :return:
    """
    event = Event.query.get_or_404(event_id)
    event.status = EventStatus.UPCOMING
    event.is_visible = False
    event.is_synchronized = False
    db.session.commit()

    flash("Event {} has been restored. It is now an Upcoming Event".format(event.id))

    return redirect(url_for("admin-events.events_archived"))


@events_blueprint.route("/archived/<event_id>/edit", methods=["GET", "POST"])
@requires_login
@admin_required
def archived_event_edit(event_id):
    """
    View for editing an Archived Event

    :param event_id: The internal Event ID
    :return: The edit template
    """
    event_type = request.args.get("type", "archived")
    go_live = request.args.get("go_live", None)

    if go_live:
        go_live = str2bool(go_live)

    try:
        return build_archived_events_edit_page(event_id=event_id, event_type=event_type, go_live=go_live)
    except TemplateNotFound:
        abort(404)
