import logging
import os

from flask import abort, current_app, flash, redirect, render_template, request, url_for
from jinja2 import TemplateNotFound
from sqlalchemy.orm.exc import NoResultFound

from app.decorators import admin_required, requires_login
from app.forms.admin.events import EventForm, UnpublishEventForm, process_event_form
from app.handlers.events import EventFollowUpHandler
from app.models.events import Event, EventStatus
from app.views.admin.events.events import build_live_event_edit_page
from app.views.blueprints import events_blueprint

logger = logging.getLogger("webapp.events")


@events_blueprint.route("/live")
@requires_login
@admin_required
def events_live():
    try:
        # next_page = get_redirect_target()
        events = Event.query.filter_by(status=EventStatus.LIVE)
        unpublish_forms = []
        for event in events:
            unpublish_forms.append(UnpublishEventForm(event_id=event.id, indico_id=event.indico_id))

        return render_template("admin/events/live.html", events=events, unpublish_forms=unpublish_forms)
    except TemplateNotFound:
        abort(404)


@events_blueprint.route("/live/add", methods=["GET", "POST"])
@requires_login
@admin_required
def live_event_add():
    form = EventForm()
    default_images_folders = [
        x[0]
        for x in os.walk(
            os.path.join(
                current_app.config.get("STATIC_FILES_PATH"),
                current_app.config.get("DEFAULT_IMAGES_FOLDER"),
                current_app.config.get("DEFAULT_EVENTS_FOLDER"),
            )
        )
    ]

    if request.method == "POST":
        if form.validate_on_submit():
            logger.debug("Add Event Form is valid")
            event = process_event_form(EventStatus.LIVE, form)
            return redirect(url_for("admin-events.live_event_edit", event_id=event.id))
        else:
            logger.error(form.errors)

    return render_template(
        "admin/events/events-live-add.html",
        event_type="Live",
        form=form,
        default_images_folders=default_images_folders,
        wowza_origin_url=current_app.config.get("WOWZA_ORIGIN_URLS")[0],
    )


@events_blueprint.route("/live/<event_id>/edit/", methods=["GET", "POST"])
@requires_login
@admin_required
def live_event_edit(event_id):
    """
    View for editing a Live Event

    :param event_id: The internal Event ID
    :return: The edit template
    """
    event_type = request.args.get("type", "live")
    try:
        return build_live_event_edit_page(event_id=event_id, event_type=event_type)
    except TemplateNotFound:
        abort(404)


@events_blueprint.route("/live/<event_id>/un-publish", methods=["POST"])
@requires_login
@admin_required
def unpublish(event_id):
    """
    Makes a Live Event Upcoming again changing it's status to Upcoming.

    :param event_id: The internal Event ID
    :return: Redirects to the upcoming events list
    """
    try:

        form = UnpublishEventForm()

        if form.validate_on_submit():

            logger.info("Unpublishing event manually: {} ".format(event_id))

            event = Event.query.get(event_id)
            event_followup_handler = EventFollowUpHandler()
            event_followup_handler.unpublish(event)

            if event.status == EventStatus.UPCOMING:
                flash("Event #{} is now upcoming".format(event_id), "success")
                return redirect(url_for("admin-events.events_upcoming"))
            if event.status == EventStatus.ARCHIVED:
                flash("Event #{} is now archived".format(event_id), "success")
                return redirect(url_for("admin-events.events_archived"))
        else:
            logger.debug("Form is not valid")

    except NoResultFound:
        flash("No event found with the ID {}".format(event_id), "error")

    return redirect(url_for("admin-events.events_live"))
