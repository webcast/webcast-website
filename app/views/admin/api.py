import logging

from flask import abort, flash, redirect, render_template, request, url_for
from jinja2 import TemplateNotFound
from sqlalchemy.orm.exc import NoResultFound

from app.decorators import admin_required, requires_login
from app.extensions import db
from app.forms.admin.api import ApiKeyForm
from app.models.api import ApiKey
from app.views.blueprints import api_blueprint

logger = logging.getLogger("webapp.api")


def view_api_key_dlc(*args, **kwargs):
    """
    Breadcrumb generator for the ApiKey edit view

    :param args:  Must include key_id
    :param kwargs: not used
    :return:
    """
    api_key_id = request.view_args["key_id"]
    api_key = ApiKey.query.get(api_key_id)
    return [{"text": "Edit: " + str(api_key.id)}]


@api_blueprint.route("/")
@requires_login
@admin_required
def api_keys():
    """
    ApiKey listing view
    :return:
    """
    api_keys = ApiKey.query.all()

    try:
        return render_template("admin/api/index.html", api_keys=api_keys)
    except TemplateNotFound:
        abort(404)


@api_blueprint.route("/add", methods=["GET", "POST"])
@requires_login
@admin_required
def keys_add():
    """
    Add ApiKey view

    :return:
    """
    form = ApiKeyForm()

    if form.validate_on_submit():
        logger.debug("Api Key Form is valid")

        new_api_key = ApiKey(
            name=form.name.data,
            description=form.description.data,
            status=form.status.data,
            resources=form.resources.data,
        )

        db.session.add(new_api_key)
        db.session.commit()
        flash("Api Key was added successfully", "success")
        return redirect(url_for("admin-api.api_keys"))

    else:
        logger.debug("Api Key Form is NOT valid")

    try:
        return render_template("admin/api/keys-add.html", form=form)
    except TemplateNotFound:
        abort(404)


@api_blueprint.route("/<key_id>/edit", methods=["GET", "POST"])
@requires_login
@admin_required
def keys_edit(key_id):
    """
    Edit ApiKey view

    :param key_id: Id of the ApiKey to be edited
    :return:
    """
    try:
        api_key = ApiKey.query.get(key_id)
        #
        # """
        # Don't try to populate the form using the obj= keyword. It is causes too much PAIN.
        # """
        form = ApiKeyForm(
            name=api_key.name,
            description=api_key.description,
            status=api_key.status,
            resources=api_key.resources,
        )
        #
        if form.validate_on_submit():
            api_key.name = form.name.data
            api_key.description = form.description.data
            api_key.status = form.status.data
            api_key.resources = form.resources.data

            db.session.commit()
            flash("Api Key was updated successfully", "success")
            return redirect(url_for("admin-api.api_keys"))
        #
        return render_template("admin/api/keys-edit.html", api_key=api_key, form=form)

    except NoResultFound:
        flash("The selected audience does not exist", "error")

    return redirect(url_for("admin-api.api_keys"))


@api_blueprint.route("/<key_id>/delete/", methods=["POST"])
@requires_login
@admin_required
def keys_delete(key_id):
    """
    Delete ApiKey view

    :param key_id: Id of the key to be deleted
    :return:
    """
    try:
        api_key = ApiKey.query.get(key_id)
        db.session.delete(api_key)
        db.session.commit()
        flash("Api Key deleted successfully", "success")

    except NoResultFound:
        flash("The selected audience does not exist", "error")

    return redirect(url_for("admin-api.api_keys"))
