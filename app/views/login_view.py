from urllib.parse import urlencode

from flask import current_app, redirect, render_template, request, session, url_for
from flask_login import login_required, logout_user

from app.views.blueprints import login_view_blueprint


@login_view_blueprint.route("/")
def index():
    if session.get("next_url"):
        next_url = session.get("next_url")
        session.pop("next_url", None)

        return redirect(next_url)
    return redirect(url_for("admin.index"))


@login_view_blueprint.route("/logout/")
@login_required
def logout():
    """
    View to log out a user on the site
    :return: Redirects to the users.index after logout
    """
    if not current_app.config["OIDC_CONFIG"]["OIDC_LOGOUT_URL"]:
        logout_user()
        return render_template("login.html")

    next_url = request.url_root + url_for("login_view.login")
    query = urlencode(
        {
            "post_logout_redirect_uri": next_url,
            "client_id": current_app.config["CERN_OPENID_CLIENT_ID"],
        }
    )
    logout_user()
    return redirect(current_app.config["OIDC_CONFIG"]["OIDC_LOGOUT_URL"] + "?" + query)


@login_view_blueprint.route("/login/")
def login():
    """
    Display the login page
    :return:
    """
    session["next_url"] = request.args.get("next")
    return render_template("login.html")
