/**
 * Created by renefernandez on 17/02/17.
 */
$(document).ready(function () {
    console.debug("Loading main.js");
    $('.ui.search.dropdown').dropdown();
    $('.mobile.ui.sidebar').sidebar('attach events', '#mobile-hidder');
    loadLiveStreamsTypeDropdown();
});

var newStreamsNumber;

function addFirstTab() {
    var firstTab = $('<a class="tab item active" data-tab="stream-0"></a>');
    $('#stream-menu').append(firstTab);

    var firstTabContent = $('<div class="ui bottom attached tab segment" data-tab="stream-0"></div>');
    $(firstTabContent).insertAfter('#stream-menu');
}

function setTabText(tab_type) {
    var tabText = '';
    switch (tab_type) {
        case 'camera':
            tabText = '<i class="video camera icon"></i> Camera';
            break;
        case'camera_slides':
            tabText = '<i class="youtube play icon"></i> Camera & Slides';
            break;
        case 'embed':
            tabText = 'Embed';
            break;
    }
    return tabText;
}
function setTabContent(tab_type, indico_id, event_id) {
    var tabContent = '';
    switch (tab_type) {
        case 'camera':
            tabContent = document.getElementById('live_stream_camera_form').innerHTML
                .replace(/\{ID}/g, newStreamsNumber);

            if (indico_id) {
                tabContent = tabContent.replace(/\{INDICO_ID}/g, indico_id).replace(/\{EVENT_ID}/g, event_id);
            }
            break;
        case'camera_slides':
            tabContent = document.getElementById('live_stream_camera_slides_form').innerHTML
                .replace(/\{ID}/g, newStreamsNumber);

            if (indico_id && event_id) {
                tabContent = tabContent.replace(/\{INDICO_ID}/g, indico_id).replace(/\{EVENT_ID}/g, event_id);
            }

            break;
        case 'embed':
            tabContent = document.getElementById('live_stream_embed_form').innerHTML
                .replace(/\{ID}/g, newStreamsNumber);

            if (indico_id && event_id) {
                tabContent = tabContent.replace(/\{INDICO_ID}/g, indico_id).replace(/\{EVENT_ID}/g, event_id);
            }
            break;
    }
    return tabContent;
}
function addNewTab(tab_type, indico_id, event_id) {

    var newTabBase = '<a class="tab item" data-tab=""></a>';
    var newTabContent = '<div class="ui bottom attached tab segment" data-tab=""></div>';

    var $lastTab = $('.tabular.main-menu > .item:last-of-type');


    if ($lastTab.data('tab') == undefined) { //There are no tabs
        addFirstTab();
        $lastTab = $('.tabular.main-menu > .item:last-of-type');
        var isNotFirstTab = false;
    } else {
        isNotFirstTab = true;
    }

    var $lastTabContent = $('.tab.segment:last-of-type');

    var $newTabContent = undefined;
    if (isNotFirstTab) {
        var $newTab = $(newTabBase).insertAfter($lastTab);
        $newTabContent = $(newTabContent).insertAfter($lastTabContent);
    } else {
        $newTab = $lastTab;
        $newTabContent = $lastTabContent;
    }
    if (newStreamsNumber === undefined) {
        newStreamsNumber = 0;
    }
    newStreamsNumber++;

    var tabValue = 'stream-' + newStreamsNumber;
    var tabText = "";
    var tabContent = "";
    tabText = setTabText(tab_type);
    tabContent = setTabContent(tab_type, indico_id, event_id);

    $newTab.html(tabText).attr('data-tab', tabValue);
    $newTab.html(tabText).attr('id', 'tab-' + tabValue);
    $('#number-of-streams').val(newStreamsNumber);
    $newTabContent.html(tabContent).attr('data-tab', tabValue);
    $newTabContent.html(tabContent).attr('id', 'tab-content-' + tabValue);


    setTimeout(function () {
        /*
         This silly hack was the only way I found to hide the dropdown properly.
         */
        $('.tabular.menu .item').tab('change tab', tabValue);
    }, 1);
    loadLiveStreamsTypeDropdown();
    loadDeleteNewLiveStream();
    // console.log('data-tab values on last added tab/content\n'
    //     + 'Menu item: ' + $('.tabular.main-menu .item:last-of-type').data('tab') + '\n'
    //     + 'Content item: ' + $('.tab.segment:last-of-type').data('tab'));

}

function loadDeleteNewLiveStream() {
    $('body').on('click', 'button.delete-new-live_stream', function () {
        var self = $(this);
        var stream_id = self.data('stream_id');
        console.debug("Clicked deleteNewLiveStream");
        console.debug("#tab-content-stream-" + stream_id);
        console.debug("#tab-content-" + stream_id);

        $("#tab-stream-" + stream_id).remove();
        $("#tab-content-stream-" + stream_id).remove();
        // $('#number-of-streams').val($('#number-of-streams').val()-1);
        $('.tabular.menu .item').tab('change tab', 'first');
    });
}

function triggerChangeStreamType(value, text, $selectedItem) {
    console.debug("Change type " + value);
    // console.debug($(this));
    // console.debug($(this).children(":first"));
    var currentDivId = $(this).children(":first").first().attr('id');
    var splittedDiv = currentDivId.split("-");
    var currentId = "";

    if (currentDivId.startsWith("existing-stream")) {
        currentId = splittedDiv[2];
    } else {
        currentId = splittedDiv[1];
    }
    console.debug(currentId);
    console.debug("Setting tab text");

    // $("#existing-tab-stream-" + currentId).text(value);

    var result = $('.tabular.menu .item.active').attr('id');
    console.debug(result);

    var tabText = setTabText(value);

    $('#' + result).html(tabText);


    if (value === 'embed') {
        // Hide camera and slides
        $('.slides-only-' + currentId).hide();
        $('.camera-only-' + currentId).hide();
        $('.embed-only-' + currentId).show();
    }

    if (value === 'camera_slides') {
        // Hide embed
        $('.embed-only-' + currentId).hide();
        $('.slides-only-' + currentId).show();
        $('.camera-only-' + currentId).show();
    }

    if (value === 'camera') {
        // Hide slides and embed
        $('.slides-only-' + currentId).hide();
        $('.embed-only-' + currentId).hide();
        $('.camera-only-' + currentId).show();
    }
}

function loadLiveStreamsTypeDropdown() {
    $('.stream-type').dropdown({
        onChange: triggerChangeStreamType
    });
}


function loadFetchCameraSlidesButtons(fetchUrl) {
    $('body').on('click', 'button.fetch-camera-slides-smil', function () {
        console.debug("Fetch clicked");
        console.debug($(this).data());
        var self = $(this);
        var stream_id = self.data('stream_id');

        var stream_camera_name = $("#existing-stream-" + self.data('stream_id') + "-stream_camera_name").val();
        var stream_slides_name = $("#existing-stream-" + self.data('stream_id') + "-stream_slides_name").val();

        let smilCamera = $('#existing-stream-' + stream_id + '-smil_camera').val();
        let smilSlides = $('#existing-stream-' + stream_id + '-smil_slides').val();

        console.debug("#stream-" + self.data('stream_id') + "-stream_camera_name");
        console.debug(stream_camera_name);

        $.getJSON(fetchUrl, {
            event_id: self.data('event_id'),
            indico_id: self.data('indico_id'),
            stream_id: stream_id,
            stream_camera_name: smilCamera,
            stream_slides_name: smilSlides,
            app_name: $("#existing-stream-" + self.data('stream_id') + "-app_name").val()
        }, function (data) {
            console.debug(data);

            $('#success-message-stream-' + stream_id).text("");
            $('#error-message-stream-' + stream_id).text("");

            $('.success-message-stream-' + stream_id).addClass('hidden');
            $('.error-message-stream-' + stream_id).addClass('hidden');


            if (data['result'].camera.result === 200) {
                $('#success-message-stream-' + stream_id).html("SMIL files fetched for camera stream: " + data['result'].camera.path + "<br/>");
                $('.success-message-stream-' + stream_id).removeClass('hidden');
            }

            if (data['result'].slides.result === 200) {
                $('#success-message-stream-' + stream_id).html($('#success-message-stream-' + stream_id).html() + "SMIL files fetched for slides stream: " + data['result'].slides.path);
                $('.success-message-stream-' + stream_id).removeClass('hidden');
            }

            if (data['result'].camera.result === 404 || data['result'].camera.result === 500) {
                $('#error-message-stream-' + stream_id).html("SMIL files NOT fetched for camera stream (Error: " + data['result'].camera.result + "): " + data['result'].camera.path + "<br/>");
                $('.error-message-stream-' + stream_id).removeClass('hidden');
            }

            if (data['result'].slides.result === 404 || data['result'].slides.result === 500) {
                $('#error-message-stream-' + stream_id).html($('#error-message-stream-' + stream_id).html() + "SMIL files NOT fetched for slides stream (Error: " + data['result'].camera.result + "): " + data['result'].slides.path);
                $('.error-message-stream-' + stream_id).removeClass('hidden');
            }

        }).error(function (result) {
            console.debug(result);
            $('#error-message-stream-' + stream_id).html("Unable to fetch smil files (Error 500 on the backend)");
            $('.error-message-stream-' + stream_id).removeClass('hidden');
        });
        return false;
    });
}

function loadDefaultImagesActions(fetchImagesUrl) {
    $("#display-default-images-button").click(function () {
        $("#default-images-modal").modal({observeChanges: true}).modal('show');
    });

    $('body').on('click', '#images-container img', function () {
        console.log("Image clicked");
        console.log($(this).attr('src'));
        $('#default_img').val($(this).attr('src'));
        $('#default-image-placeholder').attr('src', $(this).attr('src'));
        $("#default-images-modal").modal({observeChanges: true}).modal('hide');
    });

    function displayImagesOnFolder(images_path) {
        $.getJSON(fetchImagesUrl, {
            images_path: images_path
        }, function (data) {
            $('#images-container').text('');
            $.each(data.result, function (index, value) {
                console.debug(value);
                $('#images-container').append('<img src="' + value + '"\/>');
                $("#default-images-modal").modal({observeChanges: true});
            });
        });
    }

    $('#default-image-folder-selector')
        .dropdown({
            onChange: function (value, text, $selectedItem) {
                console.log(value);
                displayImagesOnFolder(value);
            }
        });
}

function loadSetDefaultStreamAttributes(wowzaOriginUrl) {
    $(".set-defaults-button").click(function () {

        // We get the selected room quality and the indico_id from their fiedls and then we fill the fields
        let stream_id = $(this).data('stream');
        let existing = $(this).data('existing');
        let quality = $("#" + existing + "stream-" + stream_id + "-room_video_quality").val();
        let indico_id = $("#indico_id").val();

        let app_name = '';
        if (quality === 'HD') {
            app_name = 'livehd';
            stream_camera_name = indico_id + '_camera';
            camera_smil = stream_camera_name + '_all';
            stream_slides_name = indico_id + '_slides';
            slides_smil = stream_slides_name + '_all';

            $('#' + existing + 'stream-' + stream_id + '-app_name').val(app_name);

            $('#' + existing + 'stream-' + stream_id + '-stream_camera_name').val(stream_camera_name);

            $('#' + existing + 'stream-' + stream_id + '-camera_src').val(wowzaOriginUrl + '/' + app_name + '/smil:' + camera_smil + '.smil/playlist.m3u8');

            $('#' + existing + 'stream-' + stream_id + '-stream_slides_name').val(stream_slides_name);

            $('#' + existing + 'stream-' + stream_id + '-slides_src').val(wowzaOriginUrl + '/' + app_name + '/smil:' + slides_smil + '.smil/playlist.m3u8');

            $('#' + existing + 'stream-' + stream_id + '-smil_camera').val(camera_smil);
            $('#' + existing + 'stream-' + stream_id + '-smil_slides').val(slides_smil);

        }

        if (quality === 'SD') {
            app_name = 'livesd';
            stream_camera_name = indico_id + '_camera';
            camera_smil = stream_camera_name + '_camera';
            stream_slides_name = indico_id + '_slides';
            slides_smil = stream_slides_name + '_slides';


            $('#' + existing + 'stream-' + stream_id + '-app_name').val(app_name);


            $('#' + existing + 'stream-' + stream_id + '-stream_camera_name').val(stream_camera_name);


            $('#' + existing + 'stream-' + stream_id + '-camera_src').val(wowzaOriginUrl + '/' + app_name + '/smil:' + camera_smil + '.smil/playlist.m3u8');

            $('#' + existing + 'stream-' + stream_id + '-stream_slides_name').val(stream_slides_name);

            $('#' + existing + 'stream-' + stream_id + '-slides_src').val(wowzaOriginUrl + '/' + app_name + '/smil:' + slides_smil + '.smil/playlist.m3u8');

            $('#' + existing + 'stream-' + stream_id + '-smil_camera').val(camera_smil);
            $('#' + existing + 'stream-' + stream_id + '-smil_slides').val(slides_smil);

        }

        if (quality === 'OTHER') {
            app_name = $("#" + existing + "stream-" + stream_id + "-app_name").val();
            stream_camera_name = $('#' + existing + 'stream-' + stream_id + '-stream_camera_name').val();
            stream_slides_name = $('#' + existing + 'stream-' + stream_id + '-stream_slides_name').val();


            $('#' + existing + 'stream-' + stream_id + '-camera_src').val(wowzaOriginUrl + '/' + app_name + '/smil:' + stream_camera_name + '.smil/playlist.m3u8');

            $('#' + existing + 'stream-' + stream_id + '-slides_src').val(wowzaOriginUrl + '/' + app_name + '/smil:' + stream_slides_name + '.smil/playlist.m3u8');

            $('#' + existing + 'stream-' + stream_id + '-smil_camera').val(stream_camera_name);
            $('#' + existing + 'stream-' + stream_id + '-smil_slides').val(stream_slides_name);

        }

    });
}

function getBasePath(url) {
    var r = ('' + url).match(/^(https?:)?\/\/[^/]+/i);
    return r ? r[0] : '';
};

function buildWowzaSmilUrl(wowza, appName, smilFile) {
    return wowza + '/' + appName + '/smil:' + smilFile + '.smil/playlist.m3u8';
}

function loadSmilFieldSyncrhonization(streamId, type) {

    // First we get the fields for the source and for the smil file name
    // Also the appName and the wowza base url
    let $smilNameField = $('#existing-stream-' + streamId + '-smil_' + type),
        $sourceField = $('#existing-stream-' + streamId + '-' + type + '_src');
    let appName = $('#existing-stream-' + streamId + '-app_name').val();
    console.debug(appName);
    let wowza = getBasePath($sourceField.val());

    // When the smil field changes, the source field is generated with the content of the field
    $smilNameField.on('input', function () {
        let $smilNameField = $('#existing-stream-' + streamId + '-smil_' + type),
            $sourceField = $('#existing-stream-' + streamId + '-' + type + '_src');
        let appName = $('#existing-stream-' + streamId + '-app_name').val();
        $sourceField.val(buildWowzaSmilUrl(wowza, appName, $smilNameField.val()));
    });

    // When the source field changes, we extract the smil filename from the source and we set the smil name to it
    $sourceField.on('input', function () {
        let sourcePieces = $sourceField.val().split(":");
        if (sourcePieces.length === 3) {
            let smilPieces = sourcePieces[2].split(".smil");
            if (smilPieces.length === 2) {
                $smilNameField.val(smilPieces[0]);
            }
        }
    });
}