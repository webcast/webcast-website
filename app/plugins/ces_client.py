import datetime
import logging
from typing import List

import requests
from flask import current_app

from app.extensions import db
from app.models.events import CDSRecord


class CesClient:
    def __init__(self, logger=None):
        if not logger:
            self.logger = logging.getLogger("webapp.ces")
        else:
            self.logger = logger

    def get_recent_events(self):
        self.fetch_recent_events()

    def fetch_recent_events(self) -> List[CDSRecord]:
        headers = {
            "Authorization": f"Bearer {current_app.config.get('CES_API_TOKEN')}",
            "Content-Type": "application/json",
        }
        self.logger.debug(f"Fetching recent events from {current_app.config.get('CES_API_URL')}")
        response = requests.get(current_app.config.get("CES_API_URL"), headers=headers, timeout=5)

        records = []
        for record in response.json()["results"]:
            indico_id = record["indicoId"]
            title = record["title"]
            cds_record = record["cdsRecord"]
            url = record["url"]
            is_restricted = record["isRestricted"]
            speakers = record["speakers"]
            published_date = record["startDate"]
            published_time = record["startTime"]
            indico_url = record["indicoUrl"]

            event_datetime = datetime.datetime.strptime(f"{published_date} {published_time}", "%Y-%m-%d %H:%M:%S")

            # Find the CdsRecord that matches the cds_record
            cds_record_object = CDSRecord.query.filter_by(cds_record_id=cds_record).first()
            if not cds_record_object:
                cds_record_object = CDSRecord(
                    cds_record_id=cds_record,
                    indico_link=indico_url,
                )
                cds_record_object.indico_id = indico_id
                cds_record_object.title = title
                cds_record_object.url = url
                cds_record_object.is_protected = is_restricted
                cds_record_object.speakers = speakers
                cds_record_object.published_date = event_datetime

                db.session.add(cds_record_object)
                db.session.commit()
                records.append(cds_record_object)

        return records
