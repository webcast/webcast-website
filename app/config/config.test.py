import os
from pathlib import Path

from celery.schedules import crontab

_basedir = os.path.abspath(os.path.dirname(__file__))

# Get project folder with Path
project_dir = Path(__file__).resolve().parents[2]

TESTING = True
WTF_CSRF_ENABLED = False
WTF_CSRF_SECRET_KEY = "<TODO>"  # nosec B105
SECRET_KEY = "<TODO>"  # nosec B105
APPLICATION_ROOT = "/"
CERN_OAUTH_CLIENT_ID = "test"
CERN_OAUTH_CLIENT_SECRET = "test"  # nosec B105
CERN_OAUTH_TOKEN_URL = ""  # nosec B105
CERN_OAUTH_AUTHORIZE_URL = ""

SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(_basedir, "test_app.db")
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_RECORD_QUERIES = False

# Images
IMAGES_FOLDER = "images"
PERMANENT_STREAM_IMAGES_FOLDER = "permanent"
EVENTS_IMAGES_FOLDER = "events"
UPLOADED_IMAGES_DEST = os.path.join(project_dir, "uploads", "images")

UPLOADED_EVENTS_IMAGES_DEST = os.path.join(project_dir, "uploads")

CONF_DIRPATH_JSON = os.path.join(project_dir, "uploads")

APP_PORT = 8080
DEBUG = True
# Available values:
# DEV: Development purposes with DEBUG level
# PROD: Production purposes with INFO level
LOG_LEVEL = "PROD"

# Needed to use Oauth
USE_PROXY = True

IS_LOCAL_INSTALLATION = True
API_URL_PREFIX = "/api"

STATIC_FILES_PATH = os.path.join(project_dir, "app", "static")
DEFAULT_IMAGES_FOLDER = "images/default"
DEFAULT_CATEGORIES_FOLDER = "categories"
DEFAULT_EVENTS_FOLDER = "events"

# available languages
BABEL_DEFAULT_LOCALE = "en"

INDICO_URL = "http://indico.corn"
INDICO_API_KEY = "42"
INDICO_API_SECRET_KEY = "42"  # nosec B105

WOWZA_ORIGIN_URLS = ["https://wowzatest.test.cern", "https://wowzatest.test.cern"]
WOWZA_EDGE_URL = "https://wowzatest.test.cern"
WOWZA_SMIL_FOLDER = os.path.join(project_dir, "tests", "samples", "smil")

CDS_SERVER = "https://cdsweb.cern.ch/record/"
CDS_API_URL = "https://cdsweb.cern.ch/search?p=300__a%3A%22Streaming+video%22+and+collection%3AIndico+and+%28collection%3ATALK+or+collection%3ARestricted_ATLAS_Talks+or+collection%3ARestricted_CMS_Talks%29+AND+8567_x%3Apngthumbnail&f=&action_search=Search&c=CERN+Document+Server&rg=12&sc=0&of=xm&sf=269__c&so=d"  # noqa

#
# CACHE
#
CACHE_ENABLE = False
CACHE_REDIS_PASSWORD = ""  # nosec B105
CACHE_REDIS_HOST = "redis"
CACHE_REDIS_PORT = "6379"
CACHE_OAUTH_TIMEOUT = 300

ANALYTICS_PIWIK_ID = ""

# Video Player
VIDEO_PLAYER_URL = "https://video-player.web.cern.ch"

TEMP_FILES_DEST = os.path.join(project_dir, "tests", "samples")

WOWZA_SECURETOKEN = {"app1": "XXXXXXXXXX", "app2": "YYYYYYYYYY"}

# Sentry
ENABLE_SENTRY = False
SENTRY_DSN = ""
SENTRY_ENVIRONMENT = "test"

#
# Celery
#
if CACHE_REDIS_PASSWORD != "":  # nosec B105
    CELERY_BROKER_URL = f"redis://:{CACHE_REDIS_PASSWORD}@{CACHE_REDIS_HOST}:{CACHE_REDIS_PORT}/0"
    CELERY_RESULT_BACKEND = f"redis://:{CACHE_REDIS_PASSWORD}@{CACHE_REDIS_HOST}:{CACHE_REDIS_PORT}/0"
else:
    CELERY_BROKER_URL = f"redis://{CACHE_REDIS_HOST}:{CACHE_REDIS_PORT}/0"
    CELERY_RESULT_BACKEND = f"redis://{CACHE_REDIS_HOST}:{CACHE_REDIS_PORT}/0"


#
# Gitlab
#
GITLAB_URL = "https://gitlab.cern.ch"
GITLAB_TOKEN = ""  # nosec B105
GITLAB_PROJECT_ID = ""
GITLAB_BRANCH = "master"
#
#
CELERY_BEAT_SCHEDULE = {
    "dummy_task": {
        "task": "app.tasks.example.dummy_task",
        "schedule": crontab(hour="*", minute=1),
    },
}
# Matomo configuration
#
MATOMO_ENABLED = False
MATOMO_SITEID = 24
# Matomo base URL
#
MATOMO_BASE_URL = "https://webanalytics.web.cern.ch"
# Matomo auth token
#
MATOMO_AUTH_TOKEN = ""  # nosec bandit B105
#
#
CES_API_TOKEN = ""  # nosec bandit B105
CES_API_URL = "https://ces.cern.ch/api/v1/info/last processed/"
