import logging
from functools import wraps

from flask import current_app, flash, redirect, request, url_for
from flask_login import current_user
from oauthlib.oauth2 import TokenExpiredError

logger = logging.getLogger("webapp.decorators")


def requires_login(f):
    """
    Add this decorator to the views that require a logged in user.

    :param f: Function that will be wrapped with this decorator.
    :type f: function
    :return: The decorated function
    :rtype: function
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        if current_user.is_authenticated or current_app.config["TESTING"]:
            return f(*args, **kwargs)
        return redirect(url_for("login_view.login", next=request.url))

    return decorated_function


def admin_required(f):
    """
    Add this decorator to the views that require a logged in Admin user.

    :param f: Function that will be wrapped with this decorator.
    :return: The decorated function
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        # First check if the user is authenticated to try to give him
        # admin rights
        # logger.debug(f"Checking if user {current_user} is admin")
        try:
            if current_app.config["TESTING"] or current_user.is_admin:
                # logger.debug(f"User {current_user} is admin")
                return f(*args, **kwargs)
            else:
                flash("You are not authorized to access this page", "danger")
                raise AttributeError(f"User {current_user} is not admin")
        except (
            TokenExpiredError,
            AttributeError,
        ):
            return redirect(url_for("login_view.login", next=request.url))

    return decorated_function
