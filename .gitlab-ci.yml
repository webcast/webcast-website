include:
  - project: "ci-tools/container-image-ci-templates"
    file: "kaniko-image.gitlab-ci.yml"
    ref: master
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml

stages:
  - install_dependencies
  - test
  - build_frontend
  - build_container
  - redeploy

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PRE_COMMIT_HOME: .cache/pre-commit
  POETRY_HOME: "/opt/poetry"
  POETRY_VERSION: "1.7.1"
  PRE_COMMIT_VERSION: "3.0.0"

.cache_python_template:
  image: python:3.9
  cache:
    - key:
        files:
          - poetry.lock
      paths:
        - .cache/pip
        - .venv

.cache_node_template:
  image: node:20
  cache:
    - key:
        files:
          - package-lock.json
      paths:
        - app/client/.npm/
        - app/client/node_modules

.build_frontend_template:
  extends: .cache_node_template
  stage: build_frontend
  before_script:
    - cd app/client
  script:
    - npm run build
  artifacts:
    paths:
      - app/client/dist # The application is built and packaged

.redeploy_paas_template:
  stage: redeploy
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:latest
  variables:
    REGISTRY_APP_NAME: webcast-website
    OKD4_SERVER: https://api.paas.okd.cern.ch # use https://openshift-dev.cern.ch for a Test site
    OKD4_APP_NAME: webapp # this is the name of the ImageStream/DeploymentConfig objects created by oc new-app. Typically, this will be the name of the GitLab project.
  script:
    - "echo 'Deploying the application in Openshift: ImageStream: $OKD4_APP_NAME. Project: $OKD4_NAMESPACE. Image: $IMAGE. Server: $OKD4_SERVER. Registry: $REGISTRY_APP_NAME.'"
    - oc login $OKD4_SERVER --token=$OKD4_TOKEN
    - oc project $OKD4_NAMESPACE
    - oc import-image $OKD4_APP_NAME --all --from=registry.cern.ch/$REGISTRY_APP_NAME/$IMAGE --confirm
    - sleep 10 && oc rollout status deploy/$OKD4_APP_NAME

install_pip:
  extends: .cache_python_template
  stage: install_dependencies
  script:
    - python -V
    - pip install poetry==$POETRY_VERSION
    - poetry --version
    - poetry install

install_npm:
  extends: .cache_node_template
  stage: install_dependencies
  script:
    - cd app/client
    - npm ci --cache .npm --prefer-offline

pre_commit:
  image: python:3.9
  # Will run the tests with coverage.
  stage: test
  before_script:
    - pip install pre-commit==$PRE_COMMIT_VERSION
    - cp app/config/config.sample.py app/config/config.py
  script:
    - pre-commit run --all-files
  cache:
    - key:
        files:
          - .pre-commit-config.yaml
      paths:
        - .cache/pre-commit

test_backend:
  extends: .cache_python_template
  stage: test
  before_script:
    - source .venv/bin/activate
  script:
    - cp app/config/config.sample.py app/config/config.py
    - pytest
  needs:
    - install_pip

lint_frontend:
  extends: .cache_node_template
  stage: test
  before_script:
    - cd app/client
  script:
    - npm run lint
  needs:
    - install_npm

test_frontend:
  extends: .cache_node_template
  stage: test
  before_script:
    - cd app/client
  script:
    - npm run coverage
  needs:
    - install_npm

build_frontend_test:
  extends: .build_frontend_template
  variables:
    VITE_API_ENDPOINT: ${API_ENDPOINT_TEST}
    VITE_IS_DEV_INSTALL: "true"
    VITE_ENVIRONMENT: "DEV"
    VITE_PIWIK_ID: "3722"
    VITE_SENTRY_DSN: ${SENTRY_DSN}
    VITE_SENTRY_ENV: "test"
    VITE_CLIENT_ID: ${CLIENT_ID_TEST}
    VITE_WEBCAST_LITE_BASE_URL: "https://webcast-lite-test.web.cern.ch"
  only:
    - test

build_frontend_qa:
  extends: .build_frontend_template
  variables:
    VITE_API_ENDPOINT: ${API_ENDPOINT_QA}
    VITE_IS_DEV_INSTALL: "true"
    VITE_ENVIRONMENT: "QA"
    VITE_PIWIK_ID: "24"
    VITE_SENTRY_DSN: ${SENTRY_DSN}
    VITE_SENTRY_ENV: "qa"
    VITE_CLIENT_ID: ${CLIENT_ID_QA}
    VITE_WEBCAST_LITE_BASE_URL: "https://webcast-lite-qa.web.cern.ch"
  only:
    - qa

build_frontend_master:
  extends: .build_frontend_template
  variables:
    VITE_API_ENDPOINT: ${API_ENDPOINT_MASTER}
    VITE_IS_DEV_INSTALL: "false"
    VITE_ENVIRONMENT: "PROD"
    VITE_PIWIK_ID: "23"
    VITE_SENTRY_DSN: ${SENTRY_DSN}
    VITE_SENTRY_ENV: "production"
    VITE_CLIENT_ID: ${CLIENT_ID_PROD}
    VITE_WEBCAST_LITE_BASE_URL: "https://webcast-lite.web.cern.ch"
  only:
    - master

build_container:
  only:
    - qa
    - master
    - test
  stage: build_container
  extends: .build_kaniko
  variables:
    PUSH_IMAGE: "true"
    REGISTRY_IMAGE_PATH: "registry.cern.ch/webcast-website/$CI_COMMIT_REF_NAME:$CI_COMMIT_TAG"

redeploy_test:
  extends: .redeploy_paas_template
  environment:
    name: test
  variables:
    OKD4_NAMESPACE: webcast-test # this is the name of your Openshift project (i.e. the site name)
    OKD4_TOKEN: $IMAGE_IMPORT_TOKEN_TEST
    IMAGE: test
  only:
    - test # master, qa or test, the branch you want to publish

redeploy_qa:
  extends: .redeploy_paas_template
  environment:
    name: qa
  variables:
    OKD4_NAMESPACE: webcast-qa
    OKD4_TOKEN: $IMAGE_IMPORT_TOKEN_QA
    IMAGE: qa
  only:
    - qa

redeploy_production:
  extends: .redeploy_paas_template
  environment:
    name: production
  variables:
    OKD4_NAMESPACE: webcast
    OKD4_TOKEN: $IMAGE_IMPORT_TOKEN_MASTER
    IMAGE: master
  only:
    - master
