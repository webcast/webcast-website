"""Initial migration

Revision ID: c754e23dcb08
Revises: 
Create Date: 2023-12-19 13:32:21.798448

"""
import sqlalchemy as sa
import sqlalchemy_utils
from alembic import op

from app.models.api import API_RESOURCES
from app.models.events import AVAILABLE_TIMEZONES
from app.models.streams import LIVESTREAM_TYPES

# revision identifiers, used by Alembic.
revision = "c754e23dcb08"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "api_keys",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("access_key", sa.String(length=255), nullable=False),
        sa.Column("secret_key", sa.String(length=255), nullable=False),
        sa.Column("name", sa.String(length=50), nullable=False),
        sa.Column("description", sa.Text(), nullable=False),
        sa.Column("status", sa.Boolean(), nullable=False),
        sa.Column(
            "resources",
            sqlalchemy_utils.types.choice.ChoiceType(API_RESOURCES),
            nullable=True,
        ),
        sa.Column("created", sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "audiences",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(length=255), nullable=False),
        sa.Column("default_application", sa.String(length=255), nullable=True),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "authorized_users",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.Text(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "categories",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(length=255), nullable=True),
        sa.Column("default_img", sa.String(length=255), nullable=True),
        sa.Column("custom_img", sa.String(length=255), nullable=True),
        sa.Column("use_default_img", sa.Boolean(), nullable=False),
        sa.Column("priority", sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "egroup",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(length=255), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "settings",
        sa.Column("id", sa.String(length=255), nullable=False),
        sa.Column("value", sa.Text(), nullable=False),
        sa.Column("lastUpdate", sa.DateTime(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "user",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("person_id", sa.String(length=255), nullable=False),
        sa.Column("first_name", sa.String(length=255), nullable=False),
        sa.Column("username", sa.String(length=255), nullable=False),
        sa.Column("last_name", sa.String(length=255), nullable=False),
        sa.Column("email", sa.String(length=255), nullable=False),
        sa.Column("identity", sa.String(length=255), nullable=True),
        sa.Column("is_admin", sa.Boolean(), nullable=False),
        sa.Column("active", sa.Boolean(), nullable=True),
        sa.Column("confirmed_at", sa.DateTime(), nullable=True),
        sa.CheckConstraint("email = lower(email)", name="lowercase_email"),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("email"),
    )
    op.create_index(op.f("ix_user_person_id"), "user", ["person_id"], unique=False)
    op.create_table(
        "audiences_authorized_users",
        sa.Column("audience_id", sa.Integer(), nullable=False),
        sa.Column("authorized_user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["audience_id"],
            ["audiences.id"],
        ),
        sa.ForeignKeyConstraint(
            ["authorized_user_id"],
            ["authorized_users.id"],
        ),
        sa.PrimaryKeyConstraint("audience_id", "authorized_user_id"),
    )
    op.create_table(
        "cds_records",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("cds_record_id", sa.Integer(), nullable=True),
        sa.Column("indico_id", sa.String(length=50), nullable=False),
        sa.Column("published_date", sa.DateTime(), nullable=True),
        sa.Column("indico_link", sa.Text(), nullable=True),
        sa.Column("video_file", sa.Text(), nullable=True),
        sa.Column("mobile_file", sa.Text(), nullable=True),
        sa.Column("image_url", sa.Text(), nullable=True),
        sa.Column("speakers", sa.Text(), nullable=True),
        sa.Column("featured", sa.Boolean(), nullable=True),
        sa.Column("is_synchronized", sa.Boolean(), nullable=True),
        sa.Column("room", sa.String(length=255), nullable=True),
        sa.Column("title", sa.String(length=255), nullable=True),
        sa.Column("is_protected", sa.Boolean(), nullable=False),
        sa.Column("audience_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["audience_id"],
            ["audiences.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "egroup_user",
        sa.Column("egroup_id", sa.Integer(), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["egroup_id"],
            ["egroup.id"],
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user.id"],
        ),
    )
    op.create_table(
        "flask_dance_oauth",
        sa.Column("user_id", sa.Integer(), nullable=True),
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("provider", sa.String(length=50), nullable=True),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("token", sqlalchemy_utils.types.json.JSONType(), nullable=True),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "streams",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("title", sa.String(length=255), nullable=False),
        sa.Column("default_img", sa.String(length=255), nullable=True),
        sa.Column("custom_img", sa.String(length=255), nullable=True),
        sa.Column("use_default_img", sa.Boolean(), nullable=False),
        sa.Column("is_embeddable", sa.Boolean(), nullable=False),
        sa.Column("is_sharable", sa.Boolean(), nullable=False),
        sa.Column("is_visible", sa.Boolean(), nullable=False),
        sa.Column("is_protected", sa.Boolean(), nullable=False),
        sa.Column("player_src", sa.String(length=255), nullable=True),
        sa.Column("player_width", sa.Integer(), nullable=False),
        sa.Column("player_height", sa.Integer(), nullable=False),
        sa.Column("audience_id", sa.Integer(), nullable=True),
        sa.Column("category_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["audience_id"],
            ["audiences.id"],
        ),
        sa.ForeignKeyConstraint(
            ["category_id"],
            ["categories.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "events",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("title", sa.Text(), nullable=False),
        sa.Column("speakers", sa.Text(), nullable=True),
        sa.Column("abstract", sa.Text(), nullable=True),
        sa.Column("room", sa.String(length=100), nullable=True),
        sa.Column("link", sa.Text(), nullable=True),
        sa.Column("link_label", sa.Text(), nullable=True),
        sa.Column("default_img", sa.String(length=255), nullable=True),
        sa.Column("custom_img", sa.String(length=255), nullable=True),
        sa.Column("use_default_img", sa.Boolean(), nullable=False),
        sa.Column("extra_html", sa.Text(), nullable=True),
        sa.Column(
            "status",
            sa.Enum("UPCOMING", "LIVE", "RECENT", "ARCHIVED", name="eventstatus"),
            nullable=True,
        ),
        sa.Column("indico_id", sa.String(length=50), nullable=True),
        sa.Column("indico_category_id", sa.String(length=50), nullable=True),
        sa.Column("display_message", sa.Text(), nullable=True),
        sa.Column("is_synchronized", sa.Boolean(), nullable=False),
        sa.Column("is_embeddable", sa.Boolean(), nullable=False),
        sa.Column("is_sharable", sa.Boolean(), nullable=False),
        sa.Column("is_visible", sa.Boolean(), nullable=False),
        sa.Column("is_follow_up", sa.Boolean(), nullable=False),
        sa.Column("is_protected", sa.Boolean(), nullable=False),
        sa.Column("random_string", sa.String(length=255), nullable=True),
        sa.Column("audience_id", sa.Integer(), nullable=False),
        sa.Column("cds_record_id", sa.Integer(), nullable=True),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("start_date", sa.DateTime(), nullable=False),
        sa.Column("end_date", sa.DateTime(), nullable=False),
        sa.Column(
            "timezone",
            sqlalchemy_utils.types.choice.ChoiceType(AVAILABLE_TIMEZONES),
            nullable=False,
        ),
        sa.Column("timestamp_started", sa.TIMESTAMP(), nullable=True),
        sa.Column("timestamp_ended", sa.TIMESTAMP(), nullable=True),
        sa.ForeignKeyConstraint(
            ["audience_id"],
            ["audiences.id"],
        ),
        sa.ForeignKeyConstraint(
            ["cds_record_id"],
            ["cds_records.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "streams_authorized_users",
        sa.Column("stream_id", sa.Integer(), nullable=False),
        sa.Column("authorized_user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["authorized_user_id"],
            ["authorized_users.id"],
        ),
        sa.ForeignKeyConstraint(
            ["stream_id"],
            ["streams.id"],
        ),
        sa.PrimaryKeyConstraint("stream_id", "authorized_user_id"),
    )
    op.create_table(
        "events_authorized_users",
        sa.Column("event_id", sa.Integer(), nullable=False),
        sa.Column("authorized_user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["authorized_user_id"],
            ["authorized_users.id"],
        ),
        sa.ForeignKeyConstraint(
            ["event_id"],
            ["events.id"],
        ),
        sa.PrimaryKeyConstraint("event_id", "authorized_user_id"),
    )
    op.create_table(
        "follow_ups",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("app_name", sa.String(length=255), nullable=True),
        sa.Column("origin_server", sa.String(length=255), nullable=True),
        sa.Column("stream_name", sa.String(length=255), nullable=True),
        sa.Column("stream_type", sa.String(length=255), nullable=True),
        sa.Column("on_air", sa.Boolean(), nullable=False),
        sa.Column("date", sa.DateTime(), nullable=True),
        sa.Column("event_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["event_id"],
            ["events.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "live_streams",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column(
            "type",
            sqlalchemy_utils.types.choice.ChoiceType(LIVESTREAM_TYPES),
            nullable=True,
        ),
        sa.Column("is_enabled", sa.Boolean(), nullable=False),
        sa.Column("title", sa.String(length=255), nullable=True),
        sa.Column("camera_src", sa.String(length=255), nullable=True),
        sa.Column("slides_src", sa.String(length=255), nullable=True),
        sa.Column(
            "room_video_quality",
            sa.Enum("HD", "SD", "OTHER", name="videoquality"),
            nullable=True,
        ),
        sa.Column("app_name", sa.String(length=100), nullable=True),
        sa.Column("stream_camera_name", sa.String(length=100), nullable=True),
        sa.Column("smil_camera", sa.String(length=100), nullable=True),
        sa.Column("smil_slides", sa.String(length=100), nullable=True),
        sa.Column("stream_slides_name", sa.String(length=100), nullable=True),
        sa.Column("smil_camera_mobile", sa.String(length=100), nullable=True),
        sa.Column("smil_slides_mobile", sa.String(length=100), nullable=True),
        sa.Column("player_version", sa.String(length=45), nullable=True),
        sa.Column("embed_code", sa.Text(), nullable=True),
        sa.Column("is_360", sa.Boolean(), nullable=True),
        sa.Column("use_smil", sa.Boolean(), nullable=False),
        sa.Column("camera_smil_fetched", sa.Boolean(), nullable=False),
        sa.Column("slides_smil_fetched", sa.Boolean(), nullable=False),
        sa.Column("event_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["event_id"],
            ["events.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("live_streams")
    op.drop_table("follow_ups")
    op.drop_table("events_authorized_users")
    op.drop_table("streams_authorized_users")
    op.drop_table("events")
    op.drop_table("streams")
    op.drop_table("flask_dance_oauth")
    op.drop_table("egroup_user")
    op.drop_table("cds_records")
    op.drop_table("audiences_authorized_users")
    op.drop_index(op.f("ix_user_person_id"), table_name="user")
    op.drop_table("user")
    op.drop_table("settings")
    op.drop_table("egroup")
    op.drop_table("categories")
    op.drop_table("authorized_users")
    op.drop_table("audiences")
    op.drop_table("api_keys")
    # ### end Alembic commands ###
