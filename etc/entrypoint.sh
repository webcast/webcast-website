#!/bin/bash

echo 'Starting app...'
# We run the application using uwsgi
uwsgi --ini /opt/app-root/src/server/uwsgi.ini --mimefile /opt/app-root/src/etc/mime.types
