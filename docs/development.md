# Development

## Requirements

- Python 3.9: Python can be installed using `pyenv` (On Mac: `brew install pyenv` and follow the screen steps).
- Node v17.5.0: Node can be installed using `nvm` (On Mac: `brew install nvm` and follow the screen steps).

## Local install

### Database

You can install a database of your choice in your computer.

- MySQL: https://dev.mysql.com/downloads/mysql/


### Create the application portal applications

URL: [https://application-portal.web.cern.ch/](https://application-portal.web.cern.ch/)

It is required one for the frontend and one for the backend:

1. **Frontend**: Check "The application can't store credentials securely". Redirect URL must be something like: `http://localhost:3000/*`. You will need the `client_id`.
2. **Backend**: Redirect URL must be something like: `http://localhost:5000/*`.
3. Create an `admins` role and assign to it a group you are in (To have access to the admin dashboard). This role will need to be set on the `ADMIN_ROLE` variable.


### Backend

Create a virtual environment:

```
python -m venv .venv
```

Activate the virtual environment:

```
source ./venv/bin/activate
```

Install the dependencies:

```
pip install -r requirements.txt
pip install -r requirements.dev.txt
```

Create the database tables for the application:

```
flask db upgrade
```

Copy the values in `app/config/config.sample.py` to `app/config/config.sample.py` and populate the correct values.

Once all the settings are set, you must initialize some application data: audiences, authorized users and permanent streams:

```
FLASK_APP=wsgi.py flask initialize_data
```

Finally, you can run the backend using the Visual Studio "Start Python: Flask" action or using the command line manually: `flask run`.

The application will be available on: `http://localhost:5000`

### Frontend

Install the dependencies:

```
cd app/client
npm install
```

Start the application:

```
npm start
```

The application will be available on: `http://localhost:3000`