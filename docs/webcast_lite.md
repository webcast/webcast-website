# Webcast Lite Service

This service uses Jinja to generate a webcast configuration folder for a given event. The folder contains a `config.json` file that contains the event's configuration and an optional `.htaccess` file that restricts access to the event folder.

## CLI

The `webcast-lite` CLI allows you to generate a webcast configuration folder for a given event.

### Usage

You can use it as an interactive CLI by running the following command:

```bash
flask webcast_lite generate-config
```

---

# Gitlab service

The `GitlabService` class encapsulates methods to interact with the GitLab API to perform various actions related to repository commits. It allows you to create new commits and commit files using a simplified interface.

## Usage

### Creating a GitlabService Instance

Before using the `GitlabService` class, you need to create an instance by providing your GitLab URL, access token, and project ID:

```python
from typing import Any, Dict, List
import requests

class GitlabService:
    def __init__(self, gitlab_url: str, token: str, project_id: str):
        # ...

# Create an instance of GitlabService
gitlab_url = "https://gitlab.com"
token = "your_access_token"
project_id = "your_project_id"
gitlab_service = GitlabService(gitlab_url, token, project_id)
```

Pay attention to the `gitlab_url` parameter. If you are using GitLab.com, you can use the default value of `https://gitlab.com`. If you are using a self-hosted GitLab instance, you need to provide the URL of your GitLab instance.

```
Make sure to replace placeholders like `"your_access_token"` and `"your_project_id"` with your actual GitLab access token and project ID.
```

### Creating a Commit

You can create a new commit using the `create_commit` method:

```python
commit_message = "Add new feature"
actions = [
    gitlab_service.create_commit_action(
        file_path="path/to/new_file.txt",
        content="This is the content of the new file.",
    ),
]
response = gitlab_service.create_commit(
    commit_message=commit_message,
    actions=actions,
    branch="main",
)
print(response)
```

### Creating a Commit File

To create a commit that adds a new file to the repository, you can use the `create_commit_file` method:

```python
file_path = "path/to/new_file.txt"
content = "This is the content of the new file."
commit_message = "Add a new file"
response = gitlab_service.create_commit_file(
    file_path=file_path,
    content=content,
    commit_message=commit_message,
    branch="main",
)
print(response)
```

---

# Webcast Lite Service

The Webcast Lite Service module is designed to facilitate the creation of event folders within a GitLab repository. These event folders contain configuration files for webcasting events. The module offers a single main function, `generate_config_folder`, that allows you to create event folders, add configuration files, and optionally include `.htaccess` files for access control.

## Function: generate_config_folder

The `generate_config_folder` function creates a new event folder in the GitLab repository and adds a configuration file along with an optional `.htaccess` file.

### Parameters

- `event_id` (int): The ID of the event to create.
- `default_streams` (str): The default stream URLs for the event.
- `default_camera_url` (str): The default camera URL for the event.
- `default_slides_url` (str): The default slides URL for the event.
- `default_title` (str): The default title for the event.
- `role_identifier` (str | None, optional): The role identifier for the event. Defaults to `None`.

### Usage

```python
from webcast_lite_service import generate_config_folder

event_id = 123
default_streams = "stream_url1,stream_url2"
default_camera_url = "camera_url"
default_slides_url = "slides_url"
default_title = "Webcast Event Title"
role_identifier = "admin"  # Optional

generate_config_folder(
    event_id=event_id,
    default_streams=default_streams,
    default_camera_url=default_camera_url,
    default_slides_url=default_slides_url,
    default_title=default_title,
    role_identifier=role_identifier,
)
```
