import datetime

from dateutil.tz import tz

from app.extensions import db
from app.models.audiences import Audience
from app.models.events import Event, EventStatus
from app.models.streams import LiveStream, VideoQuality
from tests import BaseTestCase


class StreamTest(BaseTestCase):
    def test_add_live_stream(self):
        new_live_stream = LiveStream()
        db.session.add(new_live_stream)
        db.session.commit()

        live_streams = LiveStream.query.all()
        self.assertEqual(len(live_streams), 1)

    def test_initial_values(self):
        new_live_stream = LiveStream()
        db.session.add(new_live_stream)
        db.session.commit()

        live_stream = LiveStream.query.get(1)
        self.assertEqual(live_stream.type, "camera_slides")
        self.assertEqual(live_stream.stream_camera_name, None)
        self.assertEqual(live_stream.stream_slides_name, None)
        self.assertEqual(live_stream.use_smil, True)
        self.assertEqual(live_stream.camera_smil_fetched, False)
        self.assertEqual(live_stream.slides_smil_fetched, False)
        self.assertEqual(live_stream.smil_camera, None)
        self.assertEqual(live_stream.smil_slides, None)
        self.assertEqual(live_stream.camera_src, None)
        self.assertEqual(live_stream.slides_src, None)
        self.assertEqual(live_stream.room_video_quality, VideoQuality.HD)
        self.assertEqual(live_stream.app_name, "livehd")

    def test_set_default_values(self):

        new_audience = Audience(name="atlas public", default_application="AtlasLive")
        db.session.add(new_audience)
        db.session.commit()

        to_zone = tz.gettz("Europe/Zurich")
        start_date = datetime.datetime.now(to_zone)
        indico_id = "1234"
        link = "https://cern.ch"
        meeting_name = "This is the title of the Event"

        event = Event(
            timezone="Europe/Zurich",
            indico_id=indico_id,
            title=meeting_name,
            link=link,
            audience=Audience.query.get(1),
            status=EventStatus.UPCOMING,
        )

        event.start_date = start_date
        event.end_date = start_date

        db.session.add(event)
        db.session.commit()

        events = Event.query.all()
        self.assertEqual(len(events), 1)

        live_stream = LiveStream()
        live_stream.set_default_values(event)
        event.live_stream = live_stream

        db.session.commit()

        # By default, the LiveStream will be HD
        live_stream = LiveStream.query.get(1)
        self.assertEqual(live_stream.type, "camera_slides")
        self.assertEqual(live_stream.stream_camera_name, "1234_camera")
        self.assertEqual(live_stream.stream_slides_name, "1234_slides")
        self.assertEqual(live_stream.use_smil, True)
        self.assertEqual(live_stream.camera_smil_fetched, False)
        self.assertEqual(live_stream.slides_smil_fetched, False)
        self.assertEqual(live_stream.smil_camera, "1234_camera_all")
        self.assertEqual(live_stream.smil_slides, "1234_slides_all")
        self.assertEqual(
            live_stream.camera_src,
            "https://wowzatest.test.cern/livehd/smil:1234_camera_all.smil/playlist.m3u8",
        )
        self.assertEqual(
            live_stream.slides_src,
            "https://wowzatest.test.cern/livehd/smil:1234_slides_all.smil/playlist.m3u8",
        )

    def test_set_default_values_SD(self):
        new_audience = Audience(name="atlas public", default_application="AtlasLive")
        db.session.add(new_audience)
        db.session.commit()

        to_zone = tz.gettz("Europe/Zurich")
        start_date = datetime.datetime.now(to_zone)
        indico_id = "1234"
        link = "https://cern.ch"
        meeting_name = "This is the title of the Event"

        event = Event(
            timezone="Europe/Zurich",
            indico_id=indico_id,
            title=meeting_name,
            link=link,
            audience=Audience.query.get(1),
            status=EventStatus.UPCOMING,
        )

        event.start_date = start_date
        event.end_date = start_date

        db.session.add(event)
        db.session.commit()

        events = Event.query.all()
        self.assertEqual(len(events), 1)

        live_stream = LiveStream()
        live_stream.room_video_quality = "OTHER"
        live_stream.app_name = "liveoutside"
        live_stream.set_default_values(event)
        event.live_stream = live_stream

        db.session.commit()

        # By default, the LiveStream will be HD
        live_stream = LiveStream.query.get(1)
        self.assertEqual(live_stream.type, "camera_slides")
        self.assertEqual(live_stream.stream_camera_name, "1234")
        self.assertEqual(live_stream.stream_slides_name, "1234")
        self.assertEqual(live_stream.use_smil, True)
        self.assertEqual(live_stream.camera_smil_fetched, False)
        self.assertEqual(live_stream.slides_smil_fetched, False)
        self.assertEqual(live_stream.smil_camera, "1234")
        self.assertEqual(live_stream.smil_slides, "1234")
        self.assertEqual(
            live_stream.camera_src,
            "https://wowzatest.test.cern/liveoutside/smil:1234.smil/playlist.m3u8",
        )
        self.assertEqual(
            live_stream.slides_src,
            "https://wowzatest.test.cern/liveoutside/smil:1234.smil/playlist.m3u8",
        )
