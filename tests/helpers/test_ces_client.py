import os

from httmock import HTTMock, response, urlmatch
from pathlib2 import Path

from app.models.events import CDSRecord
from app.plugins.ces_client import CesClient
from tests import BaseTestCase


# http://indico.corn/export/event/622398.json
@urlmatch(netloc=r"(.*\.)?ces\.cern\.ch(.*)")
def ces_recent_request(url, request):
    headers = {"content-type": "application/json"}
    project_dir = Path(__file__).resolve().parents[2]
    contents = Path(os.path.join(project_dir, "tests", "samples", "ces_cds_events.json")).read_text()

    return response(200, contents, headers, None, 5, request)


class CesClientTest(BaseTestCase):
    def test_fetch_recent_events(self):
        with HTTMock(ces_recent_request):

            result = CesClient().fetch_recent_events()
            self.assertNotEqual(result, "")

            self.assertEqual(len(result), 10)

    def test_get_recent_events(self):
        with HTTMock(ces_recent_request):

            CesClient().get_recent_events()
            cds_records = CDSRecord.query.all()
            self.assertEqual(len(cds_records), 10)

    # def test_get_recent_events(self):
    #     with HTTMock(cds_recent_request):
    #
    #         CdsClient.get_recent_events()
    #         cds_records = CDSRecord.query.all()
    #         self.assertEqual(len(cds_records), 12)
