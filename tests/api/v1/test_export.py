import json
import time

from flask import url_for

from app.api.v1.utils import generate_hmac_signature, is_signature_valid
from app.models.api import ApiKey
from tests import BaseTestCase
from tests.api.v1 import add_api_key, add_audience_and_authorized_users, add_live_event


class ExportTest(BaseTestCase):
    def test_no_api_key(self):
        response = self.client.get("/api/v1/export/")
        self.assertEqual(response.status_code, 400)

        expected_response = {"message": {"ak": "No Api Key provided"}}

        self.assertEqual(json.loads(response.data.decode("utf-8")), expected_response)

    def test_with_api_key_no_events(self):
        # Create an event and an Api Key
        add_audience_and_authorized_users(self.client)
        add_live_event(self.client)
        add_api_key(self.client, resources="export")

        api_object = ApiKey.query.get(1)

        timestamp = int(time.time())

        query_args = (
            ("ak", api_object.access_key),
            ("api", "export"),
            ("from", "today"),
            ("only_public", "true"),
            ("timestamp", timestamp),
            ("to", "tomorrow"),
        )

        signature = generate_hmac_signature(path="/api/v1/export/", params=query_args, secret_key=api_object.secret_key)

        signature_valid, message = is_signature_valid(
            api_object.access_key,
            signature,
            timestamp,
            args=query_args,
            api_path="/api/v1/export/",
        )

        self.assertTrue(signature_valid, message)
        self.assertEqual(message, "valid")

        api_url = url_for(
            "api_v1.exportapi",
            **{
                "ak": api_object.access_key,
                "api": "export",
                "timestamp": timestamp,
                "signature": signature,
                "from": "today",
                "to": "tomorrow",
                "only_public": "true",
            }
        )

        response = self.client.get(api_url)
        self.assertEqual(response.status_code, 200)

        expected_response = {"count": 0, "results": []}

        self.assertEqual(json.loads(response.data.decode("utf-8")), expected_response)

    def test_with_api_key_events(self):
        # Create an event and an Api Key
        add_audience_and_authorized_users(self.client)
        add_live_event(self.client)
        add_api_key(self.client, resources="export")

        api_object = ApiKey.query.get(1)

        timestamp = int(time.time())

        query_args = (
            ("ak", api_object.access_key),
            ("api", "export"),
            ("timestamp", timestamp),
            ("from", "2017-3-14"),
            ("to", "2017-3-15"),
            ("only_public", "false"),
        )

        signature = generate_hmac_signature(path="/api/v1/export/", params=query_args, secret_key=api_object.secret_key)

        signature_valid, message = is_signature_valid(
            api_object.access_key,
            signature,
            timestamp,
            args=query_args,
            api_path="/api/v1/export/",
        )

        self.assertTrue(signature_valid, message)
        self.assertEqual(message, "valid")

        api_url = url_for(
            "api_v1.exportapi",
            **{
                "ak": api_object.access_key,
                "api": "export",
                "timestamp": timestamp,
                "signature": signature,
                "from": "2017-3-14",
                "to": "2017-3-15",
                "only_public": "false",
            }
        )

        response = self.client.get(api_url)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(json.loads(response.data.decode("utf-8"))["count"], 1)
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["webcast_link"],
            "http://localhost/event/i1234",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["event_link"],
            "https://webcast.web.cern.ch",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["ical_link"],
            "http://localhost/utils/ical/1/",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["timezone"],
            "Europe/Zurich",
        )
        self.assertEqual(json.loads(response.data.decode("utf-8"))["results"][0]["restricted"], True)
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["speakers"],
            "Speaker Name Lastname",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["embed_link_slides"],
            "",
        )
        self.assertEqual(json.loads(response.data.decode("utf-8"))["results"][0]["indico_id"], "1234")
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["embed_link_camera"],
            "",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["abstract"],
            "This is the abstract of the Event",
        )
        self.assertEqual(json.loads(response.data.decode("utf-8"))["results"][0]["type"], "Live")
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["end_date"],
            "Tue, 14 Mar 2017 15:30:00 GMT",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["indico_category"],
            "987",
        )
        self.assertEqual(json.loads(response.data.decode("utf-8"))["results"][0]["id"], 1)
        self.assertEqual(json.loads(response.data.decode("utf-8"))["results"][0]["title"], "")
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["start_date"],
            "Tue, 14 Mar 2017 14:30:00 GMT",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["room_name"],
            "28-1-007",
        )

    def test_since_lots_of_months(self):
        # Create an event and an Api Key
        add_audience_and_authorized_users(self.client)
        add_live_event(self.client)
        add_api_key(self.client, resources="export")

        api_object = ApiKey.query.get(1)

        timestamp = int(time.time())

        query_args = (
            ("ak", api_object.access_key),
            ("api", "export"),
            ("timestamp", timestamp),
            ("from", "-10000m"),
            ("to", "+10000m"),
            ("only_public", "false"),
        )

        signature = generate_hmac_signature(path="/api/v1/export/", params=query_args, secret_key=api_object.secret_key)

        api_url = url_for(
            "api_v1.exportapi",
            **{
                "ak": api_object.access_key,
                "api": "export",
                "timestamp": timestamp,
                "signature": signature,
                "from": "-10000m",
                "to": "+10000m",
                "only_public": "false",
            }
        )

        response = self.client.get(api_url)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(json.loads(response.data.decode("utf-8"))["count"], 1)
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["webcast_link"],
            "http://localhost/event/i1234",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["event_link"],
            "https://webcast.web.cern.ch",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["ical_link"],
            "http://localhost/utils/ical/1/",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["timezone"],
            "Europe/Zurich",
        )
        self.assertEqual(json.loads(response.data.decode("utf-8"))["results"][0]["restricted"], True)
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["speakers"],
            "Speaker Name Lastname",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["embed_link_slides"],
            "",
        )
        self.assertEqual(json.loads(response.data.decode("utf-8"))["results"][0]["indico_id"], "1234")
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["embed_link_camera"],
            "",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["abstract"],
            "This is the abstract of the Event",
        )
        self.assertEqual(json.loads(response.data.decode("utf-8"))["results"][0]["type"], "Live")
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["end_date"],
            "Tue, 14 Mar 2017 15:30:00 GMT",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["indico_category"],
            "987",
        )
        self.assertEqual(json.loads(response.data.decode("utf-8"))["results"][0]["id"], 1)
        self.assertEqual(json.loads(response.data.decode("utf-8"))["results"][0]["title"], "")
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["start_date"],
            "Tue, 14 Mar 2017 14:30:00 GMT",
        )
        self.assertEqual(
            json.loads(response.data.decode("utf-8"))["results"][0]["room_name"],
            "28-1-007",
        )

    def test_expect_exception(self):
        # Create an event and an Api Key
        add_audience_and_authorized_users(self.client)
        add_live_event(self.client)
        add_api_key(self.client, resources="export")

        api_object = ApiKey.query.get(1)

        timestamp = int(time.time())

        query_args = (
            ("ak", api_object.access_key),
            ("api", "export"),
            ("timestamp", timestamp),
            ("from", "2017-3-145"),
            ("to", "2017-3-15"),
            ("only_public", "false"),
        )

        signature = generate_hmac_signature(path="/api/v1/export/", params=query_args, secret_key=api_object.secret_key)

        api_url = url_for(
            "api_v1.exportapi",
            **{
                "ak": api_object.access_key,
                "api": "export",
                "timestamp": timestamp,
                "signature": signature,
                "from": "2017-3-145",
                "to": "2017-3-15",
                "only_public": "false",
            }
        )

        response = self.client.get(api_url)
        self.assertEqual(response.status_code, 403)

        expected_response = {
            "valid": False,
            "message": "Error using API: Unknown string format: 2017-3-145",
        }

        self.assertEqual(json.loads(response.data.decode("utf-8")), expected_response)

    def test_expect_another_exception(self):
        # Create an event and an Api Key
        add_audience_and_authorized_users(self.client)
        add_live_event(self.client)
        add_api_key(self.client, resources="export")

        api_object = ApiKey.query.get(1)

        timestamp = int(time.time())

        query_args = (
            ("ak", api_object.access_key),
            ("api", "export"),
            ("timestamp", timestamp),
            ("from", "-10000m"),
            ("to", "+10000m"),
            ("only_public", "potato"),
        )

        signature = generate_hmac_signature(path="/api/v1/export/", params=query_args, secret_key=api_object.secret_key)

        api_url = url_for(
            "api_v1.exportapi",
            **{
                "ak": api_object.access_key,
                "api": "export",
                "timestamp": timestamp,
                "signature": signature,
                "from": "-10000m",
                "to": "+10000m",
                "only_public": "potato",
            }
        )

        response = self.client.get(api_url)
        self.assertEqual(response.status_code, 403)

        expected_response = {
            "message": "Error using API: only_public value is not valid",
            "valid": False,
        }

        self.assertEqual(json.loads(response.data.decode("utf-8")), expected_response)
