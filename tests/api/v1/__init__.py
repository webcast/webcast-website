import datetime
import logging

from flask import url_for
from httmock import response, urlmatch

from app.extensions import db
from app.models.api import ApiKey
from app.models.audiences import Audience, AuthorizedUser

logger = logging.getLogger("webapp")

WOWZA_RESPONSE = """<head></head>
<body>
    <switch>
        <video src="mp4:1234_camera_source" system-bitrate="5216000" width="1920" height="1080">
            <param name="audioBitrate" value="96000" valuetype="data"/>
            <param name="videoBitrate" value="5120000" valuetype="data"/>
            <param name="videoCodecId" value="avc1.4d4028" valuetype="data"/>
            <param name="audioCodecId" value="mp4a.40.2" valuetype="data"/>
        </video>
        <video src="mp4:1234_camera_720p" system-bitrate="3096000" width="1280" height="720">
            <param name="audioBitrate" value="96000" valuetype="data"/>
            <param name="videoBitrate" value="3000000" valuetype="data"/>
            <param name="videoBitratePeak" value="3300000" valuetype="data"/>
            <param name="videoCodecId" value="avc1.4d4029" valuetype="data"/>
            <param name="audioCodecId" value="mp4a.40.2" valuetype="data"/>
        </video>
        <video src="mp4:1234_camera_1200-360p" system-bitrate="1296000" width="640" height="360">
            <param name="audioBitrate" value="96000" valuetype="data"/>
            <param name="videoBitrate" value="1200000" valuetype="data"/>
            <param name="videoBitratePeak" value="1320000" valuetype="data"/>
            <param name="videoCodecId" value="avc1.4d4020" valuetype="data"/>
            <param name="audioCodecId" value="mp4a.40.2" valuetype="data"/>
        </video>
        <video src="mp4:1234_camera_600-360p" system-bitrate="696000" width="640" height="360">
            <param name="audioBitrate" value="96000" valuetype="data"/>
            <param name="videoBitrate" value="600000" valuetype="data"/>
            <param name="videoBitratePeak" value="660000" valuetype="data"/>
            <param name="videoCodecId" value="avc1.42801e" valuetype="data"/>
            <param name="audioCodecId" value="mp4a.40.2" valuetype="data"/>
        </video>
    </switch>
</body>"""


@urlmatch(netloc=r"(.*\.)?wowzatest\.test\.cern(.*)")
def wowza_mock(url, request):
    headers = {"content-type": "text/html"}

    return response(200, WOWZA_RESPONSE, headers, None, 5, request)


def add_audience_and_authorized_users(client):
    authorized_user_form_data1 = {"name": "Authorized User 3"}

    response = client.post(
        url_for("admin-authorization.authorized_users_add"),
        data=authorized_user_form_data1,
        follow_redirects=True,
    )

    print(response.data)

    authorized_user = AuthorizedUser.query.get(1)

    audience_form_data1 = {
        "name": "ATLAS collaborators only",
        "default_application": "atlas",
        "authorized_users": [authorized_user.id],
    }

    client.post(
        url_for("admin-authorization.audiences_add"),
        data=audience_form_data1,
        follow_redirects=True,
    )


def add_live_event(client, indico_id=None):
    """

    :return:
    """
    today = datetime.datetime(2017, 3, 14, 14, 30, 0, 0)
    end_date = datetime.datetime(today.year, today.month, today.day, today.hour + 1, today.minute)

    audience = Audience.query.get(1)

    if not indico_id:
        indico_id = "1234"

    event_data1 = {
        "abstract": "This is the abstract of the Event",
        "speakers": "Speaker Name Lastname",
        "room": "28-1-007",
        "link": "https://webcast.web.cern.ch",
        "link_label": "",
        "display_message": "This is the text that will appear on the Event",
        "extra_html": "",
        "indico_id": indico_id,
        "indico_category_id": "987",
        "timezone": "Europe/Zurich",
        "start_date": [today.strftime("%Y-%m-%d"), today.strftime("%H:%M:%S")],
        # "start_date": ['2008-05-05', '04:30:00'],
        "end_date": [end_date.strftime("%Y-%m-%d"), end_date.strftime("%H:%M:%S")],
        # "start_date": ['2008-05-05', '04:30:00'],
        "authorized_users": [],
        "audience": audience.id,
    }

    client.post(url_for("admin-events.live_event_add"), data=event_data1, follow_redirects=True)


def add_api_key(self, resources="stream_update"):
    new_api_key = ApiKey(name="Example 1", description="Description 1", status=True, resources=resources)
    db.session.add(new_api_key)
    db.session.commit()
