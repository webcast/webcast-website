import json

from tests import BaseTestCase


class ApiExamplesTest(BaseTestCase):
    def test_example_hello(self):
        response = self.client.get("/api/v1/hello/")
        self.assertEqual(response.status_code, 200)

        expected_response = {"result": {"id": 1, "text": "Hello protected world!"}}

        self.assertEqual(json.loads(response.data.decode("utf-8")), expected_response)
