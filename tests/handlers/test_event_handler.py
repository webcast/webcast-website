from httmock import HTTMock
from sqlalchemy.orm.exc import NoResultFound
from werkzeug.exceptions import NotFound

from app.handlers.events import EventHandler, EventUpcomingHandler
from app.models.events import Event
from tests import BaseTestCase
from tests.helpers import EVENT_1_JSON, indico_event_details_mock, indico_webcasts_mock


class EventHandlerTest(BaseTestCase):
    def create_event_from_json(self):
        with HTTMock(indico_event_details_mock):
            event_upcoming_handler = EventUpcomingHandler()
            event_upcoming_handler.create_event_from_json(EVENT_1_JSON)

    def test_create_event_from_json(self):
        with HTTMock(indico_event_details_mock):

            event_upcoming_handler = EventUpcomingHandler()
            event_upcoming_handler.create_event_from_json(EVENT_1_JSON)

            try:
                event = Event.query.get(1)
                self.assertIsNotNone(event)
                self.assertEqual(event.indico_id, "100")
            except NoResultFound:
                self.assertTrue(False, "Event was not created")

    def test_fetch_event_test(self):
        with HTTMock(indico_event_details_mock):
            self.create_event_from_json()

            try:
                event = Event.query.get(1)
                self.assertIsNotNone(event)
                self.assertEqual(event.indico_id, "100")
                event_upcoming_handler = EventUpcomingHandler()
                event_upcoming_handler.fetch_event_by_id(event.id)

            except NoResultFound:
                self.assertTrue(False, "Event was not created")

    def test_create_event_existing(self):
        with HTTMock(indico_event_details_mock):
            self.create_event_from_json()

            try:
                event = Event.query.get(1)
                self.assertIsNotNone(event)
                self.assertEqual(event.indico_id, "100")
            except NoResultFound:
                self.assertTrue(False, "Event was not created")

            self.create_event_from_json()

            events = Event.query.all()
            self.assertEqual(len(events), 1)

    def test_fetch_events(self):
        with HTTMock(indico_webcasts_mock):
            with HTTMock(indico_event_details_mock):
                events_handler = EventUpcomingHandler()
                events_handler.fetch_events()

                events = Event.query.all()
                self.assertEqual(len(events), 3)

    def test_fetch_event_by_id(self):
        with HTTMock(indico_webcasts_mock):
            with HTTMock(indico_event_details_mock):

                events_handler = EventUpcomingHandler()
                events_handler.fetch_event_by_indico_id(100)

                events = Event.query.all()
                self.assertEqual(len(events), 1)

    def test_get_event_or_404(self):
        with HTTMock(indico_webcasts_mock):
            with HTTMock(indico_event_details_mock):

                events_handler = EventUpcomingHandler()
                events_handler.fetch_event_by_indico_id(100)

                events = Event.query.all()
                self.assertEqual(len(events), 1)

                event_handler = EventHandler()

                event, event_found = event_handler.get_event_or_404(str(events[0].id))

                self.assertTrue(event_found)
                self.assertIsNotNone(event)

    def test_get_event_or_404_with_indico_prefix(self):
        with HTTMock(indico_webcasts_mock):
            with HTTMock(indico_event_details_mock):

                events_handler = EventUpcomingHandler()
                events_handler.fetch_event_by_indico_id(100)

                events = Event.query.all()
                self.assertEqual(len(events), 1)

                event_handler = EventHandler()

                event, event_found = event_handler.get_event_or_404("i100")

                self.assertTrue(event_found)
                self.assertIsNotNone(event)

    def test_get_event_or_404_not_found(self):
        with HTTMock(indico_webcasts_mock):
            with HTTMock(indico_event_details_mock):

                events_handler = EventUpcomingHandler()
                events_handler.fetch_event_by_indico_id(100)

                events = Event.query.all()
                self.assertEqual(len(events), 1)

                event_handler = EventHandler()

                with self.assertRaises(NotFound):
                    event_handler.get_event_or_404("101")
                    self.assertTrue(False)
