import datetime

from flask import url_for

from app.extensions import db
from app.models.audiences import Audience, AuthorizedUser
from app.models.events import Event, EventStatus
from tests import BaseTestCase


class UserUtilsTest(BaseTestCase):
    def test_util_without_room(self):
        #
        # Adding audience
        #

        authorized_user_form_data1 = {"name": "Authorized User 3"}

        self.client.post(
            url_for("admin-authorization.authorized_users_add"),
            data=authorized_user_form_data1,
            follow_redirects=True,
        )

        authorized_user = AuthorizedUser.query.get(1)

        audience_form_data1 = {
            "name": "ATLAS collaborators only",
            "default_application": "atlas",
            "authorized_users": [authorized_user.id],
        }

        self.client.post(
            url_for("admin-authorization.audiences_add"),
            data=audience_form_data1,
            follow_redirects=True,
        )

        audience = Audience.query.get(1)
        self.assertIsNotNone(audience)

        #
        # Adding event
        #
        today = datetime.datetime(2017, 5, 29, 14, 30, 0, 0)
        end_date = datetime.datetime(today.year, today.month, today.day, today.hour + 1, today.minute)

        event = Event(
            indico_id="1234",
            link="https://webcast.web.cern.ch",
            title="This is event title",
            audience=audience,
            timezone="Europe/Zurich",
            status=EventStatus.LIVE,
        )
        event.start_date = today
        event.end_date = end_date

        db.session.add(event)
        db.session.commit()
        event = Event.query.get(1)
        self.assertIsNotNone(event)

        self.assertEqual(1, event.id)
        #
        response = self.client.get(url_for("users-utils.get_ical", event_id=event.id))
        self.assertEqual(response.status_code, 200)

    def test_util_with_room(self):
        #
        # Adding audience
        #

        authorized_user_form_data1 = {"name": "Authorized User 3"}

        self.client.post(
            url_for("admin-authorization.authorized_users_add"),
            data=authorized_user_form_data1,
            follow_redirects=True,
        )

        authorized_user = AuthorizedUser.query.get(1)

        audience_form_data1 = {
            "name": "ATLAS collaborators only",
            "default_application": "atlas",
            "authorized_users": [authorized_user.id],
        }

        self.client.post(
            url_for("admin-authorization.audiences_add"),
            data=audience_form_data1,
            follow_redirects=True,
        )

        audience = Audience.query.get(1)
        self.assertIsNotNone(audience)

        #
        # Adding event
        #
        today = datetime.datetime(2017, 5, 29, 14, 30, 0, 0)
        end_date = datetime.datetime(today.year, today.month, today.day, today.hour + 1, today.minute)

        event = Event(
            indico_id="1234",
            link="https://webcast.web.cern.ch",
            title="This is event title",
            audience=audience,
            room="28-1-002",
            timezone="Europe/Zurich",
            status=EventStatus.LIVE,
        )
        event.start_date = today
        event.end_date = end_date

        db.session.add(event)
        db.session.commit()
        event = Event.query.get(1)
        self.assertIsNotNone(event)

        self.assertEqual(1, event.id)
        #
        response = self.client.get(url_for("users-utils.get_ical", event_id=event.id))
        self.assertEqual(response.status_code, 200)

    def test_not_existing_event(self):

        response = self.client.get(url_for("users-utils.get_ical", event_id=1))
        self.assertEqual(response.status_code, 404)
