import datetime

from flask import url_for

from app.extensions import db
from app.models.audiences import Audience, AuthorizedUser
from app.models.events import CDSRecord, Event, EventStatus
from tests import BaseTestCase


class FeedsTest(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self)
        #
        # Adding audience
        #

        authorized_user_form_data1 = {"name": "Authorized User 3"}

        self.client.post(
            url_for("admin-authorization.authorized_users_add"),
            data=authorized_user_form_data1,
            follow_redirects=True,
        )

        authorized_user = AuthorizedUser.query.get(1)

        audience_form_data1 = {
            "name": "ATLAS collaborators only",
            "default_application": "atlas",
            "authorized_users": [authorized_user.id],
        }

        self.client.post(
            url_for("admin-authorization.audiences_add"),
            data=audience_form_data1,
            follow_redirects=True,
        )

        audience = Audience.query.get(1)
        self.assertIsNotNone(audience)

        #
        # Adding event
        #
        today = datetime.datetime(2017, 5, 29, 14, 30, 0, 0)
        end_date = datetime.datetime(today.year, today.month, today.day, today.hour + 1, today.minute)

        event = Event(
            indico_id="1234",
            link="https://webcast.web.cern.ch",
            title="This is event title",
            audience=audience,
            timezone="Europe/Zurich",
            status=EventStatus.LIVE,
        )
        event.start_date = today
        event.end_date = end_date

        db.session.add(event)
        db.session.commit()
        event = Event.query.get(1)
        self.assertIsNotNone(event)

        self.assertEqual(1, event.id)

    def test_events_feeds_all(self):
        #
        response = self.client.get(url_for("users-feeds.events_feed"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, "application/atom+xml; charset=utf-8")

    def test_event_feeds_recent(self):
        today = datetime.datetime(2017, 5, 29, 14, 30, 0, 0)
        publish_date = datetime.datetime(today.year, today.month, today.day, today.hour + 1, today.minute)

        cds_record = CDSRecord(indico_link="https://indico.cern.ch", cds_record_id="1234")
        cds_record.title = "This is the new title of the Record"
        cds_record.room = "28/1-002"
        cds_record.image_url = "https://cern.ch/fake_img.png"
        cds_record.speakers = "Speaker 1 and Speaker 2"
        cds_record.published_date = publish_date

        db.session.add(cds_record)
        db.session.commit()

        #
        response = self.client.get(url_for("users-feeds.events_feed", type="recent"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, "application/atom+xml; charset=utf-8")

    def test_events_feeds_upcoming(self):
        #
        response = self.client.get(url_for("users-feeds.events_feed", type="upcoming"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, "application/atom+xml; charset=utf-8")

    def test_events_feeds_live(self):
        #
        response = self.client.get(url_for("users-feeds.events_feed", type="live"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, "application/atom+xml; charset=utf-8")
