import datetime

from flask import current_app, url_for

from app.models.audiences import Audience, AuthorizedUser
from app.models.events import Event, EventStatus
from tests import BaseTestCase


class AdminEventsUpcmomingTest(BaseTestCase):
    def add_audience_and_authorized_users(self):
        authorized_user_form_data1 = {"name": "Authorized User 3"}

        self.client.post(
            url_for("admin-authorization.authorized_users_add"),
            data=authorized_user_form_data1,
            follow_redirects=True,
        )

        authorized_user = AuthorizedUser.query.get(1)

        audience_form_data1 = {
            "name": "ATLAS collaborators only",
            "default_application": "atlas",
            "authorized_users": [authorized_user.id],
        }

        self.client.post(
            url_for("admin-authorization.audiences_add"),
            data=audience_form_data1,
            follow_redirects=True,
        )

    def test_events_upcoming_reachable(self):
        response = self.client.get(url_for("admin-events.events_upcoming"))
        self.assertEqual(response.status_code, 200)

    def test_events_upcoming_add_reachable(self):
        response = self.client.get(url_for("admin-events.upcoming_event_add"))
        self.assertEqual(response.status_code, 200)

    def test_upcoming_event_add(self):
        """

        :return:
        """
        today = datetime.datetime(2017, 5, 29, 14, 30, 0, 0)
        end_date = datetime.datetime(today.year, today.month, today.day, today.hour + 1, today.minute)

        self.add_audience_and_authorized_users()

        audience = Audience.query.get(1)
        self.assertIsNotNone(audience)

        event_data1 = {
            "abstract": "This is the abstract of the Event",
            "speakers": "Speaker Name Lastname",
            "room": "28-1-007",
            "link": "https://webcast.web.cern.ch",
            "link_label": "",
            "display_message": "This is the text that will appear on the Event",
            "extra_html": "",
            "indico_id": "1234",
            "indico_category_id": "987",
            "timezone": "Europe/Zurich",
            "start_date": [today.strftime("%Y-%m-%d"), today.strftime("%H:%M:%S")],
            # "start_date": ['2008-05-05', '04:30:00'],
            "end_date": [end_date.strftime("%Y-%m-%d"), end_date.strftime("%H:%M:%S")],
            # "start_date": ['2008-05-05', '04:30:00'],
            "audience": audience.id,
        }

        response = self.client.post(
            url_for("admin-events.upcoming_event_add"),
            data=event_data1,
            follow_redirects=True,
        )

        self.assertEqual(response.status_code, 200)

        events = Event.query.all()
        self.assertEqual(len(events), 1)

        event = Event.query.get(1)
        self.assertIsNotNone(event)
        self.assertEqual(event.abstract, "This is the abstract of the Event")
        self.assertEqual(event.speakers, "Speaker Name Lastname")
        self.assertEqual(event.room, "28-1-007")
        self.assertEqual(event.link, "https://webcast.web.cern.ch")
        self.assertEqual(event.link_label, "")
        self.assertEqual(event.display_message, "This is the text that will appear on the Event")
        self.assertEqual(event.extra_html, "")
        self.assertEqual(event.indico_id, "1234")
        self.assertEqual(event.indico_category_id, "987")
        self.assertEqual(event.timezone, "Europe/Zurich")
        self.assertEqual(event.start_date, today)
        self.assertEqual(event.audience, audience)

        self.assertIsNotNone(event.live_stream)

    def test_live_event_edit(self):
        self.add_audience_and_authorized_users()
        audience = Audience.query.get(1)
        self.assertIsNotNone(audience)

        today = datetime.datetime(2017, 5, 29, 14, 30, 0, 0)
        end_date = datetime.datetime(today.year, today.month, today.day, today.hour + 1, today.minute)

        old_event_data = {
            "abstract": "This is the abstract of the Event",
            "speakers": "Speaker Name Lastname",
            "room": "28-1-007",
            "link": "https://webcast.web.cern.ch",
            "link_label": "",
            "display_message": "This is the text that will appear on the Event",
            "extra_html": "",
            "indico_id": "1234",
            "indico_category_id": "987",
            "timezone": "Europe/Zurich",
            "start_date": [today.strftime("%Y-%m-%d"), today.strftime("%H:%M:%S")],
            # "start_date": ['2008-05-05', '04:30:00'],
            "end_date": [end_date.strftime("%Y-%m-%d"), end_date.strftime("%H:%M:%S")],
            # "start_date": ['2008-05-05', '04:30:00'],
            "audience": audience.id,
        }

        new_event_data = {
            "abstract": "This is the new abstract of the Event",
            "speakers": "Speaker New Name Lastname",
            "room": "28-1-003",
            "link": "https://webcasttest.web.cern.ch",
            "link_label": "",
            "display_message": "This is the new text that will appear on the Event",
            "extra_html": "",
            "indico_id": "4321",
            "indico_category_id": "789",
            "timezone": "Europe/Zurich",
            "start_date": [today.strftime("%Y-%m-%d"), today.strftime("%H:%M:%S")],
            # "start_date": ['2008-05-05', '04:30:00'],
            "end_date": [end_date.strftime("%Y-%m-%d"), end_date.strftime("%H:%M:%S")],
            # "start_date": ['2008-05-05', '04:30:00'],
            "audience": audience.id,
            "number-of-streams": 0,
        }

        response = self.client.post(
            url_for("admin-events.upcoming_event_add"),
            data=old_event_data,
            follow_redirects=True,
        )

        self.assertEqual(response.status_code, 200)

        event = Event.query.get(1)
        self.assertIsNotNone(event)

        response = self.client.post(
            url_for("admin-events.upcoming_event_edit", event_id=1, event_type="upcoming"),
            data=new_event_data,
            follow_redirects=True,
        )

        self.assertEqual(response.status_code, 200)

        event = Event.query.get(1)
        self.assertIsNotNone(event)
        self.assertEqual(event.abstract, "This is the new abstract of the Event")
        self.assertEqual(event.speakers, "Speaker New Name Lastname")
        self.assertEqual(event.room, "28-1-003")
        self.assertEqual(event.link, "https://webcasttest.web.cern.ch")
        self.assertEqual(event.link_label, "")
        self.assertEqual(event.display_message, "This is the new text that will appear on the Event")
        self.assertEqual(event.extra_html, "")
        self.assertEqual(event.indico_id, "4321")
        self.assertEqual(event.indico_category_id, "789")
        self.assertEqual(event.timezone, "Europe/Zurich")
        self.assertEqual(event.start_date, today)
        self.assertEqual(event.audience, audience)

    def test_upcoming_event_delete(self):
        self.add_audience_and_authorized_users()
        audience = Audience.query.get(1)
        self.assertIsNotNone(audience)

        today = datetime.datetime(2017, 5, 29, 14, 30, 0, 0)
        end_date = datetime.datetime(today.year, today.month, today.day, today.hour + 1, today.minute)

        old_event_data = {
            "abstract": "This is the abstract of the Event",
            "speakers": "Speaker Name Lastname",
            "room": "28-1-007",
            "link": "https://webcast.web.cern.ch",
            "link_label": "",
            "display_message": "This is the text that will appear on the Event",
            "extra_html": "",
            "indico_id": "1234",
            "indico_category_id": "987",
            "timezone": "Europe/Zurich",
            "start_date": [today.strftime("%Y-%m-%d"), today.strftime("%H:%M:%S")],
            # "start_date": ['2008-05-05', '04:30:00'],
            "end_date": [end_date.strftime("%Y-%m-%d"), end_date.strftime("%H:%M:%S")],
            # "start_date": ['2008-05-05', '04:30:00'],
            "audience": audience.id,
        }

        response = self.client.post(
            url_for("admin-events.upcoming_event_add"),
            data=old_event_data,
            follow_redirects=True,
        )

        self.assertEqual(response.status_code, 200)
        event = Event.query.get(1)
        self.assertIsNotNone(event)

        new_event_data = {
            "abstract": "This is the new abstract of the Event",
            "speakers": "Speaker New Name Lastname",
            "room": "28-1-003",
            "link": "https://webcasttest.web.cern.ch",
            "link_label": "",
            "display_message": "This is the new text that will appear on the Event",
            "extra_html": "",
            "indico_id": "4321",
            "indico_category_id": "789",
            "timezone": "Europe/Zurich",
            "start_date": [today.strftime("%Y-%m-%d"), today.strftime("%H:%M:%S")],
            # "start_date": ['2008-05-05', '04:30:00'],
            "end_date": [end_date.strftime("%Y-%m-%d"), end_date.strftime("%H:%M:%S")],
            # "start_date": ['2008-05-05', '04:30:00'],
            "audience": audience.id,
            "number-of-streams": 0,
        }

        delete_form_data = {"next": url_for("admin-events.events_upcoming")}

        response = self.client.post(
            url_for("admin-events.upcoming_event_add"),
            data=new_event_data,
            follow_redirects=True,
        )

        self.assertEqual(response.status_code, 200)

        events = Event.query.all()
        self.assertEqual(len(events), 2)

        response = self.client.post(
            url_for("admin-events.event_delete", event_id=1),
            data=delete_form_data,
            follow_redirects=True,
        )

        self.assertEqual(response.status_code, 200)

        events = Event.query.all()
        self.assertEqual(len(events), 2)

        response = self.client.post(
            url_for("admin-events.event_delete", event_id=1),
            data=delete_form_data,
            follow_redirects=True,
        )

        self.assertEqual(response.status_code, 200)

        events = Event.query.all()
        self.assertEqual(len(events), 1)

        response = self.client.post(
            url_for("admin-events.event_delete", event_id=1),
            data=delete_form_data,
            follow_redirects=True,
        )
        self.assertEqual(response.status_code, 404, "The event should not be deleted twice")

    def test_upcoming_event_go_live(self):
        # First we add the event
        self.add_audience_and_authorized_users()
        audience = Audience.query.get(1)
        self.assertIsNotNone(audience)

        today = datetime.datetime(2017, 5, 29, 14, 30, 0, 0)
        end_date = datetime.datetime(today.year, today.month, today.day, today.hour + 1, today.minute)

        old_event_data = {
            "abstract": "This is the abstract of the Event",
            "speakers": "Speaker Name Lastname",
            "room": "28-1-007",
            "link": "https://webcast.web.cern.ch",
            "link_label": "",
            "display_message": "This is the text that will appear on the Event",
            "extra_html": "",
            "indico_id": "1234",
            "indico_category_id": "987",
            "timezone": "Europe/Zurich",
            "start_date": [today.strftime("%Y-%m-%d"), today.strftime("%H:%M:%S")],
            # "start_date": ['2008-05-05', '04:30:00'],
            "end_date": [end_date.strftime("%Y-%m-%d"), end_date.strftime("%H:%M:%S")],
            # "start_date": ['2008-05-05', '04:30:00'],
            "audience": audience.id,
        }

        response = self.client.post(
            url_for("admin-events.upcoming_event_add"),
            data=old_event_data,
            follow_redirects=True,
        )

        self.assertEqual(response.status_code, 200)
        event = Event.query.get(1)
        self.assertIsNotNone(event)

        # Once the event is added we want to make it GO LIVE

        # GO LIVE edit page works?
        response = self.client.get(url_for("admin-events.upcoming_event_edit", event_id=event.id, go_live=True))
        self.assertEqual(response.status_code, 200)

        event_data_smil = {
            "abstract": "This is the abstract of the Event",
            "speakers": "Speaker Name Lastname",
            "room": "28-1-007",
            "link": "https://webcast.web.cern.ch",
            "link_label": "",
            "display_message": "This is the text that will appear on the Event",
            "extra_html": "",
            "indico_id": "1234",
            "indico_category_id": "987",
            "timezone": "Europe/Zurich",
            "start_date": [today.strftime("%Y-%m-%d"), today.strftime("%H:%M:%S")],
            "end_date": [end_date.strftime("%Y-%m-%d"), end_date.strftime("%H:%M:%S")],
            "audience": audience.id,
            "save_and_go_live": "true",
            "existing-stream-1-use_smil": "yes",
        }

        event = Event.query.get(1)

        response = self.client.post(
            url_for("admin-events.upcoming_event_edit", event_id=event.id, go_live=True),
            data=event_data_smil,
            follow_redirects=True,
        )
        self.assertTrue(event.live_stream.use_smil)
        self.assertEqual(response.status_code, 200)
        current_app.logger.debug(response.data)
        self.assertTrue("Unable to go live: use_smil is True but SMIL files were not found." in str(response.data))
        self.assertEqual(event.status, EventStatus.UPCOMING)

        event_data_no_smil = {
            "abstract": "This is the abstract of the Event",
            "speakers": "Speaker Name Lastname",
            "room": "28-1-007",
            "link": "https://webcast.web.cern.ch",
            "link_label": "",
            "display_message": "This is the text that will appear on the Event",
            "extra_html": "",
            "indico_id": "1234",
            "indico_category_id": "987",
            "timezone": "Europe/Zurich",
            "start_date": [today.strftime("%Y-%m-%d"), today.strftime("%H:%M:%S")],
            "end_date": [end_date.strftime("%Y-%m-%d"), end_date.strftime("%H:%M:%S")],
            "audience": audience.id,
            "save_and_go_live": "true",
        }

        # Save and GO LIVE  works with use smil == False?
        response = self.client.post(
            url_for("admin-events.upcoming_event_edit", event_id=event.id, go_live=True),
            data=event_data_no_smil,
        )
        self.assertEqual(response.status_code, 302)
        event = Event.query.get(1)
        self.assertEqual(event.status, EventStatus.LIVE)

    def test_unpublish_event(self):
        # First we add the event
        self.add_audience_and_authorized_users()
        audience = Audience.query.get(1)
        self.assertIsNotNone(audience)

        today = datetime.datetime(2017, 5, 29, 14, 30, 0, 0)
        end_date = datetime.datetime(today.year, today.month, today.day, today.hour + 1, today.minute)

        event_data_no_smil = {
            "abstract": "This is the abstract of the Event",
            "speakers": "Speaker Name Lastname",
            "room": "28-1-007",
            "link": "https://webcast.web.cern.ch",
            "link_label": "",
            "display_message": "This is the text that will appear on the Event",
            "extra_html": "",
            "indico_id": "1234",
            "indico_category_id": "987",
            "timezone": "Europe/Zurich",
            "start_date": [today.strftime("%Y-%m-%d"), today.strftime("%H:%M:%S")],
            "end_date": [end_date.strftime("%Y-%m-%d"), end_date.strftime("%H:%M:%S")],
            "audience": audience.id,
            "save_and_go_live": "true",
        }

        response = self.client.post(
            url_for("admin-events.upcoming_event_add"),
            data=event_data_no_smil,
            follow_redirects=True,
        )

        self.assertEqual(response.status_code, 200)
        event = Event.query.get(1)
        self.assertIsNotNone(event)

        event = Event.query.get(1)

        # Save and GO LIVE  works with use smil == False?
        response = self.client.post(
            url_for("admin-events.upcoming_event_edit", event_id=event.id, go_live=True),
            data=event_data_no_smil,
        )
        self.assertEqual(response.status_code, 302)
        event = Event.query.get(1)
        self.assertEqual(event.status, EventStatus.LIVE)

        unpublish_form_data = {}

        # Unpublish the event
        response = self.client.post(
            url_for("admin-events.unpublish", event_id=event.id),
            data=unpublish_form_data,
            follow_redirects=True,
        )
        self.assertEqual(response.status_code, 200)
        event = Event.query.get(1)
        self.assertEqual(event.status, EventStatus.ARCHIVED)
