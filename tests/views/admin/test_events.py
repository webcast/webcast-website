from flask import url_for

from tests import BaseTestCase


class AdminEventsTest(BaseTestCase):
    def test_events_home_reachable(self):
        response = self.client.get(url_for("admin-events.index"))
        self.assertEqual(response.status_code, 200)


class AdminEventsArchivedTest(BaseTestCase):
    def test_events_recent(self):
        response = self.client.get(url_for("admin-events.events_archived"))
        self.assertEqual(response.status_code, 200)


class AdminEventsRecentTest(BaseTestCase):
    def test_events_recent_reachable(self):
        response = self.client.get(url_for("admin-events.events_recent"))
        self.assertEqual(response.status_code, 200)
