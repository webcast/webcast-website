from flask import url_for
from sqlalchemy.orm.exc import NoResultFound

from app.models.streams import Category
from tests import BaseTestCase


class AdminPermanentStreamsCategoriesTest(BaseTestCase):
    def test_permanent_streams_reachable(self):
        response = self.client.get(url_for("admin-streams.categories"))
        self.assertEqual(response.status_code, 200)

    def test_permanent_streams_add_reachables(self):
        response = self.client.get(url_for("admin-streams.categories_add"))
        self.assertEqual(response.status_code, 200)

    def test_permanent_streams_add(self):
        # response = self.client.get(url_for('admin.str'
        #                                    'eams.permanent_streams_add'))
        # self.assertEqual(response.status_code, 200)

        permanent_stream_category_form_data1 = {
            "name": "Atlas",
            "priority": 1,
            "default_img": "/static-files/images/default/categories/atlas/category_atlas.png",
        }

        self.client.post(
            url_for("admin-streams.categories_add"),
            data=permanent_stream_category_form_data1,
            follow_redirects=True,
        )

        categories = Category.query.all()
        self.assertEqual(len(categories), 1)

    def test_permanent_streams_categories_delete(self):

        permanent_stream_category_form_data1 = {
            "name": "Atlas",
            "priority": 1,
            "default_img": "/static-files/images/default/categories/atlas/category_atlas.png",
        }

        self.client.post(
            url_for("admin-streams.categories_add"),
            data=permanent_stream_category_form_data1,
            follow_redirects=True,
        )

        categories = Category.query.all()
        self.assertEqual(len(categories), 1)

        self.client.post(
            url_for("admin-streams.categories_delete", category_id=1),
            follow_redirects=True,
        )

        categories = Category.query.all()
        self.assertEqual(len(categories), 0)

    def test_permanent_streams_categories_edit(self):

        permanent_stream_category_form_data1 = {
            "name": "Atlas",
            "priority": 1,
            "default_img": "/static-files/images/default/categories/atlas/category_atlas.png",
        }

        permanent_stream_category_form_data2 = {
            "name": "Salta",
            "priority": 1,
            "default_img": "/static-files/images/default/categories/atlas/category_atlas.png",
        }

        self.client.post(
            url_for("admin-streams.categories_add"),
            data=permanent_stream_category_form_data1,
            follow_redirects=True,
        )

        categories = Category.query.all()
        self.assertEqual(len(categories), 1)

        self.client.post(
            url_for("admin-streams.categories_edit", category_id=1),
            data=permanent_stream_category_form_data2,
            follow_redirects=True,
        )

        try:
            category = Category.query.filter_by(name="Salta").one()
            self.assertFalse(category.use_default_img)
        except NoResultFound:
            self.assertTrue(False)
