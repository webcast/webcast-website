from flask import url_for
from httmock import HTTMock

from app.models.events import CDSRecord
from tests import BaseTestCase
from tests.helpers.test_ces_client import ces_recent_request


class AdminEventsRecentTest(BaseTestCase):
    def test_fetch_recent_events(self):
        with HTTMock(ces_recent_request):
            response = self.client.post(url_for("admin-events.fetch_from_cds"), follow_redirects=True)
            self.assertEqual(response.status_code, 200)
            self.assertIn(
                b"Webcasts fetched from CDS and events updated",
                response.data,
                "A success message is expected",
            )
            cds_events = CDSRecord.query.all()
            self.assertEqual(len(cds_events), 10, "We expect 10 cds records")

    def test_list_recent_events_is_reachable(self):
        with HTTMock(ces_recent_request):
            response = self.client.get(url_for("admin-events.events_recent"))
            self.assertEqual(response.status_code, 200)

    def test_edit_recent_event(self):
        with HTTMock(ces_recent_request):
            self.client.post(url_for("admin-events.fetch_from_cds"), follow_redirects=True)
            self.assertEqual(len(CDSRecord.query.all()), 10, "We expect 10 cds records")
            first_record = CDSRecord.query.get(1)
            """
            cds_record.title = form.title.data
            cds_record.cds_record_id = form.cds_record_id.data
            cds_record.published_date = form.published_date.data
            cds_record.indico_id = form.indico_id.data
            cds_record.room = form.room.data
            cds_record.indico_link = form.indico_link.data
            cds_record.image_url = form.image_url.data
            cds_record.speakers = form.speakers.data
            """

            new_record_data = {
                "title": "This is the new title of the Record",
                "cds_record_id": first_record.cds_record_id,
                "room": first_record.room,
                "indico_link": first_record.indico_link,
                "image_url": first_record.image_url,
                "speakers": first_record.speakers,
                "published_date": first_record.published_date,
            }

            response = self.client.post(
                url_for("admin-events.cds_records_edit", cds_record_id=first_record.id),
                data=new_record_data,
                follow_redirects=True,
            )

            self.assert_200(response)
            self.assertIn(
                b"CDS Record updated successfully",
                response.data,
                "A success message is expected",
            )

            first_record = CDSRecord.query.get(1)
            self.assertEqual(
                first_record.title,
                "This is the new title of the Record",
                "The title must have been changed",
            )

    def test_delete_recent_event(self):
        with HTTMock(ces_recent_request):
            # cds_record_delete
            self.client.post(url_for("admin-events.fetch_from_cds"), follow_redirects=True)
            self.assertEqual(len(CDSRecord.query.all()), 10, "We expect 10 cds records")

            first_record = CDSRecord.query.get(1)

            delete_data = {
                "delete_id": first_record.id,
                "next": url_for("admin-events.events_recent"),
            }

            response = self.client.post(
                url_for("admin-events.cds_record_delete", cds_record_id=first_record.id),
                data=delete_data,
                follow_redirects=True,
            )
            self.assert_200(response, response.data)
            self.assertEqual(len(CDSRecord.query.all()), 9, "We expect 9 cds records")
