from flask import url_for
from sqlalchemy.orm.exc import NoResultFound

from app.models.api import ApiKey
from tests import BaseTestCase


class AdminApiKeysTest(BaseTestCase):
    def test_api_keys_reachable(self):
        response = self.client.get(url_for("admin-api.api_keys"))
        self.assertEqual(response.status_code, 200)

    def test_api_keys_add_reachable(self):
        response = self.client.get(url_for("admin-api.keys_add"))
        self.assertEqual(response.status_code, 200)

    def test_api_keys_add(self):

        api_key_form_data = {
            "name": "Example 1",
            "description": "This is the description number 1",
            "status": True,
            "resources": "notification",
        }
        #
        self.client.post(url_for("admin-api.keys_add"), data=api_key_form_data, follow_redirects=True)
        api_key = ApiKey.query.filter_by(name="Example 1").one()
        self.assertIsNotNone(api_key)

        api_keys = ApiKey.query.all()
        self.assertEqual(len(api_keys), 1)

    def test_api_keys_delete(self):

        api_key_form_data = {
            "name": "Example 1",
            "description": "This is the description number 1",
            "status": True,
            "resources": "notification",
        }
        #
        self.client.post(url_for("admin-api.keys_add"), data=api_key_form_data, follow_redirects=True)
        api_key = ApiKey.query.filter_by(name="Example 1").one()
        self.assertIsNotNone(api_key)

        api_keys = ApiKey.query.all()
        self.assertEqual(len(api_keys), 1)

        self.client.post(url_for("admin-api.keys_delete", key_id=api_key.id), follow_redirects=True)
        try:
            ApiKey.query.filter_by(name="Example 1").one()
            self.assertTrue(False, "ApiKey was not deleted successfully")
        except NoResultFound:
            api_keys = ApiKey.query.all()
            self.assertEqual(len(api_keys), 0)

    def test_api_keys_edit(self):

        api_key_form_data = {
            "name": "Example 1",
            "description": "This is the description number 1",
            "status": True,
            "resources": "notification",
        }

        api_key_form_new_data = {
            "name": "Example 2",
            "description": "This is the description number 2",
            "resources": "export",
        }
        #
        self.client.post(url_for("admin-api.keys_add"), data=api_key_form_data, follow_redirects=True)
        api_key = ApiKey.query.filter_by(name="Example 1").one()
        self.assertIsNotNone(api_key)

        api_keys = ApiKey.query.all()
        self.assertEqual(len(api_keys), 1)

        self.client.post(
            url_for("admin-api.keys_edit", key_id=api_key.id),
            data=api_key_form_new_data,
            follow_redirects=True,
        )
        try:
            ApiKey.query.filter_by(name="Example 1").one()
            self.assertTrue(False, "ApiKey was not edited successfully")
        except NoResultFound:
            pass  # It's ok!

        try:
            api_key = ApiKey.query.filter_by(name="Example 2").one()
            self.assertFalse(api_key.status)
        except NoResultFound:
            self.assertTrue(False, "ApiKey was not found with the new name")
