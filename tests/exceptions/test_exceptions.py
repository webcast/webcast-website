from app.exceptions import WebappException
from tests import BaseTestCase


class WebappExceptionTest(BaseTestCase):
    def test_create_webapp_exception(self):
        with self.assertRaises(WebappException):
            raise WebappException("This is a sample exception")
