import urllib3

from app.app_factory import init_celery

urllib3.disable_warnings()

# Initialize the Celery worker to run background tasks
celery = init_celery()
