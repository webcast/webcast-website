# Contributing

## Setting Up Your Development Environment

You should use Docker and Docker Compose for working on this project,
as it will create an isolated installation.

To run the project:
```bash
docker-compose up --build --remove-orphans
```

To access the bash inside the container:
```bash
docker-compose run web bash
```

## Branching

If you would like to contribute to this project, you will need to use
[git flow](https://github.com/nvie/gitflow). This way, any and all changes
happen on the qa branch and not on the master branch. As such, after
you have git-flow-ified your `zoomus` git repo, create a pull request for your
branch, and we'll take it from there.